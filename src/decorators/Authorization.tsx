import React from "react";
import {Role} from "../store/settings/role/types";
import {getUserInfoFromStorage} from "../utils";
import {ApplicationRole} from "../types";

export const withRoles = (Component: React.ComponentType, allowedRoles: Array<ApplicationRole>) => {
    return class extends React.Component<any, any> {
        render() {
            if (roleExists(allowedRoles)) {
                return <Component {...this.props} />
            } else {
                return <h1>You don't have permission to access!</h1>
            }
        }
    };
};

export const roleExists = (allowedRoles: Array<ApplicationRole>) => {
    const userRoles: Array<Role> = getUserInfoFromStorage().roles;//todo move to another location
    return allowedRoles.some(roleName => userRoles.some(role => role.name === roleName));
};