import {Role} from "../store/settings/role/types";

export type SignatureParams = {
    client?: string,
    login?: string,
    date?: string;
    startDate?: string,
    endDate?: string,
    companyid?: number;
    productivity?: string,
    departmentid?: number;
    projectid?: number;
    signature?: string;
    period?: string;
}

export type UserWithRoles = User & { role: Role, roles: Array<Role> }

export type AxiosResultMessage = {
    success: boolean,
    message: string,
}

export type User = {
    id: number,
    firstName: string,
    lastName: string,
    login: string,
    password?: string,
    email: string,
    superUser: boolean
}

export enum ApplicationRole {
    USER_COMPANY = "USER_COMPANY",
    ADMIN_COMPANY = "ADMIN_COMPANY",
    ADMIN_DEPARTMENT = "ADMIN_DEPARTMENT",
    USER_DEPARTMENT = "USER_DEPARTMENT",
    USER_PROJECT = "USER_PROJECT",
    ADMIN_PROJECT = "ADMIN_PROJECT",
    ADMIN_APPLICATION = "ADMIN_APPLICATION"
}

export enum Status {
    OPEN = "OPEN",
    PAUSED = "PAUSED",
    CLOSED = "CLOSED",
}

export enum Productivity {
    PRODUCTIVE = "PRODUCTIVE",
    UNPRODUCTIVE = "UNPRODUCTIVE",
    NEUTRAL = "NEUTRAL"
}

export enum ActivityPeriod {
    DAY = "DAY",
    WEEK = "WEEK",
    MONTH = "MONTH"
}