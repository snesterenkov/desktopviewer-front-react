export const REGISTRATION_SUCCESS = "Registration successful. Please check your email.";
export const RESTORE_PASSWORD_REQUEST_SUCCESS = "The request successfully sent. Check your email.";
export const RESTORE_PASSWORD_SUCCESS = "Your password has been changed successfully.";