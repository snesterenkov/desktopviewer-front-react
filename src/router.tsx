import React from 'react';
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import {Redirect, Route, Router, Switch} from 'react-router-dom';
// import {composeWithDevTools} from "redux-devtools-extension";

import axios from "axios";
import thunk from "redux-thunk";

import {rootReducer} from "./store";
import history from "./store/browserHistory"
import Login from "./components/auth/Login";
import MiniDrawer from "./components/drawer/MiniDrawer";
import {
    EMPLOYEES_PAGE_URL,
    HOST_URL,
    INVITE_CONFIRM_URL,
    LOGIN_URL,
    MY_PAGE_URL,
    PASSWORD_CHANGING_URL,
    PASSWORD_RECOVERY_URL,
    REGISTRATION_CONFIRM_URL,
    REGISTRATION_URL,
    SETTINGS_URL,
    USER_PAGE_URL,
    WORK_DIARY_REPORTS_URL
} from "./constants";
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core";
import SettingsTabsRoute from "./components/security/SettingsTabsRoute";
import MyPage from "./components/user-page/UserPage";
import EmployeesPage from "./components/employees/EmployeesPage";
import {NotFound} from "./components/NotFound";
import WorkDiary from "./components/reports/workdiary/WorkDiary";
import PrivateRoute from "./components/security/PrivateRoute";
import {withRoles} from "./decorators/Authorization";
import PasswordRecovery from "./components/auth/password/RestorePassword";
import PasswordChange from "./components/auth/password/ChangePassword";
import {ApplicationRole} from "./types";
import Registration from "./components/auth/registration/Registration";
import RegistrationConfirm from "./components/auth/registration/RegistrationConfirm";
import InviteConfirm from "./components/auth/invite/InviteConfirm";
import MomentUtils from '@date-io/moment';
import {MuiPickersUtilsProvider} from "material-ui-pickers";
// const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk))); todo uncomment and update Redux-Devtools to > 2.16+ version
const store = createStore(rootReducer, applyMiddleware(thunk));
axios.defaults.baseURL = HOST_URL;

const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
    },
    palette: {
        primary: {
            light: '#757ce8',
            main: '#3f50b5',
            dark: '#002884',
            contrastText: '#fff',
        },
        secondary: {
            light: '#ff7961',
            main: '#f44336',
            dark: '#ba000d',
            contrastText: '#000',
        },
    }
});

// store.subscribe(() => console.log(store.getState()));//todo remove


export class AppRouter extends React.Component<any, any> {
    render() {
        return <MuiThemeProvider theme={theme}>
            <MuiPickersUtilsProvider utils={MomentUtils}>
                <Provider store={store}>
                    <Router history={history}>
                        <MiniDrawer>
                            <Switch>
                                <Redirect exact from="/" to={MY_PAGE_URL}/>
                                <Route path={LOGIN_URL} component={Login}/>
                                <Route exact path={REGISTRATION_URL} component={Registration}/>
                                <Route path={REGISTRATION_CONFIRM_URL} component={RegistrationConfirm}/>
                                <Route path={INVITE_CONFIRM_URL} component={InviteConfirm}/>
                                <Route path={PASSWORD_RECOVERY_URL} component={PasswordRecovery}/>
                                <Route path={PASSWORD_CHANGING_URL} component={PasswordChange}/>
                                <PrivateRoute path={MY_PAGE_URL} component={MyPage}/>
                                <PrivateRoute path={USER_PAGE_URL} component={withRoles(MyPage, [ApplicationRole.ADMIN_APPLICATION, ApplicationRole.ADMIN_PROJECT])}/>
                                <PrivateRoute path={EMPLOYEES_PAGE_URL}
                                              component={withRoles(EmployeesPage, [ApplicationRole.ADMIN_APPLICATION, ApplicationRole.ADMIN_COMPANY, ApplicationRole.ADMIN_DEPARTMENT, ApplicationRole.ADMIN_PROJECT])}/>
                                <PrivateRoute path={WORK_DIARY_REPORTS_URL} component={WorkDiary}/>
                                <PrivateRoute path={SETTINGS_URL} component={SettingsTabsRoute}/>
                                <PrivateRoute component={NotFound}/>
                            </Switch>
                        </MiniDrawer>
                    </Router>
                </Provider>
            </MuiPickersUtilsProvider>
        </MuiThemeProvider>
    }
}