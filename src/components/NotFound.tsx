import React from "react";
import i18next from "i18next";

export const NotFound: React.SFC<any> = (props) => {
    return (
        <h1>{i18next.t("page_not_found", {path:props.location.pathname})}</h1>
    )
};