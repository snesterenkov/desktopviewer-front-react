import  React from "react";
import {ApplicationState} from "../../../../store";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {
    clearAllFiles, clearGoogleDriveFiles, clearLocalStorageFiles,
    getGoogleDriveFiles,
    getLocalStorageFiles
} from "../../../../store/settings/server/files/actions";
import {FilesSettings} from "../../../../store/settings/server/files/types";
import {Button, Grid, LinearProgress, Paper, Typography} from "@material-ui/core";
import i18next from "i18next";

type PropsFromState = {
    googleDriveFiles: FilesSettings
    localStorageFiles: FilesSettings
    isLoading: boolean
    isLoaded: boolean
    message?: string
}

interface Props extends PropsFromState {
    getGoogleDriveFiles: () => any
    getLocalStorageFiles: () => any
    clearAllFiles: () => any
    clearGoogleDriveFiles: () => any
    clearLocalStorageFiles: () => any
    dispatch: any
    history: any
}

class ServerFiles extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
        this.reloadAllFiles();
    }

    reloadAllFiles() {
        this.props.getGoogleDriveFiles();
        this.props.getLocalStorageFiles();
    }

    render() {
        let {googleDriveFiles, localStorageFiles} = this.props;
        return (
            <Paper>
                {this.props.isLoading ? <LinearProgress/> :
                    <Grid container direction={"column"}>
                        <Grid item>
                            <Button color={"primary"} onClick={() => this.reloadAllFiles()}>
                                {i18next.t("reload_all")}
                            </Button>
                            <Button color={"inherit"} onClick={() => this.props.clearAllFiles()}>
                                {i18next.t("clear_all")}
                            </Button>
                        </Grid>
                        <Grid container direction={"row"}>
                            <Grid container direction={"column"}>
                                <Grid item>
                                    <Typography>{i18next.t("storage")}: {googleDriveFiles.storage}</Typography>
                                    {!googleDriveFiles.failed ? <Typography>
                                            {i18next.t("error")}: {googleDriveFiles.failMessage}
                                        </Typography>
                                        :
                                        <>
                                            <Typography>{i18next.t("date")}: {googleDriveFiles.limitDate}</Typography>
                                            <Typography>{i18next.t("old_files")}: {googleDriveFiles.filesCount}</Typography>
                                        </>}
                                </Grid>
                                <Grid item>
                                    <Button color={"primary"} onClick={() => this.props.getGoogleDriveFiles()}>
                                        {i18next.t("reload")}
                                    </Button>
                                    <Button color={"inherit"} onClick={() => this.props.clearGoogleDriveFiles()}>
                                        {i18next.t("clear")}
                                    </Button>
                                </Grid>
                            </Grid>
                            <Grid container direction={"column"}>
                                <Grid item>
                                    <Typography>{i18next.t("storage")}: {localStorageFiles.storage}</Typography>
                                    {localStorageFiles.failed ?
                                        <Typography>{i18next.t("error")}: {localStorageFiles.failMessage}</Typography> :
                                        <>
                                            <Typography>{i18next.t("date")}: {localStorageFiles.limitDate}</Typography>
                                            <Typography>{i18next.t("old_files")}: {localStorageFiles.filesCount}</Typography>
                                        </>}
                                </Grid>
                                <Grid item>
                                    <Button color={"primary"} onClick={() => this.props.getLocalStorageFiles()}>
                                        {i18next.t("reload")}
                                    </Button>
                                    <Button color={"inherit"} onClick={() => this.props.clearLocalStorageFiles()}>
                                        {i18next.t("clear")}
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>}
            </Paper>
        )
    }
}

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        googleDriveFiles: applicationState.serverFiles.googleDriveFiles,
        localStorageFiles: applicationState.serverFiles.localStorageFiles,
        isLoading: applicationState.serverFiles.isLoading,
        isLoaded: applicationState.serverFiles.isLoaded,
        message: applicationState.serverFiles.message
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getGoogleDriveFiles: getGoogleDriveFiles,
    getLocalStorageFiles: getLocalStorageFiles,
    clearAllFiles: clearAllFiles,
    clearGoogleDriveFiles: clearGoogleDriveFiles,
    clearLocalStorageFiles: clearLocalStorageFiles,
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(ServerFiles);