import React from "react";
import {ApplicationState} from "../../../../store";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {getProxySettings, setProxySettings} from "../../../../store/settings/server/proxy/actions";
import {ProxySettings} from "../../../../store/settings/server/proxy/types";
import {Button, Grid, LinearProgress, Paper, TextField} from "@material-ui/core";
import i18next from "i18next";

type PropsFromState = {
    proxySettings: ProxySettings
    isLoading: boolean
    isLoaded: boolean
    message?: string
}

interface Props extends PropsFromState {
    getProxySettings: () => any
    setProxySettings: (proxySettings: ProxySettings) => any
    dispatch: any
    history: any
}

type State = {
    proxySettings: ProxySettings
}

class ServerProxy extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            proxySettings: {...props.proxySettings}
        };
        props.getProxySettings();
    }

    componentWillReceiveProps(props: Props) {
        this.setState({
            proxySettings: props.proxySettings
        });
    }

    handleChangeProxySettings = (event: React.ChangeEvent<HTMLInputElement>) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({...this.state, proxySettings: {...this.state.proxySettings, [name]: value}});
    };

    handleChangeProxyCredentials = (event: React.ChangeEvent<HTMLInputElement>) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({
            ...this.state,
            proxySettings: {
                ...this.state.proxySettings,
                proxyCredentials: {
                    ...this.state.proxySettings.proxyCredentials,
                    [name]: value
                }
            }
        });
    };

    render() {
        return (
            <Paper>
                {this.props.isLoading ? <LinearProgress/> :
                    <Grid container direction={"column"}>
                        <Grid container direction={"column"} spacing={16}>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"proxyHost"}
                                    label={i18next.t("host")}
                                    value={this.state.proxySettings.proxyHost}
                                    onChange={this.handleChangeProxySettings.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"proxyPort"}
                                    label={i18next.t("port")}
                                    value={this.state.proxySettings.proxyPort}
                                    onChange={this.handleChangeProxySettings.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"proxyType"}
                                    label={i18next.t("protocol")}
                                    value={this.state.proxySettings.proxyType}
                                    onChange={this.handleChangeProxySettings.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"nonProxyHosts"}
                                    label={i18next.t("non_proxy")}
                                    value={this.state.proxySettings.nonProxyHosts}
                                    onChange={this.handleChangeProxySettings.bind(this)}/>
                            </Grid>
                        </Grid>
                        <Grid container direction={"column"} spacing={16}>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"username"}
                                    label={i18next.t("login")}
                                    value={this.state.proxySettings.proxyCredentials.username}
                                    onChange={this.handleChangeProxyCredentials.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"password"}
                                    label={i18next.t("password")}
                                    value={this.state.proxySettings.proxyCredentials.password}
                                    onChange={this.handleChangeProxyCredentials.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <Button onClick={() => this.props.setProxySettings(this.state.proxySettings)}>
                                    {i18next.t("save")}
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>}
            </Paper>
        )
    }
}

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        proxySettings: applicationState.serverProxy.proxySettings,
        isLoading: applicationState.serverProxy.isLoading,
        isLoaded: applicationState.serverProxy.isLoaded,
        message: applicationState.serverProxy.message
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getProxySettings: getProxySettings,
    setProxySettings: setProxySettings
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(ServerProxy);