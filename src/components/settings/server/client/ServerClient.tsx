import  React from "react";
import {ApplicationState} from "../../../../store";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {Button, Grid, LinearProgress, Paper, TextField} from "@material-ui/core";
import {getClientSettings, setClientSettings} from "../../../../store/settings/server/client/actions";
import {ClientSettings} from "../../../../store/settings/server/client/types";
import i18next from "i18next";

type PropsFromState = {
    clientSettings: ClientSettings
    isLoading: boolean
    isLoaded: boolean
    message?: string
}

interface Props extends PropsFromState {
    getClientSettings: () => any
    setClientSettings: (clientSettings: ClientSettings) => any
    dispatch: any
    history: any
}

type State = {
    clientSettings: ClientSettings
}

class ServerClient extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            clientSettings: {...props.clientSettings}
        };
        props.getClientSettings();
    }

    componentWillReceiveProps(props: Props) {
        this.setState({
            clientSettings: {...props.clientSettings}
        });
    }

    handleChangeClientSettings = (event: React.ChangeEvent<HTMLInputElement>) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({...this.state, clientSettings: {...this.state.clientSettings, [name]: value}});
    };

    render() {
        return (
            <Paper>
                {this.props.isLoading ? <LinearProgress/> :
                    <Grid container direction={"column"}>
                        <Grid container direction={"column"} spacing={16}>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"registrationConfirmURL"}
                                    label={i18next.t("registration_confirm_address")}
                                    value={this.state.clientSettings.registrationConfirmURL}
                                    onChange={this.handleChangeClientSettings.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"passwordRestoreURL"}
                                    label={i18next.t("password_recovery_address")}
                                    value={this.state.clientSettings.passwordRestoreURL}
                                    onChange={this.handleChangeClientSettings.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"inviteConfirmURL"}
                                    label={i18next.t("invitation_confirm_address")}
                                    value={this.state.clientSettings.inviteConfirmURL}
                                    onChange={this.handleChangeClientSettings.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <Button onClick={() => this.props.setClientSettings(this.state.clientSettings)}>
                                    {i18next.t("save")}
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>}
            </Paper>
        )
    }
}

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        clientSettings: applicationState.serverClient.clientSettings,
        isLoading: applicationState.serverClient.isLoading,
        isLoaded: applicationState.serverClient.isLoaded,
        message: applicationState.serverClient.message
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getClientSettings: getClientSettings,
    setClientSettings: setClientSettings
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(ServerClient);