import  React from "react";
import {AppBar, Tab, Tabs} from "@material-ui/core";
import history from "../../../store/browserHistory";
import {SERVER_CLIENT_URL, SERVER_FILES_URL, SERVER_PROXY_URL, SERVER_STORAGE_URL,} from "../../../constants";
import {Route, Switch} from "react-router";
import ServerStorage from "./storage/ServerStorage";
import ServerProxy from "./proxy/ServerProxy";
import ServerClient from "./client/ServerClient";
import ServerFiles from "./files/ServerFiles";
import i18next from "i18next";

export class ServerSettings extends React.Component<any, any> {
    render() {
        return (
            <div>
                <AppBar position="static">
                    <Tabs value={this.props.match.params.category}>
                        <Tab
                            value={"storage"}
                            label={i18next.t("storages")}
                            onClick={() => history.push(SERVER_STORAGE_URL)}/>
                        <Tab
                            value={"proxy"}
                            label={i18next.t("proxy")}
                            onClick={() => history.push(SERVER_PROXY_URL)}/>
                        <Tab
                            value={"files"}
                            label={i18next.t("files")}
                            onClick={() => history.push(SERVER_FILES_URL)}/>
                        <Tab
                            value={"client"}
                            label={i18next.t("client")}
                            onClick={() => history.push(SERVER_CLIENT_URL)}/>
                    </Tabs>
                </AppBar>
                <Switch>
                    <Route path={SERVER_STORAGE_URL} component={ServerStorage}/>
                    <Route path={SERVER_PROXY_URL} component={ServerProxy}/>
                    <Route path={SERVER_FILES_URL} component={ServerFiles}/>
                    <Route path={SERVER_CLIENT_URL} component={ServerClient}/>
                </Switch>
            </div>
        );
    }
}