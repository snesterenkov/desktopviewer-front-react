import React from "react";
import {ApplicationState} from "../../../../store";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {
    getGoogleDriveParams,
    getLocalStorage,
    getStorages,
    setCurrentStorage, setGoogleDriveParams, setLocalStorageParams
} from "../../../../store/settings/server/storage/actions";
import {GoogleDriveParams, LocalStorageParams, Storage} from "../../../../store/settings/server/storage/types";
import {TextField, Typography, Paper, Grid, MenuItem, Button, LinearProgress} from "@material-ui/core";
import i18next from "i18next";

type PropsFromState = {
    googleDriveParams: GoogleDriveParams
    localStorageParams: LocalStorageParams
    storages: Storage[]
    isLoading: boolean
    isLoaded: boolean
    message?: string
}

interface Props extends PropsFromState {
    getGoogleDrive: () => any
    getLocalStorage: () => any
    getStorages: () => any
    setCurrentStorage: (storage: Storage) => any
    setGoogleDriveParams: (googleDriveParams: GoogleDriveParams) => any
    setLocalStorageParams: (localStorageParams: LocalStorageParams) => any
    dispatch: any
    history: any
}

type State = {
    googleDriveParams: GoogleDriveParams
    localStorageParams: LocalStorageParams
    editingStorageType: string
}

class ServerStorage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            editingStorageType: "GoogleDrive",
            googleDriveParams: props.googleDriveParams,
            localStorageParams: props.localStorageParams
        };
        props.getStorages();
        props.getGoogleDrive();
        props.getLocalStorage();
    }

    componentWillReceiveProps(props: Props) {
        this.setState({
            googleDriveParams: props.googleDriveParams,
            localStorageParams: props.localStorageParams
        });
    }

    handleSelectStorage = (event: React.ChangeEvent<HTMLInputElement>) => {
        let value = event.target.value;
        this.setState({...this.state, editingStorageType: value});
    };

    handleChangeGoogleDrive = (event: React.ChangeEvent<HTMLInputElement>) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({...this.state, googleDriveParams: {...this.state.googleDriveParams, [name]: value}});
    };

    handleChangeLocalStorage = (event: React.ChangeEvent<HTMLInputElement>) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({...this.state, localStorageParams: {...this.state.localStorageParams, [name]: value}});
    };

    handleSetCurrentStorage = () => {
        let newCurrentStorage: Storage = {type: this.state.editingStorageType, current: true};
        this.props.setCurrentStorage(newCurrentStorage);
    };

    render() {
        let currentStorage = this.props.storages.find(storage => storage.current);
        return (
            <Paper>
                {this.props.isLoading ? <LinearProgress/> :
                    <Grid container>
                        <Grid item>
                            <Typography variant="h5">
                                {i18next.t("current_storage_is")} {currentStorage && currentStorage.type}
                            </Typography>
                            <TextField
                                select
                                value={this.state.editingStorageType}
                                onChange={this.handleSelectStorage.bind(this)}>
                                {this.props.storages.map(storage => {
                                    return (
                                        <MenuItem key={storage.type} value={storage.type}>
                                            {storage.type}
                                        </MenuItem>
                                    )
                                })}
                            </TextField>
                            <Button
                                onClick={this.handleSetCurrentStorage.bind(this)}>{i18next.t("set_as_current")}</Button>
                        </Grid>
                        {this.state.editingStorageType == "GoogleDrive" &&
                        <Grid container direction={"column"} spacing={16}>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"serviceAccountId"}
                                    label={i18next.t("service_account_id")}
                                    value={this.state.googleDriveParams.serviceAccountId}
                                    onChange={this.handleChangeGoogleDrive.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"secretFileLocation"}
                                    label={i18next.t("secret_file_location")}
                                    value={this.state.googleDriveParams.secretFileLocation}
                                    onChange={this.handleChangeGoogleDrive.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"storageFolderName"}
                                    label={i18next.t("storage_folder_location")}
                                    value={this.state.googleDriveParams.storageFolderName}
                                    onChange={this.handleChangeGoogleDrive.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"tempFolderName"}
                                    label={i18next.t("temp_folder_location")}
                                    value={this.state.googleDriveParams.tempFolderName}
                                    onChange={this.handleChangeGoogleDrive.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <Button
                                    onClick={() => this.props.setGoogleDriveParams(this.state.googleDriveParams)}>Save</Button>
                            </Grid>
                        </Grid>}
                        {this.state.editingStorageType == "LocalStorage" &&
                        <Grid container direction={"column"} spacing={16}>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"storageFolderName"}
                                    label={i18next.t("storage_folder_location")}
                                    value={this.state.localStorageParams.storageFolderName}
                                    onChange={this.handleChangeLocalStorage.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <TextField
                                    fullWidth
                                    name={"imagesHostURL"}
                                    label={i18next.t("images_host_url")}
                                    value={this.state.localStorageParams.imagesHostURL}
                                    onChange={this.handleChangeLocalStorage.bind(this)}/>
                            </Grid>
                            <Grid item>
                                <Button onClick={() => this.props.setLocalStorageParams(this.state.localStorageParams)}>
                                    {i18next.t("save")}
                                </Button>
                            </Grid>
                        </Grid>}
                    </Grid>}
            </Paper>
        )
    }
}

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        storages: applicationState.serverStorage.storages,
        googleDriveParams: applicationState.serverStorage.googleDriveParams,
        localStorageParams: applicationState.serverStorage.localStorageParams,
        isLoading: applicationState.serverStorage.isLoading,
        isLoaded: applicationState.serverStorage.isLoaded,
        message: applicationState.serverStorage.message,
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getStorages: getStorages,
    getGoogleDrive: getGoogleDriveParams,
    getLocalStorage: getLocalStorage,
    setCurrentStorage: setCurrentStorage,
    setGoogleDriveParams: setGoogleDriveParams,
    setLocalStorageParams: setLocalStorageParams
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(ServerStorage);
