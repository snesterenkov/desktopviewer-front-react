import  React from "react";
import {Table, TableBody, TableCell, TableHead, TableRow, TextField} from "@material-ui/core";
import {getSorting, SortTableCell, stableSort} from "../SortTableCell";
import i18next from "i18next";
import {Column} from "@devexpress/dx-react-grid";

interface Props {
    columns: Column[]
    data: any[]
}

type State = {
    order: 'asc' | 'desc',
    orderBy: string,
    search: string
}

// todo попытаться сделать один компонент таблицы для других таблиц как Companies, Departments ...
export class TableComponent extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            order: 'asc',
            orderBy: '',
            search: ''
        }
    }

    handleRequestSort = (property: string) => {
        const orderBy = property;
        let order: any = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order: order, orderBy: orderBy});
    };

    handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({search: event.target.value})
    };

    filterRows = (rows: Array<any>) => {
        return rows.filter((row) => {
            let matchesItems: string[] = [];
            this.props.columns.forEach(column => matchesItems.push(row[column.name]));
            const regex = new RegExp(this.state.search, 'gi');
            return matchesItems.some(item => regex.test(item));
        });
    };

    render() {
        let {columns, data} = this.props;
        let {order, orderBy} = this.state;

        data = this.filterRows(data);
        data = stableSort(data, getSorting(order, orderBy));
        return (
            <Table>
                <TableHead>
                    <TableRow>
                        <TextField label={i18next.t("search")} onChange={this.handleSearch.bind(this)}
                                   value={this.state.search}/>
                    </TableRow>
                    <TableRow>
                        {columns.map(column => {
                            return (
                                <SortTableCell label={column.title ? column.title : ''}
                                               value={column.name}
                                               order={order}
                                               orderBy={orderBy}
                                               onRequestSort={this.handleRequestSort.bind(this, column.name)}/>
                            )
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map(item => {
                        return (
                            <TableRow key={item.id}>
                                {columns.map(column => {
                                    return (
                                        <TableCell>{item[column.name]}</TableCell>
                                    )
                                })}
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
        )
    }
}