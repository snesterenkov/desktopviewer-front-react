import React from "react";
import {SyntheticEvent} from "react";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {ApplicationState} from "../../store";
import {Project, ProjectWithDetails} from "../../store/settings/project/types";
import {
    Button,
    IconButton,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    MenuItem,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TextField, LinearProgress, TablePagination
} from "@material-ui/core";
import {Status} from "../../types";
import {
    addProject,
    changeProjectStatus,
    deleteProject,
    getProjects,
    updateProject
} from "../../store/settings/project/actions";
import {getOpenDepartments} from "../../store/settings/department/actions";
import {Department} from "../../store/settings/department/types";
import history from "../../store/browserHistory";
import {PROJECT_SETTINGS_URL} from "../../constants";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import PauseIcon from "@material-ui/icons/Pause";
import PlayIcon from "@material-ui/icons/PlayArrow";
import ClosedIcon from "@material-ui/icons/NotInterested";
import GroupIcon from "@material-ui/icons/Group";
import DeleteIcon from "../../../node_modules/@material-ui/icons/Delete";
import {getSorting, SortTableCell, stableSort} from "../SortTableCell";
import {getEmptyProject} from "../../store/settings/project/utils";
import i18next from "i18next";

type PropsFromState = {
    projects: Array<ProjectWithDetails>;
    openDepartments: Array<Department>,
    isLoading: boolean;
    isLoaded: boolean;
    message?: string;
}

interface Props extends PropsFromState {
    getProjects: () => void;
    changeProjectStatus: (projectId: number, projectStatus: Status) => void;
    loadOpenDepartments: () => void,
    addProject: (project: Project) => void;
    updateProject: (project: Project) => void;
    deleteProject: (projectId: number) => void;
    dispatch: any;
    history: any;
}

type State = {
    isEditDialogOpen: boolean,
    project: Project,
    order: 'asc' | 'desc',
    orderBy: string,
    search: string,
    rowsPerPage: number,
    page: number
}

class ProjectSettings extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            isEditDialogOpen: false,
            project: getEmptyProject(),
            order: 'asc',
            orderBy: 'name',
            search: '',
            rowsPerPage: 10,
            page: 0
        };
        this.props.getProjects();
    }

    handleClickOpen = (project?: ProjectWithDetails) => {
        this.setState({project: project ? project : getEmptyProject(), isEditDialogOpen: true});
    };

    handleOnChangeSelect = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const departmentId: number = +event.target.value;
        this.setState({
            project: {
                ...this.state.project,
                departmentId: departmentId
            }
        });
    };

    handleOnChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        let target = event.target;
        this.setState({
            project: {...this.state.project, [target.name]: target.value}
        });
    };

    handleClose = () => {
        this.setState({isEditDialogOpen: false});
    };

    handleSubmit = (event: SyntheticEvent) => {
        event.preventDefault();
        if (this.state.project.id) {
            this.props.updateProject(this.state.project);
        } else {
            this.props.addProject(this.state.project);
        }
        this.setState({isEditDialogOpen: false});
    };

    handleRequestSort = (property: string) => {
        const orderBy = property;
        let order: any = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order: order, orderBy: orderBy});
    };

    handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({search: event.target.value})
    };

    handleChangePage = (event: React.MouseEvent<HTMLButtonElement>, page: number) => {
        this.setState({page: page});
    };

    handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({rowsPerPage: +event.target.value});
    };

    filterRows = (rows: Array<ProjectWithDetails>) => {
        return rows.filter((row: ProjectWithDetails) => {
            let matchesItems: string[] = [];
            matchesItems.push(row.name);
            matchesItems.push(row.status.toString());
            matchesItems.push(row.department.name);
            const regex = new RegExp(this.state.search, 'gi');
            return matchesItems.some(item => regex.test(item));
        });
    };

    render(): React.ReactNode {
        let {order, orderBy, rowsPerPage, page} = this.state;
        let rows: Array<ProjectWithDetails> = this.props.projects.map(project => {
            return {...project, departmentName: project.department.name}
        });
        rows = this.filterRows(rows);
        rows = stableSort(rows, getSorting(order, orderBy));
        return (
            <div>
                {this.props.isLoading ? <LinearProgress/> :
                    <Paper>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>
                                        <TextField
                                            label={i18next.t("search")}
                                            onChange={this.handleSearch}
                                            value={this.state.search}/>
                                    </TableCell>
                                    <TablePagination
                                        rowsPerPageOptions={[10, 25, 50]}
                                        count={rows.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        labelRowsPerPage={i18next.t("rows_per_page")}
                                        backIconButtonProps={{
                                            'title': i18next.t("prev_page"),
                                        }}
                                        nextIconButtonProps={{
                                            'title': i18next.t("next_page"),
                                        }}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}/>
                                </TableRow>
                                <TableRow>
                                    <SortTableCell
                                        label={i18next.t("name")}
                                        value={"name"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("name")}/>
                                    <SortTableCell
                                        label={i18next.t("status")}
                                        value={"status"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("status")}/>
                                    <SortTableCell
                                        label={i18next.t("department")}
                                        value={"departmentName"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("departmentName")}/>
                                    <TableCell>
                                        <IconButton
                                            title={i18next.t("add")}
                                            onClick={() => this.handleClickOpen()}
                                            color="primary">
                                            <AddIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map(project => {
                                    return (
                                        <TableRow key={project.id}>
                                            <TableCell>{project.name}</TableCell>
                                            <TableCell>{project.status}</TableCell>
                                            <TableCell>{project.department!.name}</TableCell>
                                            <TableCell>
                                                {(project.id && project.admin) &&
                                                <IconButton
                                                    title={i18next.t("delete")}
                                                    onClick={() => this.props.deleteProject(project.id!)}
                                                    color="primary">
                                                    <DeleteIcon/>
                                                </IconButton>}

                                                {(project.admin && project.status == Status.OPEN) &&
                                                <IconButton
                                                    title={i18next.t("edit")}
                                                    onClick={() => this.handleClickOpen(project)}
                                                    color="primary">
                                                    <EditIcon/>
                                                </IconButton>}

                                                {(project.admin && project.status == Status.PAUSED && project.parentStatus == Status.OPEN) &&
                                                <IconButton
                                                    title={i18next.t("resume")}
                                                    onClick={() => this.props.changeProjectStatus(project.id!, Status.OPEN)}
                                                    color="primary">
                                                    <PlayIcon/>
                                                </IconButton>}

                                                {(project.admin && project.status == Status.OPEN && project.parentStatus == Status.OPEN) &&
                                                <IconButton
                                                    title={i18next.t("pause")}
                                                    onClick={() => this.props.changeProjectStatus(project.id!, Status.PAUSED)}
                                                    color="secondary">
                                                    <PauseIcon/>
                                                </IconButton>}

                                                {(project.admin && project.status != Status.CLOSED && project.parentStatus != Status.CLOSED) &&
                                                <IconButton
                                                    title={i18next.t("close")}
                                                    onClick={() => this.props.changeProjectStatus(project.id!, Status.CLOSED)}
                                                    color="secondary">
                                                    <ClosedIcon/>
                                                </IconButton>}

                                                {(project.admin && project.status == Status.OPEN && project.parentStatus == Status.OPEN) &&
                                                <IconButton
                                                    title={i18next.t("user_roles")}
                                                    onClick={() => history.push(PROJECT_SETTINGS_URL + `/${project.id}/roles`)}
                                                    color="inherit">
                                                    <GroupIcon/>
                                                </IconButton>}
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>

                    </Paper>}

                {this.state.isEditDialogOpen &&
                <Dialog
                    open={this.state.isEditDialogOpen}
                    onEnter={() => this.props.loadOpenDepartments()}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">{i18next.t("project")}</DialogTitle>
                    <DialogContent>
                        <TextField
                            required
                            value={this.state.project.name}
                            onChange={this.handleOnChange}
                            name="name"
                            autoFocus
                            margin="dense"
                            id="name"
                            label={i18next.t("name")}
                            fullWidth/>
                        <TextField
                            required
                            id="department"
                            name="department"
                            select
                            label={i18next.t("department")}
                            value={this.state.project.departmentId}
                            onChange={this.handleOnChangeSelect}
                            helperText="Please select department"
                            margin="normal"
                            fullWidth>
                            {this.props.openDepartments.map((department: Department) => (
                                <MenuItem key={department.id} value={department.id}>
                                    {department.name}
                                </MenuItem>
                            ))}
                        </TextField>

                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={this.handleSubmit}
                            type="submit"
                            color="primary">
                            {i18next.t("save")}
                        </Button>
                        <Button
                            onClick={this.handleClose}
                            color="primary">
                            {i18next.t("cancel")}
                        </Button>
                    </DialogActions>
                </Dialog>}
            </div>
        )
    }
}

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        isLoading: applicationState.projectSettings.isLoading,
        isLoaded: applicationState.projectSettings.isLoaded,
        projects: applicationState.projectSettings.projects,
        openDepartments: applicationState.departmentSettings.openDepartments!
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getProjects: getProjects,
    changeProjectStatus: changeProjectStatus,
    loadOpenDepartments: getOpenDepartments,
    addProject: addProject,
    updateProject: updateProject,
    deleteProject: deleteProject
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(ProjectSettings);