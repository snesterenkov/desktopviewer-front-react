import React from "react";
import {SyntheticEvent} from "react";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {ApplicationState} from "../../store";
import {
    addApplication,
    changeApplicationsStatus,
    deleteApplication,
    getApplicationsForDepartment,
    getNeutralApplicationsForDepartment,
    updateApplication
} from "../../store/settings/application/actions";
import {ApplicationSettingAction} from "../../store/settings/application/reducer";
import {Application, ApplicationProductiveTypes} from "../../store/settings/application/types";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    IconButton,
    MenuItem,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TextField,
    Typography
} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

import NeutralIcon from "@material-ui/icons/SentimentSatisfied"
import ProductiveIcon from "@material-ui/icons/SentimentVerySatisfied"
import UnproductiveIcon from "@material-ui/icons/SentimentVeryDissatisfied"

import {getDepartments} from "../../store/settings/department/actions";
import {DepartmentWithDetails} from "../../store/settings/department/types";
import {Productivity} from "../../types";
import i18next from "i18next";


type PropsFromState = {
    departments: Array<DepartmentWithDetails>,
    applicationsForDepartment: ApplicationProductiveTypes;
    neutralApplicationsForDepartment: Array<Application>;
    isLoading: boolean;
    isLoaded: boolean;
    message?: string;
}

interface Props extends PropsFromState {
    getApplicationsForDepartment: (departmentId: number) => any;
    getNeutralApplicationsForDepartment: (departmentId: number) => any;
    getDepartments: () => any;
    addApplication: (application: Application) => any;
    updateApplication: (application: Application) => any
    changeApplicationsStatus: (departmentId: number, applications: Array<Application>, status: Productivity) => any;
    deleteApplication: (applications: Array<Application>) => any;
    dispatch: any;
    history: any;
}

type State = {
    isEditDialogOpen: boolean,
    selectedDepartmentId: number,
    application: Application,
}

class ApplicationSettings extends React.Component<Props, State> {

    private initApplication = {name: ""};

    constructor(props: Props) {
        super(props);
        this.props.getDepartments();
        this.state = {
            application: this.initApplication,
            isEditDialogOpen: false,
            selectedDepartmentId: 0
        };
    }

    private reloadData(selectedDepartmentId: number) {
        this.props.getApplicationsForDepartment(selectedDepartmentId)
            .then(() => this.props.getNeutralApplicationsForDepartment(selectedDepartmentId));
    }

    componentDidUpdate(prevProps: Props) {
        if (this.props.departments != prevProps.departments && this.props.departments.length > 0) {
            const selectedDepartmentId = this.props.departments[0].id!;
            this.setState({selectedDepartmentId});
            this.reloadData(selectedDepartmentId);
        }
    }

    handleOnChangeSelect = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const selectedDepartmentId: number = +event.target.value;
        this.setState({selectedDepartmentId});
        this.reloadData(selectedDepartmentId);
    };

    handleClickOpen = (application: Application) => {
        this.setState({application: application.id ? application : this.initApplication, isEditDialogOpen: true});
    };

    handleOnChange(event: React.ChangeEvent<HTMLSelectElement>) {
        let target = event.target;
        this.setState({
            application: {...this.state.application, [target.name]: target.value}
        });
    }

    handleClose = () => {
        this.setState({isEditDialogOpen: false});
    };

    handleSubmit = (event: SyntheticEvent) => {
        event.preventDefault();
        if (this.state.application.id) {
            this.props.updateApplication(this.state.application);
        } else {
            this.props.addApplication(this.state.application);
        }
        this.reloadData(this.state.selectedDepartmentId);
        this.setState({isEditDialogOpen: false});
    };

    createTable(applications: Array<Application>, status: Productivity): React.ReactNode {
        return (<Grid item xs={3}>
                <Typography> {i18next.t("status")}</Typography>
                <Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>{i18next.t("name")}</TableCell>
                                <TableCell>
                                    {i18next.t("actions")}
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {applications ? applications.map(application => {
                                return (
                                    <TableRow key={application.id}>
                                        <TableCell>{application.name}</TableCell>
                                        <TableCell>
                                            <IconButton
                                                title={i18next.t("edit")}
                                                onClick={this.handleClickOpen.bind(this, application)}>
                                                <EditIcon/>
                                            </IconButton>
                                            {status != Productivity.PRODUCTIVE &&
                                            <IconButton
                                                title={i18next.t("assign_productive")}
                                                color="primary"
                                                onClick={() => {
                                                    this.props.changeApplicationsStatus(this.state.selectedDepartmentId, [application], Productivity.PRODUCTIVE).then(() => this.reloadData(this.state.selectedDepartmentId));
                                                }}>
                                                <ProductiveIcon/>
                                            </IconButton>}
                                            {status != Productivity.NEUTRAL &&
                                            <IconButton
                                                title={i18next.t("assign_neutral")}
                                                onClick={() => {
                                                    this.props.changeApplicationsStatus(this.state.selectedDepartmentId, [application], Productivity.NEUTRAL).then(() => this.reloadData(this.state.selectedDepartmentId));
                                                }}>
                                                <NeutralIcon/>
                                            </IconButton>}
                                            {status != Productivity.UNPRODUCTIVE &&
                                            <IconButton
                                                title={i18next.t("assign_unproductive")}
                                                color="secondary"
                                                onClick={() => {
                                                    this.props.changeApplicationsStatus(this.state.selectedDepartmentId, [application], Productivity.UNPRODUCTIVE).then(() => this.reloadData(this.state.selectedDepartmentId));
                                                }}>
                                                <UnproductiveIcon/>
                                            </IconButton>}
                                            <IconButton
                                                title={i18next.t("delete")}
                                                onClick={() => this.props.deleteApplication([application]).then(() => this.reloadData(this.state.selectedDepartmentId))}>
                                                <DeleteIcon/>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                );
                            }) : []}
                        </TableBody>
                    </Table>
                </Paper>
            </Grid>
        )
    }

    render(): React.ReactNode {
        let neutralApplications: Array<Application> = this.props.neutralApplicationsForDepartment;
        let unproductiveApplications: Array<Application> = this.props.applicationsForDepartment.UNPRODUCTIVE;
        let productiveApplications: Array<Application> = this.props.applicationsForDepartment.PRODUCTIVE;

        return (//TODO 08.10.2018 вынести таблицу в отдельнй компонент
            <div>
                <Paper>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="center">
                        <Grid container item
                              direction="row"
                              justify="center"
                              xs={3}
                              alignItems="center">
                            <Grid item>
                                <TextField
                                    fullWidth
                                    required
                                    id="department"
                                    name="department"
                                    select
                                    label={i18next.t("department")}
                                    value={this.state.selectedDepartmentId}
                                    onChange={this.handleOnChangeSelect.bind(this)}
                                    margin="normal">
                                    {this.props.departments && this.props.departments!.map((department: DepartmentWithDetails) => (
                                        <MenuItem key={department.id} value={department.id}>
                                            {department.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </Grid>
                            <Grid item>
                                <Button color="primary" onClick={this.handleClickOpen.bind(this)}>
                                    {i18next.t("add")}
                                </Button>
                            </Grid>
                        </Grid>

                        <Grid container
                              spacing={24}
                              direction="row"
                              justify="center"
                              alignItems="flex-start">
                            {this.createTable(productiveApplications, Productivity.PRODUCTIVE)}
                            {this.createTable(neutralApplications, Productivity.NEUTRAL)}
                            {this.createTable(unproductiveApplications, Productivity.UNPRODUCTIVE)}
                        </Grid>

                    </Grid>
                </Paper>

                {this.state.isEditDialogOpen &&
                <Dialog
                    open={this.state.isEditDialogOpen}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">{i18next.t("application")}</DialogTitle>
                    <DialogContent>
                        <TextField
                            required
                            value={this.state.application.name}
                            onChange={this.handleOnChange.bind(this)}
                            name="name"
                            autoFocus
                            margin="dense"
                            id="name"
                            label={i18next.t("application")}
                            fullWidth
                        />

                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={this.handleSubmit}
                            type="submit"
                            color="primary">
                            {i18next.t("save")}
                        </Button>
                        <Button
                            onClick={this.handleClose}
                            color="primary">
                            {i18next.t("cancel")}
                        </Button>
                    </DialogActions>
                </Dialog>}
            </div>
        )
    }
}

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        isLoading: applicationState.applicationSettings.isLoading,
        isLoaded: applicationState.applicationSettings.isLoaded,
        departments: applicationState.departmentSettings.departments,
        applicationsForDepartment: applicationState.applicationSettings.applicationsForDepartment as ApplicationProductiveTypes,
        neutralApplicationsForDepartment: applicationState.applicationSettings.neutralApplicationsForDepartment,
    }
}

const mapDispatchToProps = (dispatch: Dispatch<ApplicationSettingAction>) => bindActionCreators({
    getDepartments: getDepartments,
    addApplication: addApplication,
    updateApplication: updateApplication,
    changeApplicationsStatus: changeApplicationsStatus,
    deleteApplication: deleteApplication,
    getApplicationsForDepartment: getApplicationsForDepartment,
    getNeutralApplicationsForDepartment: getNeutralApplicationsForDepartment
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(ApplicationSettings);