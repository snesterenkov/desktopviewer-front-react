import React from "react";
import {SyntheticEvent} from "react";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {ApplicationState} from "../../store";
import {Department, DepartmentWithDetails} from "../../store/settings/department/types";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton, LinearProgress,
    MenuItem,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TablePagination,
    TableRow,
    TextField
} from "@material-ui/core";
import {Status} from "../../types";
import {
    addDepartment,
    changeDepartmentStatus,
    deleteDepartment,
    getDepartments,
    updateDepartment
} from "../../store/settings/department/actions";
import {Company} from "../../store/settings/company/types";
import {getOpenCompanies} from "../../store/settings/company/actions";
import history from "../../store/browserHistory";
import {DEPARTMENT_SETTINGS_URL} from "../../constants";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import PlayIcon from "@material-ui/icons/PlayArrow";
import PauseIcon from "@material-ui/icons/Pause";
import ClosedIcon from "@material-ui/icons/NotInterested";
import GroupIcon from "@material-ui/icons/Group";
import {getSorting, SortTableCell, stableSort} from "../SortTableCell";
import {getEmptyDepartment} from "../../store/settings/department/utils";
import i18next from "i18next";

type PropsFromState = {
    departments: Array<DepartmentWithDetails>;
    openCompanies: Array<Company>,
    isLoading: boolean;
    isLoaded: boolean;
    message?: string;
}

interface Props extends PropsFromState {
    getDepartments: () => any;
    changeDepartmentStatus: (departmentId: number, departmentStatus: Status) => any;
    loadOpenCompanies: () => any,
    addDepartment: (department: Department) => any;
    updateDepartment: (department: Department) => any;
    deleteDepartment: (departmentId: number) => any;
    dispatch: any;
    history: any;
}

type State = {
    isEditDialogOpen: boolean,
    department: Department,
    order: 'asc' | 'desc',
    orderBy: string,
    search: string,
    rowsPerPage: number,
    page: number
}

class DepartmentSettings extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            isEditDialogOpen: false,
            department: getEmptyDepartment(),
            order: 'asc',
            orderBy: 'name',
            search: '',
            rowsPerPage: 10,
            page: 0
        };
        this.props.getDepartments();
    }


    handleClickOpen = (department?: DepartmentWithDetails) => {
        this.setState({department: department ? department : getEmptyDepartment(), isEditDialogOpen: true});
    };

    handleOnChangeSelect = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const companyId: number = +event.target.value;
        this.setState({
            department: {
                ...this.state.department,
                companyId: companyId
            }
        });
    };

    handleOnChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        let target = event.target;
        this.setState({
            department: {...this.state.department, [target.name]: target.value}
        });
    };

    handleClose = () => {
        this.setState({isEditDialogOpen: false});
    };

    handleSubmit = (event: SyntheticEvent) => {
        event.preventDefault();
        if (this.state.department.id) {
            this.props.updateDepartment(this.state.department);
        } else {
            this.props.addDepartment(this.state.department);
        }
        this.setState({isEditDialogOpen: false});
    };

    handleRequestSort = (property: string) => {
        const orderBy = property;
        let order: any = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order: order, orderBy: orderBy});
    };

    handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({search: event.target.value})
    };

    handleChangePage = (event: React.MouseEvent<HTMLButtonElement>, page: number) => {
        this.setState({page: page});
    };

    handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({rowsPerPage: +event.target.value});
    };

    filterRows = (rows: Array<DepartmentWithDetails>) => {
        return rows.filter((row: DepartmentWithDetails) => {
            let matchesItems: string[] = [];
            matchesItems.push(row.name);
            matchesItems.push(row.status.toString());
            matchesItems.push(row.company.name);
            const regex = new RegExp(this.state.search, 'gi');
            return matchesItems.some(item => regex.test(item));
        });
    };

    render(): React.ReactNode {
        let {order, orderBy, rowsPerPage, page} = this.state;
        let rows: Array<DepartmentWithDetails> = this.props.departments.map(department => {
            return {...department, companyName: department.company.name}
        });
        rows = this.filterRows(rows);
        rows = stableSort(rows, getSorting(order, orderBy));
        return (
            <div>
                {this.props.isLoading ? <LinearProgress/> :
                    <Paper>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>
                                        <TextField
                                            label={i18next.t("search")}
                                            onChange={this.handleSearch}
                                            value={this.state.search}/>
                                    </TableCell>
                                    <TablePagination
                                        rowsPerPageOptions={[10, 25, 50]}
                                        count={rows.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        labelRowsPerPage={i18next.t("rows_per_page")}
                                        backIconButtonProps={{
                                            'aria-describedby': i18next.t("prev_page"),
                                        }}
                                        nextIconButtonProps={{
                                            'aria-describedby': i18next.t("next_page"),
                                        }}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}/>
                                </TableRow>
                                <TableRow>
                                    <SortTableCell
                                        label={i18next.t("name")}
                                        value={"name"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("name")}/>
                                    <SortTableCell
                                        label={i18next.t("status")}
                                        value={"status"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("status")}/>
                                    <SortTableCell
                                        label={i18next.t("company")}
                                        value={"companyName"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("companyName")}/>
                                    <TableCell>
                                        <IconButton
                                            title={i18next.t("add")}
                                            onClick={() => this.handleClickOpen()}
                                            color="primary">
                                            <AddIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map(department => {
                                        return (
                                            <TableRow key={department.id}>
                                                <TableCell>{department.name}</TableCell>
                                                <TableCell>{department.status}</TableCell>
                                                <TableCell>{department.company!.name}</TableCell>
                                                <TableCell>
                                                    {(department.id! && department.admin) &&
                                                    <IconButton
                                                        title={i18next.t("delete")}
                                                        onClick={() => this.props.deleteDepartment(department.id!)}
                                                        color="primary">
                                                        <DeleteIcon/>
                                                    </IconButton>}

                                                    {(department.admin && department.status == Status.OPEN) &&
                                                    <IconButton
                                                        title={i18next.t("edit")}
                                                        onClick={() => this.handleClickOpen(department)}
                                                        color="primary">
                                                        <EditIcon/>
                                                    </IconButton>}

                                                    {(department.admin && department.status == Status.PAUSED && department.parentStatus == Status.OPEN) &&
                                                    <IconButton
                                                        title={i18next.t("resume")}
                                                        onClick={() => this.props.changeDepartmentStatus(department.id!, Status.OPEN)}
                                                        color="primary">
                                                        <PlayIcon/>
                                                    </IconButton>}

                                                    {(department.admin && department.status == Status.OPEN && department.parentStatus == Status.OPEN) &&
                                                    <IconButton
                                                        title={i18next.t("pause")}
                                                        onClick={() => this.props.changeDepartmentStatus(department.id!, Status.PAUSED)}
                                                        color="secondary">
                                                        <PauseIcon/>
                                                    </IconButton>}

                                                    {(department.admin && department.status != Status.CLOSED && department.parentStatus != Status.CLOSED) &&
                                                    <IconButton
                                                        title={i18next.t("close")}
                                                        onClick={() => this.props.changeDepartmentStatus(department.id!, Status.CLOSED)}
                                                        color="secondary">
                                                        <ClosedIcon/>
                                                    </IconButton>}

                                                    {(department.admin && department.status == Status.OPEN && department.parentStatus == Status.OPEN) &&
                                                    <IconButton
                                                        title={i18next.t("user_roles")}
                                                        onClick={() => history.push(DEPARTMENT_SETTINGS_URL + `/${department.id}/roles`)}
                                                        color="inherit">
                                                        <GroupIcon/>
                                                    </IconButton>}
                                                </TableCell>
                                            </TableRow>)
                                    })}
                            </TableBody>
                        </Table>
                    </Paper>}

                {this.state.isEditDialogOpen &&
                <Dialog
                    open={this.state.isEditDialogOpen}
                    onEnter={() => this.props.loadOpenCompanies()}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">{i18next.t("department")}</DialogTitle>
                    <DialogContent>
                        <TextField
                            required
                            id="name"
                            name="name"
                            autoFocus
                            margin="dense"
                            label={i18next.t("name")}
                            value={this.state.department.name}
                            onChange={this.handleOnChange}
                            fullWidth/>
                        <TextField
                            required
                            id="company"
                            name="company"
                            margin="normal"
                            select
                            label={i18next.t("company")}
                            value={this.state.department.companyId}
                            onChange={this.handleOnChangeSelect}
                            fullWidth>
                            {this.props.openCompanies.map((company: Company) => (
                                <MenuItem
                                    key={company.id}
                                    value={company.id}>
                                    {company.name}
                                </MenuItem>
                            ))}
                        </TextField>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={this.handleSubmit}
                            type="submit"
                            color="primary">
                            {i18next.t("save")}
                        </Button>
                        <Button
                            onClick={this.handleClose}
                            color="primary">
                            {i18next.t("cancel")}
                        </Button>
                    </DialogActions>
                </Dialog>}

            </div>
        )
    }
}

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        isLoading: applicationState.departmentSettings.isLoading,
        isLoaded: applicationState.departmentSettings.isLoaded,
        departments: applicationState.departmentSettings.departments,
        openCompanies: applicationState.companySettings.openCompanies!
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getDepartments: getDepartments,
    changeDepartmentStatus: changeDepartmentStatus,
    loadOpenCompanies: getOpenCompanies,
    addDepartment: addDepartment,
    updateDepartment: updateDepartment,
    deleteDepartment: deleteDepartment
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(DepartmentSettings);