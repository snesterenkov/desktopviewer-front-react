import React from "react";
import {SyntheticEvent} from "react";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {ApplicationState} from "../../store";
import {Company, CompanyWithDetails} from "../../store/settings/company/types";
import {
    addCompany,
    changeCompanyStatus,
    deleteCompany,
    getCompanies,
    updateCompany
} from "../../store/settings/company/actions";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton, LinearProgress,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead, TablePagination,
    TableRow,
    TextField
} from "@material-ui/core";
import {convertMinutesToTime} from "../../utils/DateTimeUtil";
import {COMPANY_SETTINGS_URL} from "../../constants";
import history from "../../store/browserHistory";
import {Status} from "../../types";

AddIcon;
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import PauseIcon from "@material-ui/icons/Pause";
import PlayIcon from "@material-ui/icons/PlayArrow";
import ClosedIcon from "@material-ui/icons/NotInterested";
import GroupIcon from "@material-ui/icons/Group";
import {getSorting, SortTableCell, stableSort} from "../SortTableCell";
import {getEmptyCompany} from "../../store/settings/company/utils";
import i18next from "i18next";

type PropsFromState = {
    companies: Array<CompanyWithDetails>;
    isLoading: boolean;
    isLoaded: boolean;
    message?: string;
}

interface Props extends PropsFromState {
    getCompanies: () => void;
    changeCompanyStatus: (companyId: number, companyStatus: string) => void;
    addCompany: (company: Company) => void;
    updateCompany: (company: Company) => void;
    deleteCompany: (companyId: number) => void;
    dispatch: any;
    history: any;
}

type State = {
    isEditDialogOpen: boolean;
    company: Company;
    order: 'asc' | 'desc',
    orderBy: string,
    search: string,
    rowsPerPage: number,
    page: number
}

class CompanySettings extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            isEditDialogOpen: false,
            company: getEmptyCompany(),
            order: 'asc',
            orderBy: 'name',
            search: '',
            rowsPerPage: 10,
            page: 0
        };
        this.props.getCompanies();
    }

    handleClickOpen = (company?: CompanyWithDetails) => {
        this.setState({company: company ? company : getEmptyCompany()});
        this.setState({isEditDialogOpen: true});
    };

    handleOnChangeCompany = (event: React.ChangeEvent<HTMLSelectElement>) => {
        let target = event.target;
        this.setState({
            company: {...this.state.company, [target.name]: target.value}
        });
    };

    handleClose = () => {
        this.setState({isEditDialogOpen: false});
    };

    handleSubmit = (event: SyntheticEvent) => {
        event.preventDefault();
        if (this.state.company.id) {
            this.props.updateCompany(this.state.company);
        } else {
            this.props.addCompany(this.state.company);
        }
        this.setState({isEditDialogOpen: false});
    };

    handleRequestSort = (property: string) => {
        const orderBy = property;
        let order: any = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order: order, orderBy: orderBy});
    };

    handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({search: event.target.value})
    };

    handleChangePage = (event: React.MouseEvent<HTMLButtonElement>, page: number) => {
        this.setState({page: page});
    };

    handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({rowsPerPage: +event.target.value});
    };

    filterRows = (rows: Array<CompanyWithDetails>) => {
        return rows.filter((row: CompanyWithDetails) => {
            let matchesItems: string[] = [];
            matchesItems.push(row.name);
            matchesItems.push(row.status.toString());
            matchesItems.push(row.workdayStart.toString());
            matchesItems.push(row.workdayDelay.toString());
            matchesItems.push(row.workdayLength.toString());
            const regex = new RegExp(this.state.search, 'gi');
            return matchesItems.some(item => regex.test(item));
        });
    };

    render(): React.ReactNode {
        let {order, orderBy, rowsPerPage, page} = this.state;
        let rows: Array<CompanyWithDetails> = this.filterRows(this.props.companies);
        rows = stableSort(rows, getSorting(order, orderBy));
        return (
            <div>
                {this.props.isLoading ? <LinearProgress/> :
                    <Paper>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>
                                        <TextField
                                            label={i18next.t("search")}
                                            onChange={this.handleSearch}
                                            value={this.state.search}/>
                                    </TableCell>
                                    <TablePagination
                                        rowsPerPageOptions={[10, 25, 50]}
                                        count={rows.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        labelRowsPerPage={i18next.t("rows_per_page")}
                                        backIconButtonProps={{
                                            'title': i18next.t("prev_page"),
                                        }}
                                        nextIconButtonProps={{
                                            'title': i18next.t("next_page"),
                                        }}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}/>
                                </TableRow>
                                <TableRow>
                                    <SortTableCell
                                        label={i18next.t("name")}
                                        value={"name"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("name")}/>
                                    <SortTableCell
                                        label={i18next.t("status")}
                                        value={"status"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("status")}/>
                                    <SortTableCell
                                        label={i18next.t("workday_start")}
                                        value={"workdayStart"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("workdayStart")}/>
                                    <SortTableCell
                                        label={i18next.t("workday_length")}
                                        value={"workdayLength"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("workdayLength")}/>
                                    <SortTableCell
                                        label={i18next.t("workday_delay")}
                                        value={"workdayDelay"}
                                        order={order}
                                        orderBy={orderBy}
                                        onRequestSort={() => this.handleRequestSort("workdayDelay")}/>
                                    <TableCell>
                                        <IconButton
                                            title={i18next.t("add")}
                                            onClick={() => this.handleClickOpen()}
                                            color="primary">
                                            <AddIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map(company => {
                                    return (
                                        <TableRow key={company.id}>
                                            <TableCell>{company.name}</TableCell>
                                            <TableCell>{company.status}</TableCell>
                                            <TableCell>{company.workdayStart}</TableCell>
                                            <TableCell>{convertMinutesToTime(company.workdayLength)}</TableCell>
                                            <TableCell>{convertMinutesToTime(company.workdayDelay)}</TableCell>
                                            <TableCell>
                                                {(company.admin && company.id) &&
                                                <IconButton
                                                    title={i18next.t("delete")}
                                                    onClick={() => this.props.deleteCompany(company.id!)}
                                                    color={"primary"}>
                                                    <DeleteIcon/>
                                                </IconButton>}

                                                {(company.admin && company.status == Status.OPEN) &&
                                                <IconButton
                                                    title={i18next.t("edit")}
                                                    onClick={() => this.handleClickOpen(company)}
                                                    color={"primary"}>
                                                    <EditIcon/>
                                                </IconButton>}

                                                {(company.admin && company.status == Status.PAUSED) &&
                                                <IconButton
                                                    title={i18next.t("resume")}
                                                    onClick={() => this.props.changeCompanyStatus(company.id!, Status.OPEN)}
                                                    color="primary">
                                                    <PlayIcon/>
                                                </IconButton>}

                                                {(company.admin && company.status == Status.OPEN) &&
                                                <IconButton
                                                    title={i18next.t("pause")}
                                                    onClick={() => this.props.changeCompanyStatus(company.id!, Status.PAUSED)}
                                                    color="secondary">
                                                    <PauseIcon/>
                                                </IconButton>}

                                                {(company.admin && company.status != Status.CLOSED) &&
                                                <IconButton
                                                    title={i18next.t("close")}
                                                    onClick={() => this.props.changeCompanyStatus(company.id!, Status.CLOSED)}
                                                    color="secondary">
                                                    <ClosedIcon/>
                                                </IconButton>}

                                                {(company.admin && company.status == Status.OPEN) &&
                                                <IconButton
                                                    title={i18next.t("user_roles")}
                                                    onClick={() => history.push(COMPANY_SETTINGS_URL + `/${company.id}/roles`)}>
                                                    <GroupIcon/>
                                                </IconButton>}
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </Paper>}

                <Dialog
                    open={this.state.isEditDialogOpen}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">{i18next.t("company")}</DialogTitle>
                    <DialogContent>
                        <TextField
                            required
                            value={this.state.company.name}
                            onChange={this.handleOnChangeCompany}
                            name="name"
                            autoFocus
                            margin="dense"
                            id="name"
                            label={i18next.t("name")}
                            fullWidth/>

                        <TextField
                            required
                            type={"time"}
                            value={this.state.company.workdayStart}
                            onChange={this.handleOnChangeCompany}
                            name="workdayStart"
                            margin="dense"
                            id="workday_start"
                            label={i18next.t("workday_start")}
                            fullWidth/>

                        <TextField
                            required
                            type={"number"}
                            value={this.state.company.workdayLength}
                            onChange={this.handleOnChangeCompany}
                            name="workdayLength"
                            margin="dense"
                            id="workday_length"
                            label={i18next.t("workday_length")}
                            fullWidth/>

                        <TextField
                            value={this.state.company.workdayDelay}
                            onChange={this.handleOnChangeCompany}
                            name="workdayDelay"
                            margin="dense"
                            id="workday_delay"
                            label={i18next.t("workday_delay")}
                            fullWidth/>

                        <TextField
                            value={this.state.company.screenshotsSavingMonth}
                            onChange={this.handleOnChangeCompany}
                            name="screenshotsSavingMonth"
                            type="number"
                            margin="dense"
                            id="screenshots_store_time"
                            label={i18next.t("screenshots_store_time")}
                            fullWidth/>

                    </DialogContent>
                    <DialogActions>
                        <Button
                            type="submit"
                            color="primary"
                            onClick={this.handleSubmit}>
                            {i18next.t("save")}
                        </Button>
                        <Button
                            onClick={this.handleClose}
                            color="primary">
                            {i18next.t("cancel")}
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        isLoading: applicationState.companySettings.isLoading,
        isLoaded: applicationState.companySettings.isLoaded,
        companies: applicationState.companySettings.companies
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getCompanies: getCompanies,
    changeCompanyStatus: changeCompanyStatus,
    addCompany: addCompany,
    updateCompany: updateCompany,
    deleteCompany: deleteCompany,
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(CompanySettings);