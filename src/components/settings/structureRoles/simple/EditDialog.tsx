import React from "react";
import {
    Button,
    Checkbox, Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormControlLabel,
    FormLabel
} from "@material-ui/core";
import {Role} from "../../../../store/settings/role/types";
import {User} from "../../../../types";
import i18next from "i18next";

interface Props {
    open: boolean,
    onClose: () => any,
    onSubmit: (selectedRoles: Role[]) => any,
    user: User,
    availableRoles: Role[],
    userRoles: Role[]
}

interface State {
    selectedRoles: Role[]
}

export class EditDialog extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            selectedRoles: props.userRoles
        }
    }

    componentWillReceiveProps(props: Props) {
        this.setState({selectedRoles: props.userRoles})
    }

    isChecked = (role: Role): boolean => {
        return this.state.selectedRoles.some(selectedRole => selectedRole.id == role.id)
    };

    handleOnSelect = (event: React.ChangeEvent<HTMLInputElement>) => {
        let target = event.target;
        let value = this.props.availableRoles.find(role => role.id.toString() == target.value);
        if (value) {
            if (target.checked) {
                this.setState({selectedRoles: [...this.state.selectedRoles, value]});
            } else {
                this.setState({
                    selectedRoles: this.state.selectedRoles.filter(selectedRole => selectedRole.id != value!.id)
                });
            }
        }
    };

    render() {
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={() => this.props.onClose}
                    aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">
                        {this.props.user.lastName + ' ' + this.props.user.firstName}
                    </DialogTitle>
                    <DialogContent>
                        <FormControl component="fieldset">
                            <FormLabel component="legend">{i18next.t("roles")}</FormLabel>
                            {this.props.availableRoles.map(role => {
                                return (
                                    <FormControlLabel
                                        key={role.id}
                                        control={
                                            <Checkbox
                                                name='role'
                                                checked={this.isChecked(role)}
                                                onChange={this.handleOnSelect}
                                                value={role.id.toString()}/>
                                        }
                                        label={role.name}/>
                                )
                            })}
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={() => this.props.onSubmit(this.state.selectedRoles)}
                            color="primary">
                            {i18next.t("save")}
                        </Button>
                        <Button
                            onClick={() => this.props.onClose()}
                            color="primary">
                            {i18next.t("cancel")}
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}