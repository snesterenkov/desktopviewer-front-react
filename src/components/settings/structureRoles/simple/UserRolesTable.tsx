import {Role, UserRole} from "../../../../store/settings/role/types";
import React from "react";
import {
    IconButton,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TablePagination,
    TableRow,
    TextField
} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import {getSorting, SortTableCell, stableSort} from "../../../SortTableCell";
import {User} from "../../../../types";
import i18next from "i18next";

interface Props {
    userRoles: UserRole[],
    owner: User,
    onDelete: (userLogin: string) => any,
    onEdit: (userLogin: string) => any
}

type State = {
    order: 'asc' | 'desc',
    orderBy: string,
    search: string,
    rowsPerPage: number,
    page: number
}

type Row = {
    login: string, lastName: string, firstName: string, roles: Role[]
}

export class UserRolesTable extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            order: 'asc',
            orderBy: 'firstName',
            search: '',
            rowsPerPage: 10,
            page: 0
        }
    }

    handleRequestSort = (property: string) => {
        const orderBy = property;
        let order: any = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order: order, orderBy: orderBy});
    };

    handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({search: event.target.value})
    };

    handleChangePage = (event: React.MouseEvent<HTMLButtonElement>, page: number) => {
        this.setState({page: page});
    };

    handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({rowsPerPage: +event.target.value});
    };


    filterRows = (rows: Array<Row>) => {
        return rows.filter((row: Row) => {
            let matchesItems: string[] = [];
            matchesItems.push(row.firstName);
            matchesItems.push(row.lastName);
            row.roles.forEach(role => matchesItems.push(role.name));
            const regex = new RegExp(this.state.search, 'gi');
            return matchesItems.some(item => regex.test(item));
        });
    };

    render() {
        let {order, orderBy, rowsPerPage, page} = this.state;
        let {owner} = this.props;

        let rows: Row[] = [];
        this.props.userRoles.forEach((userRole) => {
            let user = rows.find(row => row.login == userRole.user.login);
            if (user) {
                user.roles.push(userRole.role)
            } else {
                rows.push({
                    login: userRole.user.login,
                    firstName: userRole.user.firstName,
                    lastName: userRole.user.lastName,
                    roles: [userRole.role]
                });
            }
        });
        rows = this.filterRows(rows);
        rows = stableSort(rows, getSorting(order, orderBy));

        let user: User = JSON.parse(localStorage.getItem("user")!);
        return (
            <div>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <TextField
                                    label={i18next.t("search")}
                                    onChange={this.handleSearch}
                                    value={this.state.search}/>
                            </TableCell>
                            <TablePagination
                                rowsPerPageOptions={[10, 25, 50]}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                labelRowsPerPage={i18next.t("rows_per_page")}
                                backIconButtonProps={{
                                    'title': i18next.t("prev_page"),
                                }}
                                nextIconButtonProps={{
                                    'title': i18next.t("next_page"),
                                }}
                                onChangePage={this.handleChangePage}
                                onChangeRowsPerPage={this.handleChangeRowsPerPage}/>
                        </TableRow>
                        <TableRow>
                            <SortTableCell
                                label={i18next.t("first_name")}
                                value={"firstName"}
                                order={order}
                                orderBy={orderBy}
                                onRequestSort={() => this.handleRequestSort("firstName")}/>
                            <SortTableCell
                                label={i18next.t("last_name")}
                                value={"lastName"}
                                order={order}
                                orderBy={orderBy}
                                onRequestSort={() => this.handleRequestSort("lastName")}/>
                            <TableCell>{i18next.t("roles")}</TableCell>
                            <TableCell/>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
                            return (
                                <TableRow key={row.login}>
                                    <TableCell>{row.firstName}</TableCell>
                                    <TableCell>{row.lastName}</TableCell>
                                    <TableCell>{row.roles.map(role => role.name).join(', ')}</TableCell>
                                    <TableCell>
                                        <IconButton
                                            title={i18next.t("delete")}
                                            color={"primary"}
                                            disabled={row.login == user.login || row.login == owner.login}
                                            onClick={() => this.props.onDelete(row.login)}>
                                            <DeleteIcon/>
                                        </IconButton>
                                        <IconButton
                                            title={i18next.t("edit")}
                                            color={"primary"}
                                            disabled={row.login == user.login || row.login == owner.login}
                                            onClick={() => this.props.onEdit(row.login)}>
                                            <EditIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>

            </div>
        )
    }
}