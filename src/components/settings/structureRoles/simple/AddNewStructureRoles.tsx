import React from "react";
import {Checkbox, FormControlLabel, FormLabel, Grid, GridList, IconButton} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import InviteMail from "@material-ui/icons/ContactMail"
import {User} from "../../../../types";
import {Role} from "../../../../store/settings/role/types";
import i18next from "i18next";

interface Props {
    users: User[],
    roles: Role[],
    onAdd: (selectedUsers: Array<User>, selectedRoles: Array<Role>) => any,
    onInviteAdd: () => void
}

type State = {
    selectedUsers: Array<User>;
    selectedRoles: Array<Role>;
}

const styles = {
    users: {height: "60vh"},
    roles: {height: "14vh"}
};

export class AddNewStructureRoles extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            selectedUsers: [],
            selectedRoles: []
        }
    }

    handleAdd = () => {
        this.props.onAdd(this.state.selectedUsers, this.state.selectedRoles);
        this.setState({selectedUsers: [], selectedRoles: []})
    };

    handleSelectUser = (event: React.ChangeEvent<HTMLInputElement>) => {
        let target = event.target;

        let selectedList = [...this.state.selectedUsers];
        let value = this.props.users.find(user => user.id.toString() == target.value);

        if (value) {
            if (target.checked) {
                selectedList.push(value)
            } else {
                let index = selectedList.indexOf(value);
                selectedList.splice(index, 1)
            }
            this.setState({selectedUsers: selectedList})
        }
    };

    handleSelectRole = (event: React.ChangeEvent<HTMLInputElement>) => {
        let target = event.target;

        let selectedList = [...this.state.selectedRoles];
        let value = this.props.roles.find(role => role.id.toString() == target.value);

        if (value) {
            if (target.checked) {
                selectedList.push(value)
            } else {
                let index = selectedList.indexOf(value);
                selectedList.splice(index, 1)
            }
            this.setState({selectedRoles: selectedList})
        }
    };

    isCheckedRole = (role: Role): boolean => {
        return this.state.selectedRoles.some(selectedRole => selectedRole == role);
    };

    isCheckedUser = (user: User): boolean => {
        return this.state.selectedUsers.some(selectedUser => selectedUser == user);
    };

    render() {
        return (
            <div>
                <IconButton
                    title={i18next.t("add")}
                    color="primary"
                    onClick={this.handleAdd}>
                    <AddIcon/>
                </IconButton>
                <IconButton
                    title={i18next.t("create_invite")}
                    color="primary"
                    onClick={this.props.onInviteAdd}>
                    <InviteMail/>
                </IconButton>
                <br/>
                <Grid
                    container
                    direction="column">
                    <Grid item>
                        <FormLabel>{i18next.t("available_users")}</FormLabel>
                        <GridList
                            style={styles.users}
                            cellHeight={"auto"}
                            spacing={1}>
                            {this.props.users.map(user => {
                                return (
                                    <FormControlLabel
                                        key={user.id}
                                        control={
                                            <Checkbox
                                                name="user"
                                                checked={this.isCheckedUser(user)}
                                                onChange={this.handleSelectUser}
                                                value={user.id.toString()}/>
                                        }
                                        label={user.lastName + " " + user.firstName}/>
                                )
                            })}
                        </GridList>
                    </Grid>
                    <br/>
                    <Grid item>
                        <FormLabel>{i18next.t("roles")}</FormLabel>
                        <GridList style={styles.roles} cellHeight={"auto"} spacing={1}>
                            {this.props.roles.map(role => {
                                return (
                                    <FormControlLabel
                                        key={role.id}
                                        control={
                                            <Checkbox
                                                name="role"
                                                checked={this.isCheckedRole(role)}
                                                onChange={this.handleSelectRole}
                                                value={role.id.toString()}/>
                                        }
                                        label={role.name}/>
                                )
                            })}
                        </GridList>
                    </Grid>
                </Grid>
            </div>
        )
    }
}