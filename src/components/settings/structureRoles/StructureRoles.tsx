import React from "react";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {ApplicationState} from "../../../store";
import {Grid, LinearProgress, Paper, Typography} from "@material-ui/core";
import {Company, CompanyWithDetails} from "../../../store/settings/company/types";
import {User} from "../../../types";
import {
    getFreeUsersForCompany,
    getFreeUsersForDepartment,
    getFreeUsersForProject
} from "../../../store/settings/user/actions";
import {getRolesForStructureType} from "../../../store/settings/role/actions";
import {deleteUserCompanyRole, getCompany, updateDetailsCompany} from "../../../store/settings/company/actions";
import {Department, DepartmentWithDetails} from "../../../store/settings/department/types";
import {Role, UserRole} from "../../../store/settings/role/types";
import {
    deleteUserDepartmentRole,
    getDepartment,
    updateDetailsDepartment
} from "../../../store/settings/department/actions";
import {companyFromDetailCompany} from "../../../store/settings/company/utils";
import {departmentFromDetailDepartment} from "../../../store/settings/department/utils";
import {Project, ProjectWithDetails} from "../../../store/settings/project/types";
import {projectFromDetailProject} from "../../../store/settings/project/utils";
import {deleteUserProjectRole, getProject, updateDetailsProject} from "../../../store/settings/project/actions";
import {AddNewStructureRoles} from "./simple/AddNewStructureRoles";
import {UserRolesTable} from "./simple/UserRolesTable";
import {EditDialog} from "./simple/EditDialog";
import {RouteComponentProps} from "react-router";
import {getEmptyUser} from "../../../utils";
import InviteDialog from "../../auth/invite/InviteDialog";
import {createInvite} from "../../../store/invite/actions";
import {Invite} from "../../../store/invite/types";
import i18next from "i18next";

type PropsFromState = {
    company: CompanyWithDetails;
    department: DepartmentWithDetails;
    project: ProjectWithDetails;

    roles: Role[];
    users: User[];

    isLoading: boolean;
    isLoaded: boolean;
    message?: string;
}

type MatchParams = {
    structure: string,
    id: string
}

interface Props extends PropsFromState, RouteComponentProps<MatchParams> {
    onEditCompany: (company: CompanyWithDetails) => any;
    onEditDepartment: (department: DepartmentWithDetails) => any;
    onEditProject: (project: ProjectWithDetails) => any;

    onDeleteCompanyRoles: (userRoleId: number) => any;
    onDeleteDepartmentRoles: (userRoleId: number) => any;
    onDeleteProjectRoles: (userRoleId: number) => any;

    getCompany: (companyId: number) => any;
    getDepartment: (departmentId: number) => any;
    getProject: (projectId: number) => any;

    getRoles: (structureId: number) => any;

    getUsersForCompany: (companyId: number) => any;
    getUsersForDepartment: (departmentId: number) => any;
    getUsersForProject: (projectId: number) => any;

    createInvite: (invite: Invite) => any;

    dispatch: any;
    history: any;
}

type State = {
    currentStructure: CompanyWithDetails | DepartmentWithDetails | ProjectWithDetails;
    structureFromDetailStructure: (structure: CompanyWithDetails | DepartmentWithDetails | ProjectWithDetails) => Company | Department | Project;
    onEdit: (structure: CompanyWithDetails | DepartmentWithDetails | ProjectWithDetails) => any;
    onDelete: (id: number) => any;
    getUsersForStructure: (id: number) => any;

    user: User;
    isEditDialogOpen: boolean;
    isInviteDialogOpen: boolean;
}

//todo refactoring component and move this to another class
const structureTypes = {
    company: {id: 1, name: "Company"},
    department: {id: 2, name: "Department"},
    project: {id: 3, name: "Project"}
};

class StructureRoles extends React.Component<Props, State> {

    structureName = this.props.match.params.structure;

    constructor(props: Props) {
        super(props);
        let id: number = +this.props.match.params.id;
        switch (this.structureName) {
            case "company": {
                this.props.getCompany(id);
                this.props.getRoles(1);

                this.state = {
                    currentStructure: this.props.company,
                    structureFromDetailStructure: companyFromDetailCompany,
                    onEdit: this.props.onEditCompany,
                    onDelete: this.props.onDeleteCompanyRoles,
                    getUsersForStructure: this.props.getUsersForCompany,
                    user: getEmptyUser(),
                    isEditDialogOpen: false,
                    isInviteDialogOpen: false
                };
                break;
            }
            case "department": {
                this.props.getDepartment(id);
                this.props.getRoles(2);

                this.state = {
                    currentStructure: this.props.department,
                    structureFromDetailStructure: departmentFromDetailDepartment,
                    onEdit: this.props.onEditDepartment,
                    onDelete: this.props.onDeleteDepartmentRoles,
                    getUsersForStructure: this.props.getUsersForDepartment,
                    user: getEmptyUser(),
                    isEditDialogOpen: false,
                    isInviteDialogOpen: false
                };
                break;
            }
            case "project": {
                this.props.getProject(id);
                this.props.getRoles(3);

                this.state = {
                    currentStructure: this.props.project,
                    structureFromDetailStructure: projectFromDetailProject,
                    onEdit: this.props.onEditProject,
                    onDelete: this.props.onDeleteProjectRoles,
                    getUsersForStructure: this.props.getUsersForProject,
                    user: getEmptyUser(),
                    isEditDialogOpen: false,
                    isInviteDialogOpen: false
                };
                break;
            }
        }
        this.state.getUsersForStructure(id);
    }

    componentWillReceiveProps(props: Props) {
        this.setState({
            currentStructure: {...this.state.currentStructure, ...props[this.structureName]}
        });
    }

    onCloseDialog = () => {
        this.setState({
            isEditDialogOpen: false,
            isInviteDialogOpen: false
        });
    };

    //todo refactoring
    handleSubmit = (email: string, structure: {
        id: number,
        name: string
    }, roleId: number) => (event: React.SyntheticEvent) => {4;
        event.preventDefault();
        this.props.createInvite({
            email,
            roleId,
            type: structure.name.toUpperCase(),
            structureId: +this.props.match.params.id
        });
        this.setState({isInviteDialogOpen: false});
    };

    findUserRoleId = (role: Role): number | undefined => {
        let userRole = this.state.currentStructure.userRoles.find(ur => ur.role.id == role.id && ur.user.login == this.state.user.login);
        return userRole ? userRole.id : undefined
    };

    onSubmitDialog = async (selectedRoles: Role[]) => {
        let changedUserRoles = this.state.currentStructure.userRoles.filter(userRole => userRole.user.id != this.state.user.id);

        selectedRoles.forEach(role => changedUserRoles.push({
            id: this.findUserRoleId(role),
            role: role,
            user: this.state.user,
            [this.structureName]: this.state.structureFromDetailStructure(this.state.currentStructure)
        }));
        let changedStructure = {
            ...this.props[this.structureName],
            userRoles: changedUserRoles
        };

        this.setState({isEditDialogOpen: false});

        await this.state.onEdit(changedStructure);
        this.state.getUsersForStructure(changedStructure.id!);
    };

    onAdd = async (selectedUsers: Array<User>, selectedRoles: Array<Role>) => {
        let newUserRoles: Array<UserRole> = [];

        for (let user of selectedUsers) {
            for (let role of selectedRoles) {
                newUserRoles.push({
                    role: role,
                    user: user,
                    [this.structureName]: this.state.structureFromDetailStructure(this.state.currentStructure)
                });
            }
        }
        let changedStructure = {
            ...this.state.currentStructure,
            userRoles: [...this.state.currentStructure.userRoles, ...newUserRoles]
        };
        await this.state.onEdit(changedStructure);
        this.state.getUsersForStructure(changedStructure.id!);
    };

    onDelete = async (userLogin: string) => {
        let userRolesForDelete = this.state.currentStructure.userRoles.filter(userRole => userRole.user.login == userLogin);//todo check roles (delete company only company owner)
        for (let userRole of userRolesForDelete) {
            await this.state.onDelete(userRole.id!)
        }
        this.state.getUsersForStructure(this.state.currentStructure.id!);
    };

    onEditDialog = (login: string) => {
        let user = this.state.currentStructure.userRoles.map(userRole => userRole.user).find(user => user.login == login);
        if (user) {
            this.setState({isEditDialogOpen: true, user: user});
        }
    };

    render(): React.ReactNode {
        const {currentStructure, user} = this.state;
        const {users, roles} = this.props;

        let userRoles = currentStructure.userRoles
            .filter(userRole => userRole.user.login == user.login)
            .map(userRole => userRole.role);
        return (
            <div>
                {this.props.isLoading ? <LinearProgress/> :
                    <Paper>
                        <Typography variant="h6" id="tableTitle">
                            {currentStructure.name} {i18next.t("user_roles")}
                        </Typography>
                        <Grid container>
                            <Grid item xs={10}>
                                <UserRolesTable
                                    userRoles={currentStructure.userRoles}
                                    onDelete={this.onDelete}
                                    onEdit={this.onEditDialog}
                                    owner={currentStructure.owner}/>
                            </Grid>
                            <Grid item xs={2}>
                                <AddNewStructureRoles
                                    users={users}
                                    roles={roles}
                                    onAdd={this.onAdd}
                                    onInviteAdd={() => this.setState({isInviteDialogOpen: true})}/>
                            </Grid>
                        </Grid>
                    </Paper>}
                <EditDialog
                    open={this.state.isEditDialogOpen}
                    onClose={this.onCloseDialog}
                    onSubmit={this.onSubmitDialog}
                    user={user}
                    availableRoles={roles}
                    userRoles={userRoles}/>
                <InviteDialog
                    open={this.state.isInviteDialogOpen}
                    roles={roles}
                    handleClose={this.onCloseDialog}
                    handleSubmit={this.handleSubmit}
                    structure={structureTypes[this.structureName]}
                    structureName={currentStructure.name}/>
            </div>
        )
    }
}

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        company: applicationState.companySettings.company,
        department: applicationState.departmentSettings.department,
        project: applicationState.projectSettings.project,
        roles: applicationState.roleSettings.rolesForStructure,
        users: applicationState.userSettings.freeUsers,

        isLoading: applicationState.roleSettings.isLoading
            || applicationState.userSettings.isLoading
            || applicationState.companySettings.isLoading
            || applicationState.departmentSettings.isLoading
            || applicationState.projectSettings.isLoading,
        isLoaded: applicationState.roleSettings.isLoaded,
        message: applicationState.roleSettings.message,
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    onEditCompany: updateDetailsCompany,
    onEditDepartment: updateDetailsDepartment,
    onEditProject: updateDetailsProject,

    onDeleteCompanyRoles: deleteUserCompanyRole,
    onDeleteDepartmentRoles: deleteUserDepartmentRole,
    onDeleteProjectRoles: deleteUserProjectRole,

    getCompany: getCompany,
    getDepartment: getDepartment,
    getProject: getProject,

    getRoles: getRolesForStructureType,

    getUsersForCompany: getFreeUsersForCompany,
    getUsersForDepartment: getFreeUsersForDepartment,
    getUsersForProject: getFreeUsersForProject,

    createInvite: createInvite
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(StructureRoles);
