import  React from "react";
import {ApplicationState} from "../../store";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {Permission} from "../../store/settings/permission/types";
import {LinearProgress, Paper, Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import {getAllPermissions} from "../../store/settings/permission/actions";
import  i18next from "i18next";


type PropsFromState = {
    permissions: Permission[];

    isLoading: boolean;
    isLoaded: boolean;
    message?: string;
}

interface Props extends PropsFromState {
    getPermissions: () => any
}


class PermissionSettings extends React.Component<Props> {
    componentWillMount(){
        this.props.getPermissions();
    }

    render() {
        return (
            <div>
                {this.props.isLoading ? <LinearProgress/> :
                    <Paper>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>{i18next.t("name")}</TableCell>
                                    <TableCell>{i18next.t("code")}</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.props.permissions.map(permission => {
                                    return (
                                        <TableRow key={permission.id}>
                                            <TableCell>{permission.name}</TableCell>
                                            <TableCell>{permission.code}</TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </Paper>}
            </div>
        )
    }
}


function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        permissions: applicationState.permissionSettings.permissions,
        isLoading: applicationState.permissionSettings.isLoading,
        isLoaded: applicationState.permissionSettings.isLoaded,
        message: applicationState.permissionSettings.message,
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getPermissions: getAllPermissions
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(PermissionSettings);