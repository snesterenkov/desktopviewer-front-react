import React from "react";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";

import {Getter} from "@devexpress/dx-react-core";
import {
    ChangeSet,
    Column,
    DataTypeProvider,
    EditingState,
    IntegratedFiltering,
    IntegratedPaging,
    IntegratedSorting,
    PagingState,
    SearchState,
    SortingState
} from "@devexpress/dx-react-grid";
import {
    Grid,
    PagingPanel,
    SearchPanel,
    Table,
    TableEditColumn,
    TableEditRow,
    TableHeaderRow,
    Toolbar
} from "@devexpress/dx-react-grid-material-ui";
import {Chip, IconButton, Input, LinearProgress, MenuItem, Paper, Select} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";

import {addUser, deleteUser, getUsers, updateUser} from "../../store/settings/user/actions";
import {ApplicationState} from "../../store";
import {User} from "../../types";
import {SimpleSnackbar, SnackbarVariant} from "../commons/SimpleSnackbar";
import i18next from "i18next";

type PropsFromState = {
    users: User[];
    isLoading: boolean;
    isLoaded: boolean;
    message?: string;
}

interface Props extends PropsFromState {
    onInit: () => any;
    onAdd: (user: User) => any;
    onEdit: (user: User) => any;
    onDelete: (userId: number) => any;
    dispatch: any;
    history: any;
}

type State = {
    editingStateColumnExtensions?: any;
    pageSizes?: Array<number>;
    snackbar: {
        isOpen: boolean,
        message: string,
        variant: SnackbarVariant
    }
}

const getRowId = (row: any) => row.id;

const BooleanFormatter = ({value}: any) => value ? <Chip label={i18next.t("yes")}/> : null;

const BooleanEditor = ({value, onValueChange}: any) => (
    <Select
        input={<Input/>}
        value={value ? "Yes" : "No"}
        onChange={(event: any) => onValueChange(event.target.value === "Yes")}
        style={{width: "100%"}}>
        <MenuItem value="Yes">
            {i18next.t("yes")}
        </MenuItem>
        <MenuItem value="No">
            {i18next.t("no")}
        </MenuItem>
    </Select>
);

const BooleanTypeProvider = (props: any) => (
    <DataTypeProvider
        formatterComponent={BooleanFormatter}
        editorComponent={BooleanEditor}
        {...props}
    />
);

const AddButton = ({onExecute}: any) => (
    <div style={{textAlign: "center"}}>
        <IconButton onClick={onExecute} title={i18next.t("add")}>
            <AddIcon/>
        </IconButton>
    </div>
);

const EditButton = ({onExecute}: any) => (
    <IconButton onClick={onExecute} title={i18next.t("edit")}>
        <EditIcon/>
    </IconButton>
);

const DeleteButton = ({onExecute}: any) => (
    <IconButton onClick={onExecute} title={i18next.t("delete")}>
        <DeleteIcon/>
    </IconButton>
);

const CommitButton = ({onExecute}: any) => (
    <IconButton onClick={onExecute} title={i18next.t("save")}>
        <SaveIcon/>
    </IconButton>
);

const CancelButton = ({onExecute}: any) => (
    <IconButton color="secondary" onClick={onExecute} title={i18next.t("cancel")}>
        <CancelIcon/>
    </IconButton>
);

const commandComponents = {
    add: AddButton,
    edit: EditButton,
    delete: DeleteButton,
    commit: CommitButton,
    cancel: CancelButton,
};

const Command = ({id, onExecute}: any) => {
    const CommandButton = commandComponents[id];
    return (
        <CommandButton
            onExecute={onExecute}
        />
    );
};

const LoadingState = ({loading, columnCount}: any) => (
    <td colSpan={columnCount + 1} style={{textAlign: "center", verticalAlign: "middle"}}>
        {loading ? <LinearProgress variant="query"/> : i18next.t("no_data")}
    </td>
);

class UserSettings extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            editingStateColumnExtensions: [
                {columnName: "id", editingEnabled: false},
            ],
            pageSizes: [15, 30, 45, 0],
            snackbar: {
                isOpen: false, message: "", variant: "info"
            }
        };
        this.props.onInit();
    }

    commitChanges = (changeSet: ChangeSet) => {
        if (changeSet.added) {
            this.props.onAdd(changeSet.added[0])
                .then(this.setState({snackbar: {isOpen: true, variant: "success", message: "The user is added."}}));
        } else if (changeSet.changed) {
            const userId: number = +Object.keys(changeSet.changed)[0];
            const user: User = this.props.users.find(x => x.id === userId)!;
            const updateUser: User = {...user, ...changeSet.changed[userId]};
            this.props.onEdit(updateUser)
                .then(this.setState({snackbar: {isOpen: true, variant: "success", message: "The user is updated."}}));
        } else if (changeSet.deleted) {

            this.props.onDelete(+changeSet.deleted[0])
                .then(this.setState({snackbar: {isOpen: true, variant: "success", message: "The user is deleted."}}));
        }
    };

    handleSnackbarClose = () => () => {
        this.setState({snackbar: {isOpen: false, variant: "info", message: ""}})
    };

    render(): React.ReactNode {
        let columns: Column[] = [
            {name: "id", title: "№"},
            {name: "superUser", title: i18next.t("super_user")},
            {name: "firstName", title: i18next.t("first_name")},
            {name: "lastName", title: i18next.t("last_name")},
            {name: "login", title: i18next.t("login")},
            {name: "password", title: i18next.t("password")},
            {name: "email", title: "Email"}
        ];
        const {pageSizes, editingStateColumnExtensions} = this.state;
        const {users, isLoading} = this.props;
        return (<Paper>
                <SimpleSnackbar open={this.state.snackbar.isOpen} message={this.state.snackbar.message}
                                variant={this.state.snackbar.variant}
                                handleClose={this.handleSnackbarClose()}/>
                <Grid rows={users} columns={columns} getRowId={getRowId}>
                    <SearchState/>
                    <BooleanTypeProvider for={["superUser"]}/>
                    <EditingState onCommitChanges={this.commitChanges.bind(this)}
                                  columnExtensions={editingStateColumnExtensions}/>
                    <IntegratedFiltering/>
                    <SortingState defaultSorting={[{columnName: columns[0].name, direction: "asc"}]}/>
                    <IntegratedSorting/>
                    <PagingState
                        defaultCurrentPage={0}
                        defaultPageSize={15}
                    />
                    <IntegratedPaging/>
                    <Table
                        noDataCellComponent={() => <LoadingState columnCount={columns.length} loading={isLoading}/>}/>
                    <TableHeaderRow showSortingControls/>
                    <PagingPanel pageSizes={pageSizes}/>
                    <Toolbar/>
                    <SearchPanel messages={{searchPlaceholder: i18next.t("search")}}/>
                    <TableEditRow/>
                    <TableEditColumn showAddCommand showEditCommand showDeleteCommand commandComponent={Command}/>
                    <Getter
                        name="tableColumns"
                        computed={({tableColumns}) => [
                            ...tableColumns.filter((c: any) => c.type !== "editCommand"),
                            {key: "editCommand", type: "editCommand", width: 250}]
                        }
                    />
                </Grid>
            </Paper>
        )
    }
}

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        isLoading: applicationState.userSettings.isLoading,
        isLoaded: applicationState.userSettings.isLoaded,
        users: applicationState.userSettings.users,
        message: applicationState.userSettings.message
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    onInit: getUsers,
    onAdd: addUser,
    onEdit: updateUser,
    onDelete: deleteUser
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(UserSettings);