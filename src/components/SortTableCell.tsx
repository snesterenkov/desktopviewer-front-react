import  React from "react";
import {TableCell, TableSortLabel} from "@material-ui/core";
import i18next from "i18next";

interface Props {
    label: string,
    value: string,
    order?: 'asc' | 'desc',
    orderBy: string,
    onRequestSort: () => any
}

const desc = (a: any, b: any, orderBy: string) => {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
};

export const getSorting = (order: string, orderBy: string) => {
    return order === 'desc' ? (a: any, b: any) => desc(a, b, orderBy) : (a: any, b: any) => -desc(a, b, orderBy);
};

export const stableSort = (array: any[], cmp: any) => {
    const stabilizedThis = array.map((el: any, index: number) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
};


export class SortTableCell extends React.Component<Props> {
    render() {
        let {orderBy, value, order, label} = this.props;
        return (
            <TableCell sortDirection={value === orderBy ? order : false}>
                <TableSortLabel
                    active={value === orderBy}
                    direction={order}
                    onClick={this.props.onRequestSort}
                    title={i18next.t("sort")}>
                    {label}
                </TableSortLabel>
            </TableCell>
        )
    }
}