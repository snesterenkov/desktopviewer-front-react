import React from "react";
import {ApplicationSettingAction} from "../../store/settings/application/reducer";
import {ApplicationState} from "../../store";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {createStyles, Grid, LinearProgress, Paper, Table, TableBody, TableCell, TableHead, TableRow, Theme, withStyles} from "@material-ui/core";
import {getUserDependentProjects} from "../../store/settings/project/actions";
import {ProjectWithDetails} from "../../store/settings/project/types";
import {Department} from "../../store/settings/department/types";
import {Company} from "../../store/settings/company/types";
import {getUserStatsByProjectAndDate} from "../../store/snapshot/actions";
import {convertDateToFormatString, convertMinutesToTime} from "../../utils/DateTimeUtil";
import DatePicker from "../commons/DatePicker";
import history from "../../store/browserHistory";
import {USER_PAGE_URL} from "../../constants";
import {DropDown} from "../commons/DropDown";
import i18next from "i18next";

type PropsFromState = {
    userDependentProjects: Array<ProjectWithDetails>;
    isLoading: boolean;
    usersStats: any
}

interface Props extends PropsFromState {
    classes: any;
    getUserDependentProjects: () => any;
    getUserStatsByProjectAndDate: (projects: Array<ProjectWithDetails>, date: string) => any;
}

type UserStats = {
    userId: number;
    fullName: string;
    workTime?: any;
    startTime?: any;
    productiveTime?: any;
    efficiency?: any;
}

type State = {
    date: string;
    usersStats: Array<UserStats>;

    companies: Array<Company>;
    projects: Array<ProjectWithDetails>;
    departments: Array<Department>;

    selectedCompanyId?: number;
    selectedProjectId?: number;
    selectedDepartmentId?: number;

    filteredProjects: Array<ProjectWithDetails>;
    filteredDepartments: Array<Department>;

    isLoading: boolean
}

const styles = (theme: Theme) => createStyles({
    root: {
        flexGrow: 1,
        padding: theme.spacing.unit * 2,
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        padding: theme.spacing.unit * 2,
    },
});

const EmployeesPage = withStyles(styles)(
    class  extends React.Component<Props, State> {

        private initState: State = {
            date: convertDateToFormatString(new Date),
            usersStats: [],
            companies: [],
            projects: [],
            departments: [],
            filteredProjects: [],
            filteredDepartments: [],
            isLoading: false,
        };

        state: Readonly<State> = this.initState;

        componentDidMount() {
            this.props.getUserDependentProjects();
        }

        componentDidUpdate(prevProps: Props) {
            if (this.props.userDependentProjects !== prevProps.userDependentProjects && this.props.userDependentProjects.length > 0) {
                let projects = this.props.userDependentProjects;
                let departments: Array<Department> = this.getDepartmentsFromProjects(projects);
                let companies: Array<Company> = this.getCompaniesFromDepartments(departments);
                // let selectedCompanyId: number = -1;
                //let filteredDepartments: Array<Department> = this.getFilteredDepartments(departments, selectedCompanyId)
                let filteredDepartments: Array<Department> = this.getFilteredDepartments(departments);
                let filteredProjects: Array<ProjectWithDetails> = this.getFilteredProjects(filteredDepartments, projects);
                let usersStats: Array<UserStats> = this.getUserStats(filteredProjects);

                this.setState({
                    projects,
                    departments,
                    companies,
                    filteredDepartments,
                    filteredProjects,
                    usersStats
                });

                this.props.getUserStatsByProjectAndDate(filteredProjects, this.state.date);
            }
            if (this.props.usersStats !== prevProps.usersStats) {
                let usersStats: Array<UserStats> = this.state.usersStats;
                let stats = this.props.usersStats;
                usersStats.forEach((userStat) => {
                    userStat.workTime = stats[userStat.userId] ? convertMinutesToTime(stats[userStat.userId].snapshotsCount * 10) : "--";
                    userStat.productiveTime = stats[userStat.userId] ? convertMinutesToTime(stats[userStat.userId].productiveSnapshotsCount * 10) : "--";
                    userStat.startTime = stats[userStat.userId] ? stats[userStat.userId].startTime : "--";
                    userStat.efficiency = stats[userStat.userId] ? stats[userStat.userId].efficiency + "%" : "--";
                });
                this.setState({usersStats})
            }
            ///для синхронизации спинера
            if (this.props.isLoading && !prevProps.isLoading && !this.state.isLoading) {
                this.setState({isLoading: true})
            } else if (!this.props.isLoading && !prevProps.isLoading && this.state.isLoading) {
                this.setState({isLoading: false})
            }
        }

        private getDepartmentsFromProjects = (projects: Array<ProjectWithDetails>): Array<Department> => {
            return projects.map(project => project.department)
                .filter((department, index, departments) => departments.findIndex(found => found.id === department.id) === index)
        };

        private getCompaniesFromDepartments = (departments: Array<Department>): Array<Company> => {
            return departments.map(department => department.company)
                .filter((company, index, companies) => companies.findIndex(found => found.id === company.id) === index)
        };

        private getUserStats = (projects: Array<ProjectWithDetails>) => {
            return projects.map((project: ProjectWithDetails) => project.userRoles)
                .reduce((arr1, arr2) => arr1.concat(arr2))
                .map((userRole) => ({
                    userId: userRole.user.id,
                    fullName: userRole.user.lastName + " " + userRole.user.firstName
                }))
                .filter((user, index, users) => users.findIndex(found => found.userId === user.userId) === index);
        };

        private getFilteredDepartments = (departments: Array<Department>, selectedCompanyId?: number): Array<Department> => {
            return (selectedCompanyId && selectedCompanyId > 0) ? departments.filter(department => department.company!.id === selectedCompanyId) : departments;
        };

        private getFilteredProjects = (departments: Array<Department>, projects: Array<ProjectWithDetails>): Array<ProjectWithDetails> => {
            return projects.filter(project => departments.find(department => department.id === project.departmentId));
        };

        private changeCompany = (event: React.ChangeEvent<HTMLSelectElement>): void => {
            const selectedCompanyId: number = +event.target.value;
            const filteredDepartments: Array<Department> = this.getFilteredDepartments(this.state.departments, selectedCompanyId);
            const filteredProjects: Array<ProjectWithDetails> = this.getFilteredProjects(filteredDepartments, this.state.projects);
            const usersStats: Array<UserStats> = this.getUserStats(filteredProjects);
            this.props.getUserStatsByProjectAndDate(filteredProjects, this.state.date);
            this.setState({
                selectedCompanyId,
                selectedDepartmentId: undefined,
                selectedProjectId: undefined,
                filteredDepartments,
                filteredProjects,
                usersStats
            })
        };

        private changeDepartment = (event: React.ChangeEvent<HTMLSelectElement>): void => {
            const selectedDepartmentId: number = +event.target.value;

            let selectedDepartments: Array<Department>;
            let filteredProjects: Array<ProjectWithDetails>;
            let usersStats: Array<UserStats>;

            if (selectedDepartmentId > 0) {
                selectedDepartments = [this.state.filteredDepartments.find(department => department.id === selectedDepartmentId)!];
                filteredProjects = this.getFilteredProjects(selectedDepartments, this.state.projects);
                usersStats = this.getUserStats(filteredProjects);
            } else {
                filteredProjects = this.getFilteredProjects(this.state.filteredDepartments, this.state.projects);
                usersStats = this.getUserStats(filteredProjects);
            }

            this.props.getUserStatsByProjectAndDate(filteredProjects, this.state.date);
            this.setState({
                selectedDepartmentId: selectedDepartmentId,
                selectedProjectId: undefined,
                filteredProjects,
                usersStats
            })
        };

        private changeProject = (event: React.ChangeEvent<HTMLSelectElement>): void => {
            const selectedProjectId: number = +event.target.value;

            let selectedProjects: Array<ProjectWithDetails>;
            let usersStats: Array<UserStats>;

            if (selectedProjectId > 0) {
                selectedProjects = [this.state.filteredProjects.find(project => project.id === selectedProjectId)!];
                usersStats = this.getUserStats(selectedProjects);
            }
            else {
                selectedProjects = this.state.filteredProjects;
                usersStats = this.getUserStats(selectedProjects);
            }
            this.props.getUserStatsByProjectAndDate(selectedProjects, this.state.date);
            this.setState({selectedProjectId, usersStats})
        };

        private reloadUserStats = (date: string): void => {
            this.props.getUserStatsByProjectAndDate(this.state.filteredProjects, date);
            this.setState({date});
        };

        render(): React.ReactNode {
            let {classes} = this.props;
            return (
                <Paper className={classes.root}>

                    <Paper className={classes.paper}>
                        <Grid container xl={12}
                              spacing={16}
                              direction="row"
                              justify="flex-end"
                              alignContent={"center"}
                              alignItems="stretch">
                            <Grid item xs>
                                <DropDown
                                    required={false}
                                    label={i18next.t("company")}
                                    defaultLabel={i18next.t("all_companies")}
                                    value={this.state.selectedCompanyId}
                                    data={this.state.companies}
                                    handleChange={this.changeCompany}/>
                            </Grid>
                            <Grid item xs>
                                <DropDown
                                    required={false}
                                    label={i18next.t("department")}
                                    defaultLabel={i18next.t("all_departments")}
                                    value={this.state.selectedDepartmentId}
                                    data={this.state.filteredDepartments}
                                    handleChange={this.changeDepartment}/>
                            </Grid>
                            <Grid item xs>
                                <DropDown
                                    required={false}
                                    label={i18next.t("project")}
                                    defaultLabel={i18next.t("all_projects")}
                                    value={this.state.selectedProjectId}
                                    data={this.state.filteredProjects}
                                    handleChange={this.changeProject}/>
                            </Grid>
                            <DatePicker
                                label={i18next.t("date")}
                                date={this.state.date}
                                handleChange={this.reloadUserStats}/>
                        </Grid>
                    </Paper>
                    {this.state.isLoading && <LinearProgress/>}
                    <Paper className={classes.paper}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>{i18next.t("fio")}</TableCell>
                                    <TableCell>{i18next.t("work_time")}</TableCell>
                                    <TableCell>{i18next.t("start_at")}</TableCell>
                                    <TableCell>{i18next.t("productivity")}</TableCell>
                                    <TableCell>{i18next.t("efficiency")}</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.usersStats && this.state.usersStats.map((item: UserStats) => {
                                    return (
                                        <TableRow key={item.userId} onClick={this.goToUserPage(item.userId)}>
                                            <TableCell>{item.fullName}</TableCell>
                                            <TableCell>{item.workTime}</TableCell>
                                            <TableCell>{item.startTime}</TableCell>
                                            <TableCell>{item.productiveTime}</TableCell>
                                            <TableCell>{item.efficiency}</TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </Paper>

                </Paper>
            );
        }

        private goToUserPage = (userId: number) => (event: React.SyntheticEvent) => {
            history.push(USER_PAGE_URL.replace(":id", userId.toString()))
        }
    }
);

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        isLoading: applicationState.projectSettings.isLoading || applicationState.snapshot.isLoading,
        userDependentProjects: applicationState.projectSettings.userDependentProjects,
        usersStats: applicationState.snapshot.usersStats
    }
}

const mapDispatchToProps = (dispatch: Dispatch<ApplicationSettingAction>) => bindActionCreators({
    getUserDependentProjects: getUserDependentProjects,
    getUserStatsByProjectAndDate: getUserStatsByProjectAndDate
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(EmployeesPage);