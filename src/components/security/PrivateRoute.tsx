import {Redirect, Route, RouteComponentProps, RouteProps} from "react-router";
import React from "react";
import {connect} from "react-redux";
import {ApplicationState} from "../../store";
import {LOGIN_URL} from "../../constants";

type OwnProps = {
    component: React.ComponentType<any>
}

type StateProps = {
    isAuthenticated: boolean,
}

type PrivateRouteProps = StateProps & OwnProps & RouteProps;

type RenderComponent = (props: RouteComponentProps<any>) => React.ReactNode;

class PrivateRoute extends Route<PrivateRouteProps> {
    render() {
        const {component: Component, ...rest}: PrivateRouteProps = this.props;
        const renderComponent: RenderComponent = (props) => (
            this.props.isAuthenticated
                ? <Component {...props} />
                : <Redirect to={LOGIN_URL}/>
        );
        return (
            <Route {...rest} render={renderComponent}/>
        );
    }
}

function mapStateToProps(authState: ApplicationState): StateProps {
    return {
        isAuthenticated: authState.auth.isAuthenticated,
    }
}

export default connect<StateProps, {}, OwnProps>(mapStateToProps)(PrivateRoute);