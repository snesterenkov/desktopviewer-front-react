import React from 'react';
import history from "../../store/browserHistory";
import {AppBar, Tab, Tabs} from "@material-ui/core";
import UsersSettings from "../settings/UserSettings";
import CompanySettings from "../settings/CompanySettings";
import {Route, Switch} from "react-router";
import DepartmentSettings from "../settings/DepartmentSettings";
import ProjectSettings from "../settings/ProjectSettings";
import StructureRoles from "../settings/structureRoles/StructureRoles";
import ApplicationSettings from "../settings/ApplicationSettings";
import PermissionSettings from "../settings/PermissionSettings";
import {roleExists, withRoles} from "../../decorators/Authorization";
import {
    APPLICATION_SETTINGS_URL,
    COMPANY_SETTINGS_URL,
    DEPARTMENT_SETTINGS_URL,
    PERMISSION_SETTINGS_URL,
    PROJECT_SETTINGS_URL,
    SERVER_SETTINGS_URL,
    SERVER_STORAGE_URL,
    STRUCTURE_ROLES_URL,
    USER_SETTINGS_URL
} from "../../constants";
import {ServerSettings} from "../settings/server/ServerSettings";
import {ApplicationRole} from "../../types";
import i18next from "i18next";

class SettingsTabsRoute extends React.Component<any, any> {
    render() {
        return (
            <div>
                <AppBar position="static">
                    <Tabs value={this.props.match.params.category}>
                        <Tab
                            value={"company"}
                            label={i18next.t("companies")}
                            onClick={() => history.push(COMPANY_SETTINGS_URL)}/>
                        {roleExists([ApplicationRole.ADMIN_APPLICATION, ApplicationRole.ADMIN_COMPANY, ApplicationRole.ADMIN_DEPARTMENT, ApplicationRole.ADMIN_PROJECT]) && [
                            <Tab
                                key={"department"}
                                value={"department"}
                                label={i18next.t("departments")}
                                onClick={() => history.push(DEPARTMENT_SETTINGS_URL)}/>,

                            <Tab
                                key={"project"}
                                value={"project"}
                                label={i18next.t("projects")}
                                onClick={() => history.push(PROJECT_SETTINGS_URL)}/>,

                            <Tab
                                key={"application"}
                                value={"application"}
                                label={i18next.t("application")}
                                onClick={() => history.push(APPLICATION_SETTINGS_URL)}/>
                        ]}
                        {roleExists([ApplicationRole.ADMIN_APPLICATION]) && [
                            <Tab
                                key={"permission"}
                                value={"permission"}
                                label={i18next.t("permission")}
                                onClick={() => history.push(PERMISSION_SETTINGS_URL)}/>,

                            <Tab
                                key={"user"}
                                value={"user"}
                                label={i18next.t("users")}
                                onClick={() => history.push(USER_SETTINGS_URL)}/>,

                            <Tab
                                key={"server"}
                                value={"server"}
                                label={i18next.t("server")}
                                onClick={() => history.push(SERVER_STORAGE_URL)}/>
                        ]}

                    </Tabs>
                </AppBar>
                <Switch>
                    <Route path={USER_SETTINGS_URL}
                           component={withRoles(UsersSettings, [ApplicationRole.ADMIN_APPLICATION])}/>
                    <Route exact path={COMPANY_SETTINGS_URL} component={CompanySettings}/>
                    <Route exact path={DEPARTMENT_SETTINGS_URL}
                           component={withRoles(DepartmentSettings, [ApplicationRole.ADMIN_APPLICATION, ApplicationRole.ADMIN_COMPANY, ApplicationRole.ADMIN_DEPARTMENT, ApplicationRole.ADMIN_PROJECT])}/>
                    <Route exact path={PROJECT_SETTINGS_URL}
                           component={withRoles(ProjectSettings, [ApplicationRole.ADMIN_APPLICATION, ApplicationRole.ADMIN_COMPANY, ApplicationRole.ADMIN_DEPARTMENT, ApplicationRole.ADMIN_PROJECT])}/>
                    <Route path={STRUCTURE_ROLES_URL}
                           component={withRoles(StructureRoles, [ApplicationRole.ADMIN_APPLICATION, ApplicationRole.ADMIN_COMPANY, ApplicationRole.ADMIN_DEPARTMENT, ApplicationRole.ADMIN_PROJECT])}/>
                    <Route path={APPLICATION_SETTINGS_URL}
                           component={withRoles(ApplicationSettings, [ApplicationRole.ADMIN_APPLICATION, ApplicationRole.ADMIN_COMPANY, ApplicationRole.ADMIN_DEPARTMENT, ApplicationRole.ADMIN_PROJECT])}/>
                    <Route path={PERMISSION_SETTINGS_URL}
                           component={withRoles(PermissionSettings, [ApplicationRole.ADMIN_APPLICATION])}/>
                    <Route path={SERVER_SETTINGS_URL}
                           component={withRoles(ServerSettings, [ApplicationRole.ADMIN_APPLICATION])}/>
                </Switch>
            </div>
        );
    }
}

export default SettingsTabsRoute;