import React from "react";
import {createStyles, LinearProgress, Paper, Theme, withStyles} from "@material-ui/core";
import {bindActionCreators, Dispatch} from "redux";
import {ApplicationState} from "../../../store";
import {ApplicationSettingAction} from "../../../store/settings/application/reducer";
import {connect} from "react-redux";
import {convertDateToFormatString} from "../../../utils/DateTimeUtil";
import {ActivityPeriod} from "../../../types";
import {getProjectsList, getWorkPeriodStatistics} from "../../../store/reports/workdiary/actions";
import {Project} from "../../../store/settings/project/types";
import {Department} from "../../../store/settings/department/types";
import {CompanyWithProjects, WorkPeriodStatistic} from "../../../store/reports/workdiary/types";
import {Company} from "../../../store/settings/company/types";
import BasePanel from "./parts/BasePanel";
import FiltersPanel from "./parts/FiltersPanel";
import InfoPanel from "./parts/InfoPanel";

type PropsFromState = {
    companiesWithProjects: Array<CompanyWithProjects>,
    workPeriodStatistics: Array<WorkPeriodStatistic>,
    isLoading: boolean,
    message?: string,
}

interface Props extends PropsFromState {
    getProjectsList: () => any,
    getWorkPeriodStatistics: (period: ActivityPeriod, startDate: string, endDate: string, projectDTOs: Array<Project>) => any, //todo add to state
    classes: any
}

type State = {
    companies: Array<Company>;
    projects: Array<Project>;
    departments: Array<Department>;

    selectedCompanyId?: number;
    selectedProjectId?: number;
    selectedDepartmentId?: number;

    filteredProjects: Array<Project>;
    filteredDepartments: Array<Department>;

    activityPeriod: ActivityPeriod
    startDate: string,
    endDate: string,
}

const styles = (theme: Theme) => createStyles({
    root: {
        flexGrow: 1,
        padding: theme.spacing.unit * 2,
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        padding: theme.spacing.unit * 2,
    },
});

const WorkDiary = withStyles(styles)(
    class extends React.Component<Props, State> {

        private initState: State = {
            startDate: convertDateToFormatString(new Date),
            endDate: convertDateToFormatString(new Date),
            companies: [],
            projects: [],
            departments: [],
            filteredProjects: [],
            filteredDepartments: [],
            activityPeriod: ActivityPeriod.DAY,
        };

        constructor(props: Props) {
            super(props);
            const currentDate = convertDateToFormatString(new Date);
            this.props.getProjectsList();
            this.state = {...this.initState, endDate: currentDate, startDate: currentDate};
        }

        handleDateStartChange = (startDate: string) => {
            this.props.getWorkPeriodStatistics(this.state.activityPeriod, startDate, this.state.endDate, this.state.filteredProjects);
            this.setState({startDate})
        };

        handleDateEndChange = (endDate: string) => {
            this.props.getWorkPeriodStatistics(this.state.activityPeriod, this.state.startDate, endDate, this.state.filteredProjects);
            this.setState({endDate})
        };

        componentDidUpdate(prevProps: Props) {
            if (this.props.companiesWithProjects != prevProps.companiesWithProjects && this.props.companiesWithProjects.length > 0) {
                let companies: Array<CompanyWithProjects> = this.props.companiesWithProjects;
                let departments: Array<Department & { projects: Array<Project> }> = companies
                    .map((company: CompanyWithProjects) => company.departments)
                    .reduce((arr1, arr2) => arr1.concat(arr2));
                let projects: Array<Project> = departments
                    .map((department: Department & { projects: Array<Project> }) => department.projects)
                    .reduce((arr1, arr2) => arr1.concat(arr2));
                this.props.getWorkPeriodStatistics(this.state.activityPeriod, this.state.startDate, this.state.endDate, projects);
                this.setState({
                    companies: companies as Array<Company>,
                    departments: departments as Array<Department>,
                    projects,
                    filteredDepartments: departments as Array<Department>,
                    filteredProjects: projects
                });
            }
        }

        onActivityPeriodChange = (event: React.SyntheticEvent, value: string) => this.changeCurrentActivityPeriod(ActivityPeriod[value]);


        render(): React.ReactNode {
            let {classes} = this.props;
            return (
                <Paper className={classes.root}>
                    <BasePanel classes={classes}
                               startDate={this.state.startDate}
                               handleChangeStartDate={this.handleDateStartChange}
                               endDate={this.state.endDate}
                               handleChangeEndDate={this.handleDateEndChange}
                               value={this.state.activityPeriod}
                               onChange={this.onActivityPeriodChange}/>

                    {this.props.isLoading && <LinearProgress/>}

                    <FiltersPanel classes={classes}
                                  selectedCompanyId={this.state.selectedCompanyId!}
                                  companies={this.state.companies}
                                  handleCompanyChange={this.changeCompany}
                                  selectedDepartmentId={this.state.selectedDepartmentId!}
                                  departments={this.state.filteredDepartments}
                                  handleDepartmentChange={this.changeDepartment}
                                  selectedProjectId={this.state.selectedProjectId!}
                                  projects={this.state.filteredProjects} handleProjectChange={this.changeProject}/>

                    <InfoPanel classes={classes} workPeriodStatistics={this.props.workPeriodStatistics}/>

                </Paper>
            )
        }

        private changeCompany = (event: React.ChangeEvent<HTMLSelectElement>) => {
            let selectedCompanyId: number = +event.target.value;
            let filteredDepartments: Array<Department>;
            let filteredProjects: Array<Project>;
            if (selectedCompanyId > 0) {
                filteredDepartments = this.state.departments.filter((department: Department) => department.companyId === selectedCompanyId);
                filteredProjects = this.state.projects.filter((project: Project) => filteredDepartments.find(department => department.id === project.departmentId));
            } else {
                filteredDepartments = this.state.departments;
                filteredProjects = this.state.projects;
            }
            this.props.getWorkPeriodStatistics(this.state.activityPeriod, this.state.startDate, this.state.endDate, filteredProjects);
            this.setState({selectedCompanyId, selectedDepartmentId: undefined, selectedProjectId: undefined, filteredDepartments, filteredProjects})
        };

        private changeDepartment = (event: React.ChangeEvent<HTMLSelectElement>) => {
            let selectedDepartmentId: number = +event.target.value;
            let filteredProjects: Array<Project> = selectedDepartmentId > 0 ? this.state.projects.filter((project: Project) => project.departmentId === selectedDepartmentId) : this.state.projects;
            this.props.getWorkPeriodStatistics(this.state.activityPeriod, this.state.startDate, this.state.endDate, filteredProjects);
            this.setState({selectedDepartmentId, selectedProjectId: undefined, filteredProjects})
        };

        private changeProject = (event: React.ChangeEvent<HTMLSelectElement>) => {
            let selectedProjectId: number = +event.target.value;
            this.props.getWorkPeriodStatistics(this.state.activityPeriod, this.state.startDate, this.state.endDate, [this.state.filteredProjects.find(project => project.id === selectedProjectId)!]);
            this.setState({selectedProjectId})
        };

        private changeCurrentActivityPeriod = (activityPeriod: ActivityPeriod) => {
            this.setState({activityPeriod});
            switch (activityPeriod) {
                case ActivityPeriod.DAY:
                    this.props.getWorkPeriodStatistics(ActivityPeriod.DAY, this.state.startDate, this.state.endDate, this.state.filteredProjects);
                    this.setState({activityPeriod});
                    break;
                case ActivityPeriod.WEEK: {
                    this.props.getWorkPeriodStatistics(ActivityPeriod.WEEK, this.state.startDate, this.state.endDate, this.state.filteredProjects);
                    this.setState({activityPeriod});
                    break;
                }
                case ActivityPeriod.MONTH: {
                    this.props.getWorkPeriodStatistics(ActivityPeriod.MONTH, this.state.startDate, this.state.endDate, this.state.filteredProjects);
                    this.setState({activityPeriod});
                    break;
                }
            }
        };

    }
);

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        isLoading: applicationState.workDiary.isLoading,
        message: applicationState.workDiary.message,
        companiesWithProjects: applicationState.workDiary.companiesWithProjects,
        workPeriodStatistics: applicationState.workDiary.workPeriodStatistics,
    }
}

const mapDispatchToProps = (dispatch: Dispatch<ApplicationSettingAction>) => bindActionCreators({
    getProjectsList: getProjectsList,
    getWorkPeriodStatistics: getWorkPeriodStatistics

}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(WorkDiary);