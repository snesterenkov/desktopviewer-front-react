import React from "react";
import {FormControlLabel, Grid, Paper, Radio, RadioGroup} from "@material-ui/core";
import DatePicker from "../../../commons/DatePicker";
import {ActivityPeriod} from "../../../../types";
import i18next from "i18next";


type Props = {
    classes: any,
    startDate: any,
    endDate: any,
    handleChangeStartDate: (startDate: string) => void,
    handleChangeEndDate: (endDate: string) => void,
    value: any,
    onChange: (event: React.SyntheticEvent, value: string) => void
}

const BasePanel: React.SFC<Props> = (props: Props) => {
    return <Paper className={props.classes.paper}>
        <Grid
            container xl={12}
            direction="row"
            justify="space-evenly"
            alignItems="stretch">
            <DatePicker
                label={i18next.t("start_date")}
                date={props.startDate!}
                handleChange={props.handleChangeStartDate}/>
            <DatePicker
                label={i18next.t("end_date")}
                date={props.endDate!}
                handleChange={props.handleChangeEndDate}/>
            <Grid item xs>
                <RadioGroup
                    row={true}
                    aria-label={i18next.t("period")}
                    name="period"
                    value={props.value}
                    onChange={props.onChange}>
                    <FormControlLabel
                        value={ActivityPeriod.DAY}
                        control={<Radio color="primary"/>}
                        label={i18next.t("day")}/>
                    <FormControlLabel
                        value={ActivityPeriod.WEEK}
                        control={<Radio color="primary"/>}
                        label={i18next.t("week")}/>
                    <FormControlLabel
                        value={ActivityPeriod.MONTH}
                        control={<Radio color="primary"/>}
                        label={i18next.t("month")}/>
                </RadioGroup>
            </Grid>
        </Grid>
    </Paper>;
};

export default BasePanel;