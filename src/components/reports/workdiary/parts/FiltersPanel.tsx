import React from "react";
import {Grid, Paper} from "@material-ui/core";
import {DropDown} from "../../../commons/DropDown";
import {Company} from "../../../../store/settings/company/types";
import {Department} from "../../../../store/settings/department/types";
import {Project} from "../../../../store/settings/project/types";
import i18next from "i18next";

type Props = {
    classes: any,
    selectedCompanyId: number,
    companies: Array<Company>,
    handleCompanyChange: (event: React.ChangeEvent<HTMLSelectElement>) => void,
    selectedDepartmentId: number,
    departments: Array<Department>,
    handleDepartmentChange: (event: React.ChangeEvent<HTMLSelectElement>) => void,
    selectedProjectId: number,
    projects: Array<Project>,
    handleProjectChange: (event: React.ChangeEvent<HTMLSelectElement>) => void
}

const FiltersPanel: React.SFC<Props> = (props: Props) => {
    return <Paper className={props.classes.paper}>
        <Grid
            container xl={12}
            spacing={16}
            direction="row"
            justify="flex-end"
            alignItems="stretch">
            <Grid item xs>
                <DropDown
                    required={false}
                    label={i18next.t("company")}
                    defaultLabel={i18next.t("all_companies")}
                    value={props.selectedCompanyId}
                    data={props.companies}
                    handleChange={props.handleCompanyChange}/>
            </Grid>
            <Grid item xs>
                <DropDown
                    required={false}
                    label={i18next.t("department")}
                    defaultLabel={i18next.t("all_departments")}
                    value={props.selectedDepartmentId}
                    data={props.departments}
                    handleChange={props.handleDepartmentChange}/>
            </Grid>
            <Grid item xs>
                <DropDown
                    required={false}
                    label={i18next.t("project")}
                    defaultLabel={i18next.t("all_projects")}
                    value={props.selectedProjectId}
                    data={props.projects}
                    handleChange={props.handleProjectChange}/>
            </Grid>
        </Grid>
    </Paper>;
};

export default FiltersPanel;