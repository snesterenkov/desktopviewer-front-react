import  React from "react";
import {Paper, Table, TableBody, TableCell, TableFooter, TableHead, TableRow} from "@material-ui/core";
import {Period, WorkPeriodStatistic} from "../../../../store/reports/workdiary/types";
import {convertDateToFormatString} from "../../../../utils/DateTimeUtil";
import i18next from "i18next";

type Props = {
    classes: any,
    workPeriodStatistics: Array<WorkPeriodStatistic>
}


const getHoursForUsersByPeriod = (workPeriodStatistics: Array<WorkPeriodStatistic>, periodIndex: number): number => {
    return workPeriodStatistics.reduce((a, b) => a + b.periods[periodIndex].hours, 0);
};

const getAverageEfficiencyByPeriod = (workPeriodStatistics: Array<WorkPeriodStatistic>, index: number): number => {
    const efficiencySum = workPeriodStatistics.reduce((sum, item) => {
        return sum + item.periods[index].efficiency
    }, 0);
    return Math.round(efficiencySum / workPeriodStatistics.length);
};

const getTotalHoursForUser = (workPeriodStatistic: WorkPeriodStatistic) => {
    return workPeriodStatistic.periods.reduce((a, b) => a + b.hours, 0);
};

const getTotalHoursForUsers = (workPeriodStatistics: Array<WorkPeriodStatistic>) => {
    return workPeriodStatistics
        .map((workPeriodStatistic: WorkPeriodStatistic) =>
            workPeriodStatistic.periods
                .map((period: Period) => period.hours)
                .reduce((sum, item) => {
                    return sum + item
                }, 0)
        ).reduce((sum: number, item: number) => {
            return sum + item
        }, 0);
};


const InfoPanel: React.SFC<Props> = (props: Props) => {
    return <Paper className={props.classes.paper}>
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell>{i18next.t("fio")}</TableCell>
                    {props.workPeriodStatistics.length > 0 && props.workPeriodStatistics[0].periods.map((period: Period, i) => {
                        return (
                            <TableCell
                                key={i}>{convertDateToFormatString(new Date(period.startDate))
                            + ((period.startDate != period.endDate) ? (" - " + convertDateToFormatString(new Date(period.endDate))) : "")}</TableCell>
                        )
                    })}
                    {(props.workPeriodStatistics.length > 0 && props.workPeriodStatistics[0].periods.length > 1) &&
                    <TableCell>{i18next.t("total")}</TableCell>}
                </TableRow>
            </TableHead>
            <TableBody>
                {props.workPeriodStatistics.length > 0 && props.workPeriodStatistics.map((item: WorkPeriodStatistic) =>
                    <TableRow key={item.user.id}>
                        <TableCell>{item.user.lastName + " " + item.user.firstName}</TableCell>
                        {item.periods.map((period: Period, i) => {
                            return (
                                <TableCell key={i}>{period.hours.toFixed(2) + " " + period.efficiency + "%"}</TableCell>
                            )
                        })}
                        {(props.workPeriodStatistics.length > 0 && props.workPeriodStatistics[0].periods.length > 1) &&
                        <TableCell>{getTotalHoursForUser(item).toFixed(2)}</TableCell>}
                    </TableRow>
                )}
            </TableBody>
            <TableFooter>
                <TableRow>
                    <TableCell>{i18next.t("total")}:</TableCell>
                    {props.workPeriodStatistics.length > 0 && props.workPeriodStatistics[0].periods.map((period: Period, i) => {
                        return (
                            <TableCell
                                key={i}>{getHoursForUsersByPeriod(props.workPeriodStatistics, i).toFixed(2) + " " + getAverageEfficiencyByPeriod(props.workPeriodStatistics, i) + "%"}</TableCell>
                        )
                    })}
                    {(props.workPeriodStatistics.length > 0 && props.workPeriodStatistics[0].periods.length > 1) &&
                    <TableCell>{getTotalHoursForUsers(props.workPeriodStatistics).toFixed(2)}</TableCell>}
                </TableRow>
            </TableFooter>
        </Table>
    </Paper>
};

export default InfoPanel;