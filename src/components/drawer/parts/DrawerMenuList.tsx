import {ListItem, ListItemIcon, ListItemText, MenuList} from "@material-ui/core";
import SettingsMenuPopper from "./SettingsMenuPopper";
import React from "react";

import MyPageIcon from "@material-ui/icons/AccountCircle";
import EmployeesPageIcon from "@material-ui/icons/RecentActors";
import SettingsIcon from "@material-ui/icons/Settings";
import ReportIcon from "@material-ui/icons/Assessment";

import history from "../../../store/browserHistory";
import {EMPLOYEES_PAGE_URL, MY_PAGE_URL, WORK_DIARY_REPORTS_URL} from "../../../constants";
import {roleExists} from "../../../decorators/Authorization";
import {ApplicationRole} from "../../../types";
import * as i18next from "i18next";

type State = {
    anchorEl?: HTMLElement
}

class DrawerMenuList extends React.Component<any, State> {

    state: Readonly<State> = {
        anchorEl: undefined
    };

    private historyPush = (url: string) => () => {
        history.push(url);
        this.setState({anchorEl: undefined});
    };

    handleSettingsMenuClick = (event: any) => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleSettingsMenuClose = () => {
        this.setState({anchorEl: undefined});
    };

    render(): React.ReactNode {
        return <MenuList>

            <ListItem onClick={this.historyPush(MY_PAGE_URL)} button>
                <ListItemIcon>{<MyPageIcon/>}</ListItemIcon>
                <ListItemText primary={i18next.t("my_page")}/>
            </ListItem>

            <ListItem onClick={this.historyPush(EMPLOYEES_PAGE_URL)} button>
                <ListItemIcon>{<EmployeesPageIcon/>}</ListItemIcon>
                <ListItemText primary={i18next.t("employees")}/>
            </ListItem>

            <ListItem onClick={this.handleSettingsMenuClick} button
                      aria-owns={this.state.anchorEl ? "simple-menu" : undefined}
                      aria-haspopup="true">
                <ListItemIcon>{<SettingsIcon/>}</ListItemIcon>
                <ListItemText primary={i18next.t("settings")}/>
            </ListItem>
            <SettingsMenuPopper anchorEl={this.state.anchorEl} onClickAway={this.handleSettingsMenuClose} onClick={this.historyPush}/>

            {roleExists([ApplicationRole.ADMIN_APPLICATION, ApplicationRole.ADMIN_COMPANY, ApplicationRole.ADMIN_DEPARTMENT, ApplicationRole.ADMIN_PROJECT]) &&
            <ListItem onClick={this.historyPush(WORK_DIARY_REPORTS_URL)} button>
                <ListItemIcon>{<ReportIcon/>}</ListItemIcon>
                <ListItemText primary={i18next.t("reports")}/>
            </ListItem>}

        </MenuList>;
    }
}

export default DrawerMenuList;