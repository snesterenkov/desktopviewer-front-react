import {AppBar, Button, Menu, MenuItem, Toolbar, Typography} from "@material-ui/core";
import classNames from "classnames";
import IconButton from "@material-ui/core/IconButton/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import {ApplicationState} from "../../../store";
import {bindActionCreators, Dispatch} from "redux";
import {logoutUser} from "../../../store/auth/actions";
import {connect} from "react-redux";
import browserHistory from "../../../store/browserHistory";
import {LOGIN_URL, REGISTRATION_URL} from "../../../constants";
import i18next from "i18next";
import {SyntheticEvent} from "react";


type OwnProps = {
    classes: any,
    drawerOpen: boolean,
    onClick: () => void
    setLanguage: (lang: string) => void
}

type StateProps = {
    isAuthenticated: boolean,
}

type DispatchProps = {
    onLogout: () => void;
}

type Props = StateProps & DispatchProps & OwnProps

/*type Props = {
    classes: any,
    drawerOpen: boolean,
    onClick: () => void
    setLanguage: (lang: string) => void

    isAuthenticated: boolean,
    onLogout: () => void;
}*/

type State = {
    anchorEl: HTMLElement | null
}

class NavBar extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            anchorEl: null,
        };
    }

    handleClick = (event: SyntheticEvent) => {
        this.setState({anchorEl: event.currentTarget as HTMLElement});
    };

    handleClose = () => {
        this.setState({anchorEl: null});
    };

    onSetLanguage = (lang: string) => {
        this.props.setLanguage(lang);
        window.location.reload();
    };

    render() {
        const {anchorEl} = this.state;
        return (
            <AppBar position="fixed"
                    className={classNames(this.props.classes.appBar, this.props.drawerOpen && this.props.classes.appBarShift)}>
                <Toolbar disableGutters={!this.props.drawerOpen}>
                    {this.props.isAuthenticated ?
                        <IconButton
                            color="inherit"
                            aria-label={i18next.t("open_drawer")}
                            onClick={this.props.onClick}
                            className={classNames(this.props.classes.menuButton, this.props.drawerOpen && this.props.classes.hide)}>
                            <MenuIcon/>
                        </IconButton>
                        :
                        <IconButton color="inherit">
                            <MenuIcon/>
                        </IconButton>
                    }
                    <Typography variant="h6" color="inherit" noWrap>
                        Desktopviewer
                    </Typography>

                    {this.props.isAuthenticated ?
                        <Button color="inherit" onClick={this.props.onLogout}>
                            {i18next.t("logout")}
                        </Button>
                        :
                        <>
                            <Button color="inherit" onClick={() => browserHistory.push(LOGIN_URL)}>
                                {i18next.t("sign_in")}
                            </Button>
                            <Button color="inherit"
                                    onClick={() => browserHistory.push(REGISTRATION_URL)}>
                                {i18next.t("registration")}
                            </Button>
                        </>
                    }

                    <Button color="inherit"
                            aria-owns={anchorEl ? 'language-menu' : undefined}
                            aria-haspopup="true"
                            onClick={this.handleClick}>
                        {i18next.t("language")}
                    </Button>
                    <Menu id="language-menu"
                          anchorEl={anchorEl}
                          open={Boolean(anchorEl)}
                          onClose={this.handleClose}>
                        <MenuItem onClick={() => this.onSetLanguage('en')}>English</MenuItem>
                        <MenuItem onClick={() => this.onSetLanguage('ru')}>Русский</MenuItem>
                    </Menu>

                    <Button color="inherit" href="http://tracker-old.eklib.com/app/download/">
                        {i18next.t("download_tracker")}
                    </Button>
                </Toolbar>
            </AppBar>)
    }
}

function mapStateToProps(authState: ApplicationState):StateProps {
    return {
        isAuthenticated: authState.auth.isAuthenticated//todo remove?get from parent
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    onLogout: logoutUser
}, dispatch);

export default connect<StateProps, DispatchProps, OwnProps>(mapStateToProps, mapDispatchToProps)(NavBar);