import  React from "react";
import {ClickAwayListener, ListItem, ListItemIcon, ListItemText, MenuItem, MenuList, Paper, Popper} from "@material-ui/core";

import CompaniesSettingsIcon from "@material-ui/icons/LocationCity";
import DepartmentsSettingsIcon from "@material-ui/icons/Group";
import ProjectsSettingsIcon from "@material-ui/icons/BusinessCenter";
import ApplicationsSettingsIcon from "@material-ui/icons/Description";
import UsersSettingsIcon from "@material-ui/icons/SupervisorAccountTwoTone";
import ServerSettingsIcon from "@material-ui/icons/Computer";

import {APPLICATION_SETTINGS_URL, COMPANY_SETTINGS_URL, DEPARTMENT_SETTINGS_URL, PROJECT_SETTINGS_URL, SERVER_STORAGE_URL, USER_SETTINGS_URL} from "../../../constants";
import {roleExists} from "../../../decorators/Authorization";
import {ApplicationRole} from "../../../types";
import i18next from "i18next";

type Props = {
    anchorEl: HTMLElement | undefined,
    onClickAway: () => void,
    onClick: (url: string) => () => void,
}

const SettingsMenuPopper: React.SFC<Props> = (props: Props) => {
    return <Popper open={Boolean(props.anchorEl!)} anchorEl={props.anchorEl!} placement="right-start">
        <Paper>
            <ClickAwayListener onClickAway={props.onClickAway}>
                <MenuList>
                    <MenuItem onClick={props.onClick(COMPANY_SETTINGS_URL)}>
                        <ListItem button>
                            <ListItemIcon>{<CompaniesSettingsIcon/>}</ListItemIcon>
                            <ListItemText primary={i18next.t("company_settings")}/>
                        </ListItem>
                    </MenuItem>
                    {roleExists([ApplicationRole.ADMIN_APPLICATION, ApplicationRole.ADMIN_COMPANY, ApplicationRole.ADMIN_DEPARTMENT, ApplicationRole.ADMIN_PROJECT]) && [
                        <MenuItem key={"Departments settings"} onClick={props.onClick(DEPARTMENT_SETTINGS_URL)}>
                            <ListItem button>
                                <ListItemIcon>{<DepartmentsSettingsIcon/>}</ListItemIcon>
                                <ListItemText primary={i18next.t("department_settings")}/>
                            </ListItem>
                        </MenuItem>,
                        <MenuItem  key={"Projects settings"} onClick={props.onClick(PROJECT_SETTINGS_URL)}>
                            <ListItem button>
                                <ListItemIcon>{<ProjectsSettingsIcon/>}</ListItemIcon>
                                <ListItemText primary={i18next.t("project_settings")}/>
                            </ListItem>
                        </MenuItem>,
                        <MenuItem  key={"Applications settings"} onClick={props.onClick(APPLICATION_SETTINGS_URL)}>
                            <ListItem button>
                                <ListItemIcon>{<ApplicationsSettingsIcon/>}</ListItemIcon>
                                <ListItemText primary={i18next.t("application_settings")}/>
                            </ListItem>
                        </MenuItem>
                    ]}
                    {roleExists([ApplicationRole.ADMIN_APPLICATION]) && [
                        <MenuItem key={"Users settings"} onClick={props.onClick(USER_SETTINGS_URL)}>
                            <ListItem button>
                                <ListItemIcon>{<UsersSettingsIcon/>}</ListItemIcon>
                                <ListItemText primary={i18next.t("user_settings")}/>
                            </ListItem>
                        </MenuItem>,
                        <MenuItem key={"Server settings"}  onClick={props.onClick(SERVER_STORAGE_URL)}>
                            <ListItem button>
                                <ListItemIcon>{<ServerSettingsIcon/>}</ListItemIcon>
                                <ListItemText primary={i18next.t("server_settings")}/>
                            </ListItem>
                        </MenuItem>
                    ]}
                </MenuList>
            </ClickAwayListener>
        </Paper>
    </Popper>;
};

export default SettingsMenuPopper;