import {createStyles, CssBaseline, Divider, Theme, Typography} from "@material-ui/core";
import withStyles from "@material-ui/core/es/styles/withStyles";
import React from "react";
import classNames from "classnames";
import Drawer from "@material-ui/core/es/Drawer/Drawer";

import NavBar from "./parts/NavBar";
import {getUserInfoFromStorage} from "../../utils";
import {User} from "../../types";
import DrawerMenuList from "./parts/DrawerMenuList";
import {ApplicationState} from "../../store";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import i18next from "i18next";


const drawerWidth = 240;

const styles = (theme: Theme) => createStyles({
    root: {
        flexGrow: 1,
        display: "flex",
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(["width", "margin"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(["width", "margin"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
    },
    hide: {
        display: "none",
    },
    drawer: {
        width: "auto",
        // maxWidth: "240px"
    },
    drawerPaper: {
        overflowY: "hidden",
        whiteSpace: "nowrap",
        width: drawerWidth,
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: "hidden",
        overflowY: "hidden",
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.up("sm")]: {
            width: theme.spacing.unit * 9,
        },
    },
    toolbar: {
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        padding: "0 8px",
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        marginLeft: 100,
        marginTop: 54,
        padding: theme.spacing.unit * 3,
        overflow: "auto"
    },
});

type State = {
    isDrawerOpen: boolean
}

const MiniDrawer = withStyles(styles)(
    class extends React.Component<any, State> {
        state: Readonly<State> = {
            isDrawerOpen: false
        };

        componentWillMount() {
            let lang = localStorage.getItem("language");
            this.setLanguage(lang ? lang : 'en');
        }

        //todo maybe refactor
        setLanguage = (lang: string) => {
            localStorage.setItem("language", lang);
            i18next.init({
                lng: lang,
                resources: require(`../../localization/${lang}.json`)
            });
            i18next.changeLanguage(lang);
        };

        private getUserName = () => {
            const user: User = getUserInfoFromStorage();
            return user ? user.lastName + " " + user.firstName : undefined;
        };

        handleDrawerChangeState = () => {
            this.setState((state: State) => {
                return {isDrawerOpen: !state.isDrawerOpen};
            });
        };

        render() {
            const {classes} = this.props;
            return (
                <div className={classes.root}>
                    <CssBaseline/>

                    <NavBar classes={classes} drawerOpen={this.state.isDrawerOpen}
                            onClick={this.handleDrawerChangeState}
                            setLanguage={this.setLanguage}/>

                    {this.props.isAuthenticated &&
                    <Drawer className={classes.drawer}
                            variant="permanent"
                            classes={{paper: classNames(classes.drawerPaper, !this.state.isDrawerOpen && classes.drawerPaperClose)}}
                            open={this.state.isDrawerOpen}>
                        <div className={classes.toolbar}>
                            <Typography variant="h6">{this.getUserName()}</Typography>
                        </div>
                        <Divider/>

                        <DrawerMenuList/>
                    </Drawer>}
                    <main className={classes.content}>
                        {this.props.children}
                    </main>
                </div>
            );
        }
    }
);

function mapStateToProps(authState: ApplicationState) {
    return {
        isAuthenticated: authState.auth.isAuthenticated
    }
}

export default withRouter(connect<any, any, any>(mapStateToProps)(MiniDrawer));//todo поменять типы..