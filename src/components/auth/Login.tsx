import React from 'react';
import {
    Avatar,
    Button,
    createStyles,
    CssBaseline,
    FormControl,
    Input,
    InputLabel,
    LinearProgress,
    Paper,
    Theme,
    Typography,
    withStyles
} from "@material-ui/core";
import LockIcon from "@material-ui/icons/Lock";
import {loginUser} from "../../store/auth/actions";
import {connect} from "react-redux";
import {ApplicationState} from "../../store";
import {AnyAction, bindActionCreators, Dispatch} from "redux";
import {ApplicationSettingAction} from "../../store/settings/application/reducer";
import {Auth} from "../../store/auth/types";
import {PASSWORD_RECOVERY_URL, REGISTRATION_URL} from "../../constants";
import * as i18next from "i18next";
import {Link} from "react-router-dom";

const styles = (theme: Theme) => createStyles({
    layout: {
        width: 'auto',
        display: 'block', // Fix IE11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
    },
    form: {
        width: '100%', // Fix IE11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

type State = {
    user: Auth,
}

type Props = {
    isLoading: boolean,
    message: string,
    loginUser: (user: Auth) => (dispatch: Dispatch<AnyAction>) => Promise<any>;
    classes: any
}

const Login = withStyles(styles)(
    class extends React.Component<Props, State> {

        state = {
            user: {login: "", password: ""}
        };

        handleChange = (event: React.SyntheticEvent) => {
            const {name, value} = event.target as HTMLInputElement;
            const {user} = this.state;
            this.setState({
                user: {
                    ...user,
                    [name]: value
                }
            });
        };

        handleSubmit = (event: React.SyntheticEvent) => {
            event.preventDefault();
            const {user} = this.state;
            if (user.login && user.password)
                this.props.loginUser(user);
        };

        render(): React.ReactNode {
            const {message, classes, isLoading} = this.props;
            return <React.Fragment>
                <CssBaseline/>
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockIcon/>
                        </Avatar>
                        <Typography variant="h5">{i18next.t("sign_in")}</Typography>
                        <form className={classes.form} onSubmit={this.handleSubmit}>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="login">{i18next.t("login")}</InputLabel>
                                <Input
                                    id="login"
                                    name="login"
                                    autoComplete="login"
                                    value={this.state.user.login}
                                    onChange={this.handleChange} autoFocus/>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">{i18next.t("password")}</InputLabel>
                                <Input
                                    name="password"
                                    type="password"
                                    id="password"
                                    value={this.state.user.password}
                                    onChange={this.handleChange}
                                    autoComplete="current-password"
                                />
                            </FormControl>
                            <Link to={PASSWORD_RECOVERY_URL}>{i18next.t("forgot_password")}</Link>
                            {isLoading && <LinearProgress/>}
                            {message && <Typography color="error">{i18next.t("wrong_username_password")}</Typography>}
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}>
                                {i18next.t("sign_in")}
                            </Button>
                            <Typography>
                                {i18next.t("no_account")} <Link to={REGISTRATION_URL}>{i18next.t("sign_up")}</Link>
                            </Typography>
                        </form>
                    </Paper>
                </main>
            </React.Fragment>;
        }
    }
);

function mapStateToProps(authState: ApplicationState) {
    return {
        isLoading: authState.auth.isLoading,
        isAuthenticated: authState.auth.isAuthenticated,
        message: authState.auth.error!
    }
}

const mapDispatchToProps = (dispatch: Dispatch<ApplicationSettingAction>) => bindActionCreators({
    loginUser: loginUser
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(Login);