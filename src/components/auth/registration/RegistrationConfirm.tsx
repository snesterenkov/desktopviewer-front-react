import React from "react";
import {confirmRegistration} from "../../../store/auth/actions";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {Typography} from "@material-ui/core";
import i18next from "i18next";

type Props = {
    confirmRegistration: (token: string) => any;
}

const RegistrationConfirm: React.SFC<Props> = (props: any) => {
    // let message: string ="init"; todo
    props.confirmRegistration(props.match.params.token);
    return <Typography>{i18next.t("sign_up_confirm")}</Typography>
};

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    confirmRegistration: confirmRegistration,
}, dispatch);

export default connect<any, any, any>(null, mapDispatchToProps)(RegistrationConfirm);