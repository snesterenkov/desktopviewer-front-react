import React from 'react';
import {
    Avatar,
    Button,
    createStyles,
    CssBaseline,
    Paper,
    TextField,
    Theme,
    Typography,
    withStyles
} from "@material-ui/core";
import AccountIcon from "@material-ui/icons/AccountCircle";
import {bindActionCreators, Dispatch} from "redux";
import {ApplicationSettingAction} from "../../../store/settings/application/reducer";
import {addUser} from "../../../store/settings/user/actions";
import {connect} from "react-redux";
import {AxiosResultMessage, User} from "../../../types";
import {SimpleSnackbar, SnackbarProps} from "../../commons/SimpleSnackbar";
import i18next from "i18next";

const styles = (theme: Theme) => createStyles({
    layout: {
        width: 'auto',
        display: 'block', // Fix IE11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
    },
    form: {
        width: '100%', // Fix IE11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

type State = {
    login: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    confirmedPassword: string;
    snackbar: SnackbarProps;
    error?: string;
}

type Props = { classes: any, createUser: (user: Partial<User>) => any }

const Registration = withStyles(styles)(
    class extends React.Component<Props, State> {

        state: Readonly<State> = {
            login: "", firstName: "", lastName: "", email: "", password: "", confirmedPassword: "",
            snackbar: {
                open: false, message: "", variant: "info"
            },
            error: undefined
        };

        handleChange = (event: React.SyntheticEvent) => {
            const {name, value} = event.target as HTMLInputElement;
            this.setState({
                ...this.state,
                error: undefined,
                [name]: value
            });
        };

        handleSubmit = (event: React.SyntheticEvent) => {
            event.preventDefault();
            if (this.state.password == this.state.confirmedPassword) {
                const user: Partial<User> = {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    login: this.state.login,
                    password: this.state.password,
                    email: this.state.email,
                    superUser: false
                };
                this.props.createUser(user).then((res: AxiosResultMessage) =>
                    this.setState({
                        snackbar: {
                            open: true,
                            variant: res.success ? "success" : "error",
                            message: res.message
                        }
                    })
                );
            } else {
                this.setState({error: i18next.t("password_no_match")})
            }
        };

        handleSnackbarClose = () => () => {
            this.setState({snackbar: {open: false, variant: "info", message: ""}})
        };

        render(): React.ReactNode {
            const {classes} = this.props;
            return <React.Fragment>
                <CssBaseline/>
                <SimpleSnackbar
                    open={this.state.snackbar.open}
                    message={this.state.snackbar.message}
                    variant={this.state.snackbar.variant}
                    handleClose={this.handleSnackbarClose()}/>
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <AccountIcon/>
                        </Avatar>
                        <Typography variant="h5">{i18next.t("sign_up")}</Typography>
                        <form className={classes.form} onSubmit={this.handleSubmit}>
                            <TextField
                                fullWidth
                                required
                                autoFocus
                                id="login"
                                label={i18next.t("login")}
                                name="login"
                                autoComplete="username"
                                value={this.state.login}
                                onChange={this.handleChange}/>

                            <TextField
                                fullWidth
                                required
                                id="firstName"
                                label={i18next.t("first_name")}
                                name="firstName"
                                value={this.state.firstName}
                                onChange={this.handleChange}/>

                            <TextField
                                fullWidth
                                required
                                id="lastName"
                                label={i18next.t("last_name")}
                                name="lastName"
                                value={this.state.lastName}
                                onChange={this.handleChange}/>

                            <TextField
                                fullWidth
                                required
                                id="email"
                                label="Email"
                                name="email"
                                type="email"
                                autoComplete="email"
                                value={this.state.email}
                                onChange={this.handleChange}/>

                            <TextField
                                fullWidth
                                required
                                id="password"
                                label={i18next.t("password")}
                                type="password"
                                name="password"
                                autoComplete="new-password"
                                inputProps={{minLength: 6}}
                                value={this.state.password}
                                onChange={this.handleChange}/>

                            <TextField
                                fullWidth
                                required
                                id="confirmedPassword"
                                label={i18next.t("confirm_password")}
                                type="password"
                                name="confirmedPassword"
                                autoComplete="new-password"
                                inputProps={{minLength: 6}}
                                value={this.state.confirmedPassword}
                                onChange={this.handleChange}/>

                            <Typography color="error">{this.state.error}</Typography>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}>
                                {i18next.t("sign_up")}
                            </Button>
                        </form>
                    </Paper>
                </main>
            </React.Fragment>;
        }
    }
);

const mapDispatchToProps = (dispatch: Dispatch<ApplicationSettingAction>) => bindActionCreators({
    createUser: addUser
}, dispatch);

export default connect<any, any, any>(null, mapDispatchToProps)(Registration);
