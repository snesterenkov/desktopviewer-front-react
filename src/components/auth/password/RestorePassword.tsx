import  React from "react";
import {Button, createStyles, CssBaseline, Paper, TextField, Theme, Typography, withStyles} from "@material-ui/core";
import {bindActionCreators, Dispatch} from "redux";
import {ApplicationSettingAction} from "../../../store/settings/application/reducer";
import {connect} from "react-redux";
import {recoveryPassword} from "../../../store/settings/user/actions";
import {AxiosResultMessage} from "../../../types";
import {SimpleSnackbar, SnackbarProps} from "../../commons/SimpleSnackbar";
import i18next from "i18next";


const styles = (theme: Theme) => createStyles({
    layout: {
        width: 'auto',
        display: 'block', // Fix IE11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
    },
    form: {
        width: '100%', // Fix IE11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});


type Props = {
    classes: any,
    recoveryPassword: (email: string) => any
}

type State = {
    email: string,
    snackbar: SnackbarProps
    error?: string
}


const RestorePassword = withStyles(styles)(
    class extends React.Component<Props, State> {

        state: Readonly<State> = {
            email: "",
            error: undefined,
            snackbar: {
                open: false, message: "", variant: "info"
            },
        };

        handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
            const {value} = event.target;
            this.setState({email: value});
        };

        handleSubmit = (event: React.SyntheticEvent) => {
            event.preventDefault();
            this.props.recoveryPassword(this.state.email)
                .then((res: AxiosResultMessage) =>
                    this.setState({
                        snackbar: {
                            open: true,
                            variant: res.success ? "success" : "error",
                            message: res.message
                        }
                    })
                );
        };

        handleSnackbarClose = () => () => {
            this.setState({snackbar: {open: false, variant: "info", message: ""}})
        };

        render(): React.ReactNode {
            const {classes} = this.props;
            return <React.Fragment>
                <CssBaseline/>
                <SimpleSnackbar
                    open={this.state.snackbar.open}
                    message={this.state.snackbar.message}
                    variant={this.state.snackbar.variant}
                    handleClose={this.handleSnackbarClose()}/>
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <form onSubmit={this.handleSubmit}>
                            <Typography variant="h5">{i18next.t("restore_password")}</Typography>
                            <TextField
                                required
                                id="email"
                                label="Email"
                                type="email"
                                value={this.state.email}
                                onChange={this.handleChange}/>
                            {this.state.error}
                            <Button color="primary" type="submit">
                                {i18next.t("restore")}
                            </Button>
                        </form>
                    </Paper>

                </main>
            </React.Fragment>;
        }
    }
);

const mapDispatchToProps = (dispatch: Dispatch<ApplicationSettingAction>) => bindActionCreators({
    recoveryPassword: recoveryPassword
}, dispatch);

export default connect<any, any, any>(null, mapDispatchToProps)(RestorePassword);