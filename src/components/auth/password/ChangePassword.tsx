import React from "react";
import {Button, createStyles, CssBaseline, Paper, TextField, Theme, Typography, withStyles} from "@material-ui/core";
import {bindActionCreators, Dispatch} from "redux";
import {ApplicationSettingAction} from "../../../store/settings/application/reducer";
import {connect} from "react-redux";
import {changePassword} from "../../../store/settings/user/actions";
import {RouteComponentProps} from "react-router";
import {SimpleSnackbar, SnackbarProps} from "../../commons/SimpleSnackbar";
import {AxiosResultMessage} from "../../../types";
import  i18next from "i18next";


const styles = (theme: Theme) => createStyles({
    layout: {
        width: 'auto',
        display: 'block', // Fix IE11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
    },
    form: {
        width: '100%', // Fix IE11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});


type MatchParams = {
    token: string;
}

interface Props extends RouteComponentProps<MatchParams> {
    classes: any,
    changePassword: (token: string, password: string) => any
}

type State = {
    password: string,
    confirmedPassword: string,
    snackbar: SnackbarProps
    error?: string
}


const PasswordRecovery = withStyles(styles)(
    class extends React.Component<Props, State> {

        state: Readonly<State> = {
            password: "",
            confirmedPassword: "",
            snackbar: {
                open: false, message: "", variant: "info"
            },
            error: undefined
        };

        handleChangePassword = (event: React.ChangeEvent<HTMLSelectElement>) => {
            const {value} = event.target;
            this.setState({password: value});
        };

        handleChangeConfirmedPassword = (event: React.ChangeEvent<HTMLSelectElement>) => {
            const {value} = event.target;
            this.setState({confirmedPassword: value, error: undefined});
            if (this.state.password != value)
                this.setState({error: i18next.t("password_no_match")})
        };

        handleSubmit = (event: React.SyntheticEvent) => {
            event.preventDefault();
            const token: string = this.props.match.params.token;
            this.props.changePassword(token, this.state.password)
                .then((res: AxiosResultMessage) =>
                    this.setState({
                        snackbar: {
                            open: true,
                            variant: res.success ? "success" : "error",
                            message: res.message
                        }
                    })
                );
        };

        handleSnackbarClose = () => () => {
            this.setState({snackbar: {open: false, variant: "info", message: ""}})
        };

        render(): React.ReactNode {
            const {classes} = this.props;
            console.log(this.state);
            return <React.Fragment>
                <CssBaseline/>
                <SimpleSnackbar
                    open={this.state.snackbar.open}
                    message={this.state.snackbar.message}
                    variant={this.state.snackbar.variant}
                    handleClose={this.handleSnackbarClose()}/>
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <form onSubmit={this.handleSubmit}>
                            <Typography variant="h5">{i18next.t("change_password")}</Typography>
                            <TextField
                                required
                                id="password"
                                label="Password"
                                type="password"
                                inputProps={{minLength: 6}}
                                value={this.state.password}
                                onChange={this.handleChangePassword}/>

                            <TextField
                                required
                                id="password_confirmed"
                                label="Confirm password"
                                type="password"
                                inputProps={{minLength: 6}}
                                value={this.state.confirmedPassword}
                                onChange={this.handleChangeConfirmedPassword}/>
                            {this.state.error}
                            <Button color="primary" type="submit">{i18next.t("change")}</Button>
                        </form>
                    </Paper>

                </main>
            </React.Fragment>;
        }
    }
);


const mapDispatchToProps = (dispatch: Dispatch<ApplicationSettingAction>) => bindActionCreators({
    changePassword: changePassword
}, dispatch);

export default connect<any, any, any>(null, mapDispatchToProps)(PasswordRecovery);