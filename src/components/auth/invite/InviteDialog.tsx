import React from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField} from "@material-ui/core";
import {DropDown} from "../../commons/DropDown";
import {Role} from "../../../store/settings/role/types";
import i18next from "i18next";

type Props = {
    open: boolean,
    roles: Array<Role>;
    structureName: string
    structure: {
        id: number,
        name: string
    };
    handleClose: () => void;
    handleSubmit: (email: string, structure: {
        id: number,
        name: string
    }, roleId: number) => any;
}

type State = {
    email: string;
    roleId?: number;
}


class InviteDialog extends React.Component<Props, State> {

    state: Readonly<State> = {
        email: ""
    };

    componentDidUpdate(prevProps: Readonly<Props>): void {
        if ((prevProps.roles != this.props.roles) && this.props.roles.length > 0)
            this.setState((state: State, props: Props) => ({roleId: props.roles[0].id}))
    }

    render(): React.ReactNode {
        const {email, roleId} = this.state;
        return <Dialog open={this.props.open}
                       onClose={this.props.handleClose}
                       aria-labelledby="form-invite-dialog-title">
            <DialogTitle
                id="form-invite-dialog-title">{i18next.t("create_invite_to")} {this.props.structureName + " " + this.props.structure.name}</DialogTitle>
            <form onSubmit={this.props.handleSubmit(email, this.props.structure, roleId!)}>
                <DialogContent>
                    <TextField
                        required
                        type={"email"}
                        value={email}
                        onChange={this.handleEmailChange}
                        name="email"
                        autoFocus
                        margin="dense"
                        id="email"
                        label="Email"
                        fullWidth/>

                    <DropDown
                        label={i18next.t("role")}
                        value={roleId}
                        data={this.props.roles}
                        required
                        handleChange={this.handleRoleChange}/>

                </DialogContent>
                <DialogActions>
                    <Button type="submit" color="primary">{i18next.t("send")}</Button>
                    <Button onClick={this.props.handleClose} color="primary">{i18next.t("cancel")}</Button>
                </DialogActions>
            </form>
        </Dialog>;
    }


    private handleEmailChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const {value} = event.target;
        this.setState({email: value});
    };

    private handleRoleChange = (event: React.SyntheticEvent) => {
        const {value} = event.target as HTMLInputElement;
        this.setState({roleId: +value});
    }
}

export default InviteDialog;