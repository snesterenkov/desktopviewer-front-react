import React from "react";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {Button, createStyles, Paper, TextField, Theme, Typography, withStyles} from "@material-ui/core";
import {RouteComponentProps} from "react-router";
import {confirmInvite, getInviteByToken} from "../../../store/invite/actions";
import {Invite} from "../../../store/invite/types";
import {loginUser} from "../../../store/auth/actions";
import {Auth} from "../../../store/auth/types";
import {User} from "../../../types";
import i18next from "i18next";

type MatchParams = {
    token: string
}

type DispatchProps = {
    getInviteByToken: (token: string) => any;
    confirmInvite: (token: string) => any;
    loginUser: (user: Auth) => any;
};

type Props = RouteComponentProps<MatchParams> & DispatchProps & { classes: any };

type State = {
    user: Partial<User>,
    invite?: Invite,
    error?: string
}

const styles = (theme: Theme) => createStyles({
    root: {
        flexGrow: 1,
        padding: theme.spacing.unit * 2,
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        padding: theme.spacing.unit * 2,
    },
});

const InviteConfirm = withStyles(styles)(
    class extends React.Component<Props, State> {

        state: Readonly<State> = {
            user: {login: "", firstName: "", lastName: "", email: "", password: ""},
            invite: undefined,
            error: undefined
        };

        componentDidMount(): void {
            this.props.getInviteByToken(this.props.match.params.token).then((invite: Invite) => {
                    if (invite.user)
                        this.setState({invite});
                    else
                        this.setState((state: any) => {
                            return {invite, user: {...state.user, email: invite.email}}
                        });
                }
                , (error: string) => this.setState({error}));
        }

        handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
            const {value} = event.target;
            this.setState((state: any) => {
                return {invite: {...state.invite, user: {...state.invite!.user, password: value}}};
            });
        };

        handleUserPropertyChange = (event: React.SyntheticEvent) => {
            const {name, value} = event.target as HTMLInputElement;
            this.setState({
                ...this.state,
                error: undefined,
                user: {[name]: value}
            });
        };

        handleSubmit = (event: React.SyntheticEvent) => {
            event.preventDefault();
            this.props.loginUser({
                login: this.state.invite!.user!.login,
                password: this.state.invite!.user!.password!
            }).then(() =>
                this.props.confirmInvite(this.props.match.params.token), (error: string) => this.setState({error}))
        };

        render(): React.ReactNode {
            console.log(this.state);
            return <Paper className={this.props.classes.root}>
                {
                    this.state.invite ?
                        (this.state.invite.user ?
                            <Paper className={this.props.classes.paper}>
                                <TextField
                                    disabled
                                    type={"text"}
                                    value={this.state.invite.user.login}
                                    name="login"
                                    autoFocus
                                    margin="dense"
                                    id="login"
                                    label={i18next.t("login")}
                                    fullWidth/>

                                <TextField
                                    required
                                    type={"password"}
                                    value={this.state.invite.user.password}
                                    onChange={this.handleChange}
                                    name="password"
                                    autoFocus
                                    margin="dense"
                                    id="password"
                                    label={i18next.t("password")}
                                    fullWidth/>
                                <Typography color={"error"}>{this.state.error}</Typography>
                                <Button onClick={this.handleSubmit}>Confirm invite</Button>
                            </Paper>
                            :
                            <Paper className={this.props.classes.paper}>
                                <TextField
                                    fullWidth
                                    required
                                    autoFocus
                                    id="login"
                                    label={i18next.t("login")}
                                    name="login"
                                    autoComplete="username"
                                    value={this.state.user.login}
                                    onChange={this.handleUserPropertyChange}/>

                                <TextField
                                    fullWidth
                                    required
                                    id="firstName"
                                    label={i18next.t("first_name")}
                                    name="firstName"
                                    value={this.state.user.firstName}
                                    onChange={this.handleUserPropertyChange}/>

                                <TextField
                                    fullWidth
                                    required
                                    id="lastName"
                                    label={i18next.t("last_name")}
                                    name="lastName"
                                    value={this.state.user.lastName}
                                    onChange={this.handleUserPropertyChange}/>

                                <TextField
                                    fullWidth
                                    required
                                    disabled
                                    id="email"
                                    label="Email"
                                    name="email"
                                    type="email"
                                    value={this.state.user.email}
                                    onChange={this.handleUserPropertyChange}/>

                                <TextField
                                    fullWidth
                                    required
                                    id="password"
                                    label={i18next.t("password")}
                                    type="password"
                                    name="password"
                                    autoComplete="new-password"
                                    inputProps={{minLength: 6}}
                                    value={this.state.user.password}
                                    onChange={this.handleUserPropertyChange}/>

                                <Typography color="error">{this.state.error}</Typography>

                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary">
                                    {i18next.t("sign_up")}
                                </Button>
                            </Paper>) :
                        <Typography variant="h5" color={"error"}>{this.state.error}</Typography>
                }
            </Paper>
        }
    }
);

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators({
    getInviteByToken: getInviteByToken,
    confirmInvite: confirmInvite,
    loginUser: loginUser
}, dispatch);

export default connect<any, any, any>(null, mapDispatchToProps)(InviteConfirm);