import React from "react";
import {ApplicationSettingAction} from "../../store/settings/application/reducer";
import {createStyles, LinearProgress, Paper, Theme, withStyles} from "@material-ui/core";
import {ApplicationState} from "../../store";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";
import {Snapshot, UserPositionInCompany} from "../../store/snapshot/types";
import {changeSnapshotsProductivity, getDatesWithSnapshotByUser, getSnapshotsByUserAndDates, getUserPositionInCompany} from "../../store/snapshot/actions";
import {getCompaniesByEmployee} from "../../store/settings/company/actions";
import {ActivityPeriod, Productivity, User} from "../../types";
import {Company} from "../../store/settings/company/types";
import {convertDateToFormatString, getEndOfMonth, getEndOfWeek, getStartOfMonth, getStartOfWeek} from "../../utils/DateTimeUtil";
import {BasePanel} from "./parts/BasePanel";
import {InfoPanel} from "./parts/InfoPanel";
import {SnapshotsHistoryPanel} from "./parts/SnapshotsHistoryPanel";
import {SnapshotsPanel} from "./parts/SnapshotsPanel";
import {RouteComponentProps} from "react-router";
import {getObservedUser} from "../../store/user-page/actions";

type PropsFromState = {
    snapshots: Array<Snapshot>;
    observedUser: User,
    userPosition?: UserPositionInCompany;
    employeeCompanies: Company[],
    dates: Array<number>;
    isLoading: boolean;
    message?: string;
}

type MatchParams = {
    id: string;
}

interface Props extends PropsFromState, RouteComponentProps<MatchParams> {
    getObservedUser: (userId?: number) => any;
    getSnapshotsByUserAndDates: (userId: number, startDate: string, endDate: string) => any,
    getDatesWithSnapshotByUser: (userId: number) => any,
    getUserPositionInCompany: (userId: number, startDate: string, endDate: string, company: Company) => any,
    getCompanyByEmployee: (employeeId: number) => any,
    changeSnapshotsProductivity: (snapshots: Array<Snapshot>, productivity: Productivity) => any,
    classes: any;
}

export type MatrixItem = {
    order: number,
    count: number,
    snapshots: Array<Snapshot | null>
};

export type SnapshotHistory = {
    id: number,
    date: string,
    time: string,
    message: string,
    snapshotsCount: number
}

type State = {
    //BasePanel
    activityPeriod: ActivityPeriod
    startDate?: string,
    endDate?: string,
    //InfoPanel
    startTime: string;
    workTime: string;
    productivity: string;
    efficiency: string;
    averageActivity: number | string;
    selectedCompanyId?: number,
    //SnapshotPanel
    popperTarget: any,
    isPopperOpen: boolean,
    snapshot: Snapshot | undefined
    snapshotMatrix: Array<MatrixItem>,
    //SnapshotHistoryPanel
    snapshotHistories?: Array<SnapshotHistory>,

    isLoading: boolean
}

const styles = (theme: Theme) => createStyles({
    root: {
        flexGrow: 1,
        padding: theme.spacing.unit * 2,
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        padding: theme.spacing.unit * 2,
    },
});

const UserPage = withStyles(styles)(
    class extends React.Component<Props, State> {

        private initState: State = {
            startTime: "--",
            workTime: "--",
            productivity: "--",
            averageActivity: "--",
            efficiency: "--",
            activityPeriod: ActivityPeriod.DAY,
            snapshotMatrix: [],
            isPopperOpen: false,
            popperTarget: null,
            snapshot: undefined,
            isLoading: false
        };

        state = {...this.initState, selectedCompanyId: 0, endDate: convertDateToFormatString(new Date), startDate: convertDateToFormatString(new Date)};

        componentDidMount() {
            this.props.getObservedUser(this.getUserId())
                .then((user: User) => {
                    const userId = user.id;
                    this.props.getCompanyByEmployee(userId)
                        .then(() => this.props.getDatesWithSnapshotByUser(userId))
                        .then(() => this.props.getUserPositionInCompany(userId, this.state.startDate, this.state.endDate, this.props.employeeCompanies[this.state.selectedCompanyId!]))
                        .then(() => this.props.getSnapshotsByUserAndDates(userId, this.state.startDate, this.state.startDate));
                })
        }

        componentDidUpdate(prevProps: Props) {
            if (this.props.snapshots != prevProps.snapshots) {
                if (this.props.snapshots.length > 0) {
                    const startTime: string = this.getStartTime(this.props.snapshots);
                    const workTime: string = this.convertSnapshotsCountToTime(this.props.snapshots.length);
                    const productivity: string = this.getProductivity(this.props.snapshots);
                    const averageActivity: number = this.getAverageActivity(this.props.snapshots);
                    const efficiency: string = this.getEfficiency(this.props.snapshots);
                    const newState = {startTime, workTime, averageActivity, productivity, efficiency};
                    if (this.state.activityPeriod === ActivityPeriod.DAY) {
                        const snapshotMatrix: Array<MatrixItem> = this.createSnapshotsPerHourMatrix(this.props.snapshots);
                        this.setState({...newState, snapshotMatrix});
                    } else {
                        const snapshotHistories: Array<SnapshotHistory> = this.getSnapshotHistories(this.props.snapshots)!;
                        this.setState({...newState, snapshotHistories})
                    }
                } else {
                    this.setState({...this.initState, activityPeriod: this.state.activityPeriod});
                }
            }
            if (this.props.match.path !== prevProps.match.path) {
                this.reload(this.getUserId());
            }
            //для синхронизации спинера
            if (this.props.isLoading && !prevProps.isLoading && !this.state.isLoading) {
                this.setState({isLoading: true})
            } else if (!this.props.isLoading && !prevProps.isLoading && this.state.isLoading) {
                this.setState({isLoading: false})
            }
        }

        handleMouseOut = () => {
            this.setState({isPopperOpen: false, snapshot: undefined, popperTarget: undefined});
        };

        handleMouseOver = (event: React.MouseEvent, snapshot: Snapshot) => {
            const {currentTarget} = event;
            this.setState(state => ({
                popperTarget: currentTarget,
                isPopperOpen: !state.isPopperOpen,
                snapshot
            }));
        };

        handleDateStartChange = (startDate: string) => {
            if (this.state.activityPeriod == ActivityPeriod.DAY)
                this.getActivityInfo(startDate, startDate);
            else
                this.getActivityInfo(this.state.endDate!, startDate);
        };

        handleDateEndChange = (endDate: string) => {
            if (this.state.activityPeriod == ActivityPeriod.DAY)
                this.getActivityInfo(endDate, endDate);
            else
                this.getActivityInfo(endDate, this.state.startDate!);
        };

        render(): React.ReactNode {
            const {classes, observedUser, userPosition, dates} = this.props;
            return (
                observedUser ? <Paper className={classes.root}>
                    <BasePanel classes={classes}
                               activityPeriod={this.state.activityPeriod}
                               user={observedUser}
                               startDate={this.state.startDate!}
                               endDate={this.state.endDate!}
                               changeCurrentActivityPeriod={this.changeCurrentActivityPeriod} dates={dates!}
                               handleDateStartChange={this.handleDateStartChange}
                               handleDateEndChange={this.handleDateEndChange}/>
                    {this.state.isLoading && <LinearProgress/>}

                    <InfoPanel classes={classes}
                               startTime={this.state.startTime}
                               workTime={this.state.workTime}
                               productivity={this.state.productivity}
                               efficiency={this.state.efficiency}
                               averageActivity={this.state.averageActivity}
                               userPosition={userPosition!}
                               changeCompany={this.changeCompany}/>

                    {(this.state.activityPeriod !== ActivityPeriod.DAY && this.state.snapshotHistories && this.state.snapshotHistories.length > 0) &&
                    <SnapshotsHistoryPanel classes={classes} snapshotHistories={this.state.snapshotHistories!}/>}

                    {(this.state.activityPeriod === ActivityPeriod.DAY && this.state.snapshotMatrix && this.state.snapshotMatrix.length > 0) &&
                    <SnapshotsPanel
                        classes={classes}
                        snapshotMatrix={this.state.snapshotMatrix}
                        open={this.state.isPopperOpen}
                        popperTarget={this.state.popperTarget}
                        snapshot={this.state.snapshot!}
                        handleMouseOver={this.handleMouseOver}
                        handleMouseOut={this.handleMouseOut}
                        changeSnapshotsProductivity={this.props.changeSnapshotsProductivity}/>
                    }
                </Paper> : <h1>loading...</h1>
            )
        }

        private getUserId(): number {
            return +this.props.match.params.id;
        };

        private reload(userId: number) {
            const currentDate = convertDateToFormatString(new Date);
            this.props.getCompanyByEmployee(userId)
                .then(this.getActivityInfo(currentDate));
            this.setState({...this.initState, selectedCompanyId: 0});
        }

        private getActivityInfo(endDate: string, startDate?: string, initObservedUser?: User) {
            const userId: number = initObservedUser ? initObservedUser.id : this.props.observedUser.id;
            this.props.getDatesWithSnapshotByUser(userId)
                .then(() => this.props.getUserPositionInCompany(userId, startDate ? startDate : endDate, endDate, this.props.employeeCompanies[this.state.selectedCompanyId!]))
                .then(() => this.props.getSnapshotsByUserAndDates(userId, startDate ? startDate : endDate, endDate));
            this.setState({startDate, endDate});
        }

        private getProductivity = (snapshots: Array<Snapshot>) => {
            return this.convertSnapshotsCountToTime(this.getProductiveSnapshots(snapshots).length);
        };

        private convertSnapshotsCountToTime = (snapshotsCount: number) => {
            return Math.floor((snapshotsCount * 10) / 60) + "h" + (snapshotsCount * 10) % 60 + "m";
        };

        private getProductiveSnapshots = (snapshots: Array<Snapshot>) => {
            return snapshots.filter((snapshot) => snapshot.productivity !== Productivity.UNPRODUCTIVE);
        };

        private getStartTime = (snapshots: Array<Snapshot>) => {
            if (this.state.activityPeriod === ActivityPeriod.DAY)
                return snapshots[0].time;
            let starts: Array<string> = [];
            let date: string = "";
            snapshots.forEach((snapshot: Snapshot) => {
                if (date !== snapshot.date) {
                    starts.push(snapshot.time);
                    date = snapshot.date;
                }
            });
            let earliestStart = starts.reduce((prev, current) => current < prev ? current : prev);
            let latestStart = starts.reduce((prev, current) => current > prev ? current : prev);
            return earliestStart === latestStart ? earliestStart : earliestStart + "-" + latestStart;
        };

        private getAverageActivity = (snapshots: Array<Snapshot>) => {
            const activityPercentSum: number = snapshots.reduce((sum, snapshot) => sum + snapshot.userActivityPercent, 0);
            return Math.round(activityPercentSum / snapshots.length) || 0;
        };

        private getEfficiency = (snapshots: Array<Snapshot>) => {//todo refactoring
            const productiveSnapshots: Array<Snapshot> = this.getProductiveSnapshots(snapshots);
            const averageActivity: number = this.getAverageActivity(productiveSnapshots);
            return Math.round((productiveSnapshots.length / snapshots.length) * averageActivity) + "%";
        };

        private getSnapshotHistories = (snapshots: Array<Snapshot>) => {
            if (this.state.activityPeriod === ActivityPeriod.DAY)
                return [];
            let snapshotHistories: Array<SnapshotHistory> = [];
            let item: SnapshotHistory = {id: 0, date: "", message: "", time: "", snapshotsCount: 0};
            snapshots.forEach((snapshot) => {
                if (snapshot.date !== item.date || snapshot.message !== item.message) {
                    item = {
                        id: snapshot.id,
                        date: snapshot.date,
                        message: snapshot.message,
                        time: "",
                        snapshotsCount: 0
                    };
                    snapshotHistories.push(item);
                }
                snapshotHistories[snapshotHistories.length - 1].snapshotsCount += 1;
            });
            snapshotHistories.forEach((item: SnapshotHistory) => {
                item.time = this.convertSnapshotsCountToTime(item.snapshotsCount)
            });
            return snapshotHistories;
        };

        private createSnapshotsPerHourMatrix = (snapshots: Array<Snapshot>) => {
            const HOURS_IN_DAY: number = 24;

            let matrix: Array<MatrixItem> = Array<MatrixItem>(HOURS_IN_DAY);
            for (let i = 0; i < matrix.length; i++) {
                matrix[i] = {order: i, count: 0, snapshots: new Array<Snapshot>(6)};
            }

            for (let i = 0; i < snapshots.length; i++) {
                let hoursAndMinutes: Array<string> = snapshots[i].time.split(":");
                let hour: number = +hoursAndMinutes[0];
                let minutesBlock: number = Math.floor(+hoursAndMinutes[1] / 10);
                matrix[hour].snapshots[minutesBlock] = snapshots[i];
                matrix[hour].count++;
            }

            matrix.filter((matrixItem) => matrixItem.count > 0).forEach((matrixItem) => {//todo мб переписать?
                for (let i = 0; i < 6; i++) {
                    if (matrixItem.snapshots[i] === undefined)
                        matrixItem.snapshots[i] = null;
                }
            });

            return matrix;
        };

        private changeCompany = (offset: number) => {
            let selectedCompanyId: number = this.state.selectedCompanyId!;
            selectedCompanyId += offset;
            selectedCompanyId = selectedCompanyId > (this.props.employeeCompanies.length - 1) ? 0 : selectedCompanyId;
            selectedCompanyId = selectedCompanyId < 0 ? this.props.employeeCompanies.length - 1 : selectedCompanyId;
            this.setState({selectedCompanyId});
            if (this.state.activityPeriod === ActivityPeriod.DAY) {
                this.getActivityInfo(this.state.endDate!);
            } else {
                this.getActivityInfo(this.state.endDate!, this.state.startDate!);
            }
        };

        private changeCurrentActivityPeriod = (activityPeriod: ActivityPeriod) => {
            this.setState({activityPeriod, snapshotMatrix: [], snapshotHistories: []});
            switch (activityPeriod) {
                case ActivityPeriod.DAY:
                    let today: string = convertDateToFormatString(new Date);
                    this.getActivityInfo(today);
                    break;
                case ActivityPeriod.WEEK: {
                    this.getActivityInfo(getEndOfWeek(this.state.endDate!), getStartOfWeek(this.state.endDate!));
                    break;
                }
                case ActivityPeriod.MONTH: {
                    this.getActivityInfo(getEndOfMonth(this.state.endDate!), getStartOfMonth(this.state.endDate!));
                    break;
                }
            }
        };
    }
);

function mapStateToProps(applicationState: ApplicationState): PropsFromState {
    return {
        isLoading: applicationState.snapshot.isLoading || applicationState.userPage.isLoading || applicationState.companySettings.isLoading,
        message: applicationState.snapshot.message || applicationState.userPage.message || applicationState.companySettings.message,
        observedUser: applicationState.userPage.observedUser!,
        snapshots: applicationState.snapshot.snapshots,
        userPosition: applicationState.snapshot.userPosition,
        employeeCompanies: applicationState.companySettings.employeeCompanies,
        dates: applicationState.snapshot.dates,
    }
}

const mapDispatchToProps = (dispatch: Dispatch<ApplicationSettingAction>) => bindActionCreators({
    getObservedUser: getObservedUser,
    getSnapshotsByUserAndDates: getSnapshotsByUserAndDates,
    getDatesWithSnapshotByUser: getDatesWithSnapshotByUser,
    getUserPositionInCompany: getUserPositionInCompany,
    getCompanyByEmployee: getCompaniesByEmployee,
    changeSnapshotsProductivity: changeSnapshotsProductivity,
}, dispatch);

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(UserPage);