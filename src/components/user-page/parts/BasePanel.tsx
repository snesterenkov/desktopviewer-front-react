import React from "react";
import {FormControlLabel, Grid, Paper, RadioGroup, Typography} from "@material-ui/core";
import {ActivityPeriod, User} from "../../../types";
import DatePicker from "../../commons/DatePicker";
import Radio from "@material-ui/core/Radio/Radio";
import i18next from "i18next";

type BasePanelProps = {
    classes: any;

    user: User;
    dates: Array<number>;
    activityPeriod: ActivityPeriod;
    startDate: string;
    endDate: string;

    changeCurrentActivityPeriod: (period: ActivityPeriod) => any;

    handleDateStartChange: (date: string) => void;
    handleDateEndChange: (date: string) => void;
}

export const BasePanel: React.SFC<BasePanelProps> = (props) => {
    return (
        <Paper className={props.classes.paper}>
            <Grid
                container
                direction="row"
                justify="flex-end"
                alignItems="stretch">
                <Grid item xs={4}>
                    <Typography variant="h5">
                        {props.user.lastName + " " + props.user.firstName}
                    </Typography>
                </Grid>
                <Grid item xs>
                    {props.activityPeriod !== ActivityPeriod.DAY &&
                    <DatePicker
                        label={i18next.t("start_date")}
                        date={props.startDate}
                        handleChange={props.handleDateStartChange} dates={props.dates}/>}
                </Grid>
                <Grid item xs>
                    <DatePicker
                        label={props.activityPeriod !== ActivityPeriod.DAY ? i18next.t("end_date") : i18next.t("date")}
                        date={props.endDate}
                        handleChange={props.handleDateEndChange} dates={props.dates}/>
                </Grid>
                <Grid item xs>
                    <RadioGroup
                        row={true}
                        aria-label={i18next.t("period")}
                        name="period"
                        value={props.activityPeriod}
                        onChange={(event, value) => props.changeCurrentActivityPeriod(ActivityPeriod[value])}>
                        <FormControlLabel
                            value={ActivityPeriod.DAY}
                            control={<Radio color="primary"/>}
                            label={i18next.t("day")}/>

                        <FormControlLabel
                            value={ActivityPeriod.WEEK}
                            control={<Radio color="primary"/>}
                            label={i18next.t("week")}/>

                        <FormControlLabel
                            value={ActivityPeriod.MONTH}
                            control={<Radio color="primary"/>}
                            label={i18next.t("month")}/>
                    </RadioGroup>
                </Grid>
            </Grid>
        </Paper>
    );
};