import React from "react";
import {Paper, Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import {SnapshotHistory} from "../UserPage";
import i18next from "i18next";

type SnapshotsHistoryPanelProps = {
    classes: any;
    snapshotHistories: Array<SnapshotHistory>;
}

export const SnapshotsHistoryPanel: React.SFC<SnapshotsHistoryPanelProps> = (props) => {

    return (
        <Paper className={props.classes.paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>{i18next.t("date")}</TableCell>
                        <TableCell>{i18next.t("spent_time")}</TableCell>
                        <TableCell>{i18next.t("message")}</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.snapshotHistories && props.snapshotHistories.map(item => {
                        return (
                            <TableRow key={item.id}>
                                <TableCell>{item.date}</TableCell>
                                <TableCell>{item.time}</TableCell>
                                <TableCell>{item.message}</TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
};