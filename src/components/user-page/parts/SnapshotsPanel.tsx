import {Productivity} from "../../../types";
import React from "react";
import {Card, CardActionArea, CardActions, CardContent, CardHeader, CardMedia, Chip, Grid, LinearProgress, Paper, Popper, Typography} from "@material-ui/core";
import {Snapshot} from "../../../store/snapshot/types";
import {MatrixItem} from "../UserPage";
import i18next from "i18next";

type SnapshotsPanelProps = {
    classes: any;

    snapshotMatrix: Array<MatrixItem>,

    popperTarget: HTMLElement,
    open: boolean,
    snapshot: Snapshot;

    handleMouseOver: (event: React.MouseEvent, snapshot: Snapshot) => void;
    handleMouseOut: (event: React.MouseEvent) => void;
    changeSnapshotsProductivity: (snapshots: Array<Snapshot>, productivity: Productivity) => void;
}

export const SnapshotsPanel: React.SFC<SnapshotsPanelProps> = (props) => {

    return (
        <Paper className={props.classes.paper}>
            <Popper open={props.open} anchorEl={props.popperTarget}>
                {props.snapshot ?
                    <Card>
                        <CardActionArea>
                            <CardMedia component="img" image={props.snapshot!.resizedFileName}/>
                        </CardActionArea>
                        <CardContent>
                            <Typography>{i18next.t("taken")}: {props.snapshot!.time}</Typography>
                            <Typography>{i18next.t("memo")}: {props.snapshot!.message}</Typography>
                            <Typography>{i18next.t("active_window")}: {props.snapshot!.note}</Typography>
                            <Typography>{i18next.t("mouse_clicks")}: {props.snapshot!.countMouseClick}</Typography>
                            <Typography>{i18next.t("keyboards_clicks")}:{props.snapshot!.countKeyboardClick} </Typography>
                        </CardContent>
                    </Card>
                    : <Card/>}
            </Popper>

            <Grid container xs={12}
                  spacing={8}
                  direction="row"
                  justify="center"
                  alignItems="stretch">

                {props.snapshotMatrix.map((matrixItem: MatrixItem, i) => (
                    <Grid container item xs={12}
                          spacing={24}
                          direction="row"
                          alignItems="center" key={i}>
                        {matrixItem.count > 0 &&
                        <Grid item>
                            <Typography variant={"h4"}>{matrixItem.order + ":00"}</Typography>
                        </Grid>}
                        {matrixItem.snapshots.map((snapshot, j) => (
                            <Grid item xs key={j}>
                                {snapshot && <Card>
                                    <CardHeader subheader={snapshot.message}/>
                                    <CardActionArea>
                                        <CardMedia component="img"
                                                   onClick={() => window.open(snapshot.originalFileName.slice(0, -9))}
                                                   onMouseOver={(event) => {
                                                       props.handleMouseOver(event, snapshot);
                                                   }}
                                                   onMouseOut={(event) => {
                                                       props.handleMouseOut(event);
                                                   }}
                                                   image={snapshot!.resizedFileName ? snapshot!.resizedFileName : ""}
                                        />
                                    </CardActionArea>
                                    <CardActions>
                                        {snapshot!.productivity === Productivity.NEUTRAL &&
                                        <Chip label={snapshot!.time}
                                              onClick={() => props.changeSnapshotsProductivity([snapshot], Productivity.PRODUCTIVE)}/>}

                                        {snapshot!.productivity === Productivity.PRODUCTIVE &&
                                        <Chip color="primary" label={snapshot!.time}
                                              onClick={() => props.changeSnapshotsProductivity([snapshot], Productivity.UNPRODUCTIVE)}/>}

                                        {snapshot!.productivity === Productivity.UNPRODUCTIVE &&
                                        <Chip color="secondary" label={snapshot!.time}
                                              onClick={() => props.changeSnapshotsProductivity([snapshot], Productivity.NEUTRAL)}/>}

                                        <LinearProgress variant="determinate" value={snapshot!.userActivityPercent}
                                                        style={{height: 32, flexGrow: 1}}/>
                                    </CardActions>
                                </Card>}
                            </Grid>
                        ))}
                    </Grid>
                ))}
            </Grid>
        </Paper>
    );
};