import React from "react";
import {Grid, IconButton, Paper, Typography} from "@material-ui/core";
import ArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import ArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import {UserPositionInCompany} from "../../../store/snapshot/types";
import i18next from "i18next";

type InfoPanelProps = {
    classes: any;

    startTime: string;
    workTime: string;
    productivity: string;
    efficiency: string;
    averageActivity: number | string;
    userPosition: UserPositionInCompany;

    changeCompany: (offset: number) => void;
}

export const InfoPanel: React.FunctionComponent<InfoPanelProps> = (props) => {

    return (
        <Paper className={props.classes.paper}>
            <Grid container xl={12}
                  direction="row"
                  justify="flex-end"
                  alignItems="stretch">
                <Grid item container xs
                      direction="column"
                      justify="center"
                      alignItems="center">
                    <Typography variant="h6">{i18next.t("start_at")}:</Typography>
                    <Typography variant="h4">{props.startTime}</Typography>
                </Grid>
                <Grid item container xs
                      direction="column"
                      justify="center"
                      alignItems="center">
                    <Typography variant="h6">{i18next.t("time")}: </Typography>
                    <Typography variant="h4">{props.workTime}</Typography>
                    <Typography variant="h6">{i18next.t("productive_time")}: {props.productivity}</Typography>
                </Grid>
                <Grid item container xs
                      direction="column"
                      justify="center"
                      alignItems="center">
                    <Typography variant="h6">{i18next.t("efficiency")}:</Typography>
                    <Typography variant="h4">{props.efficiency}</Typography>
                    <Typography variant="h6">{"(" + props.productivity + "/" + props.workTime + ")" + "*" + props.averageActivity}</Typography>
                </Grid>
                <Grid item container xs
                      direction="column"
                      justify="center"
                      alignItems="center">
                    <Grid item container xs
                          direction="row"
                          justify="center"
                          alignItems="center">
                        <IconButton>
                            <ArrowLeftIcon onClick={() => props.changeCompany(-1)}/>
                        </IconButton>
                        <Typography variant="h6">{props.userPosition ? props.userPosition!.companyName : "--"}:</Typography>
                        <IconButton>
                            <ArrowRightIcon  onClick={() => props.changeCompany(1)}/>
                        </IconButton>
                    </Grid>
                    <Typography variant="h4">{props.userPosition ? props.userPosition!.employeePlace : "--"}</Typography>
                    <Typography variant="h6">{props.userPosition ? props.userPosition!.employeesCount : "--"}</Typography>
                </Grid>
            </Grid>
        </Paper>
    );
};