import React from "react";
import {createStyles, IconButton, Snackbar, Theme} from "@material-ui/core";

import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import InfoIcon from "@material-ui/icons/Info";
import WarningIcon from "@material-ui/icons/Warning";
import ErrorIcon from "@material-ui/icons/Error";
import CloseIcon from "@material-ui/icons/Close";
import withStyles from "@material-ui/core/es/styles/withStyles";
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import i18next from "i18next";

export type SnackbarVariant = "success" | "warning" | "error" | "info";

export type SnackbarProps = {
    open: boolean;
    message: string;
    variant: SnackbarVariant;
}

const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
};

type Props = SnackbarProps & { classes: any, handleClose: () => any; }

const styles = (theme: Theme) => createStyles({
    margin: {
        margin: theme.spacing.unit,
    },
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.dark,
    },
    warning: {
        backgroundColor: amber[700],
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing.unit,
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
});

export const SimpleSnackbar = withStyles(styles)((props: Props) => {
    const Icon: any = variantIcon[props.variant];
    const {classes, message, open} = props;
    return (
        <Snackbar
            className={classes.success}

            anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
            }}
            open={open}
            autoHideDuration={6000}
            onClose={props.handleClose}
            message={
                <span className={classes.message}>
                    <Icon className={classes.iconVariant}/>
                    {message}
                </span>
            }
            action={[
                <IconButton
                    key="close"
                    aria-label={i18next.t("close")}
                    color="inherit"
                    onClick={props.handleClose}>
                    <CloseIcon/>
                </IconButton>
            ]
            }
        >
        </Snackbar>
    )
});