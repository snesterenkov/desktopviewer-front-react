import React from "react";
import {MenuItem, TextField} from "@material-ui/core";

type DropDownProps = {
    label: string
    value?: number;
    data: Array<any>;

    defaultLabel?: string;

    required: boolean;

    handleChange: (event: React.ChangeEvent<HTMLSelectElement>) => any;
}

export const DropDown: React.FunctionComponent<DropDownProps> = (props: DropDownProps) => {
    return (
        <TextField
            fullWidth
            required={props.required}
            id={props.label}
            name={props.label}
            select
            label={props.label}
            value={props.value ? props.value : -1}
            onChange={props.handleChange}
            margin="normal"
        >
            {!props.required && <MenuItem key={-1} value={-1}>{props.defaultLabel} </MenuItem>}
            {props.data && props.data!.map((item) => (
                <MenuItem key={item.id} value={item.id}>
                    {item.name}
                </MenuItem>
            ))}
        </TextField>
    )
};