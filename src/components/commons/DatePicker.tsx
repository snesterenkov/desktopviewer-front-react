import React from "react";
import {createStyles, Grid, IconButton, Theme} from "@material-ui/core";
import {convertDateToFormatString, shiftDate} from "../../utils/DateTimeUtil";
import {DatePicker as MatDatePicker} from 'material-ui-pickers';

import ArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import ArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import CalendarIcon from "@material-ui/icons/Event";
import {Moment} from "moment";
import {withStyles} from "@material-ui/core/es";
import classNames from "classnames";

//todo переместить в другой пакет
type DatePickerProps = {
    label: string
    date: string;
    dates?: number[];
    handleChange: (date: string) => void;

    classes: any;
}

const styles = (theme: Theme) => createStyles({
    picker:{
        width: 'auto'
    },
    dayWrapper: {
        position: 'relative',
    },
    day: {
        width: 36,
        height: 36,
        fontSize: theme.typography.caption.fontSize,
        margin: '0 2px',
        color: 'inherit',
    },
    customDayHighlight: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: '2px',
        right: '2px',
        border: `1px solid ${theme.palette.secondary.main}`,
        borderRadius: '50%',
    },
    highlightNonCurrentMonthDay: {
        color: theme.palette.text.disabled,
    },
    highlightWorkingDay: {
        background: theme.palette.primary.light,
        color: theme.palette.common.white,
    },
    highlightSelectedDay: {
        background: theme.palette.primary.dark,
        color: theme.palette.common.white,
    }
});


const DatePicker: React.FunctionComponent<DatePickerProps> = (props) => {
    const selectedDate = props.date;
    const dates: Array<string> = props.dates ? props.dates.map(date => new Date(date).toDateString()) : [];

    const renderWrappedWeekDay = (date: Moment, selectedDate: Moment, dayInCurrentMonth: boolean) => {
        const {classes} = props;
        const wrapperClassName = classNames({
            [classes.highlightWorkingDay]: (dates.indexOf(date.toDate().toDateString()) > -1) && dayInCurrentMonth,
        });
        const dayClassName = classNames(classes.day, {
                [classes.highlightNonCurrentMonthDay]: !dayInCurrentMonth,
                [classes.highlightSelectedDay]: date.isSame(selectedDate)
            })
        ;
        return (
            <div className={wrapperClassName}>
                <IconButton className={dayClassName}>
                    <span> {date.format("D")} </span>
                </IconButton>
            </div>
        );
    };

    const handleShiftDate = (offset: number) => () => {
        return props.handleChange(shiftDate(selectedDate, offset));
    };

    return (
        <Grid container item xs
              direction="row"
              justify="center"
              alignContent="center"
              alignItems="center">
            <Grid item>
                <IconButton>
                    <ArrowLeftIcon onClick={handleShiftDate(-1)}/>
                </IconButton>
            </Grid>
            <Grid item className={props.classes.picker}>
                <MatDatePicker
                    fullWidth
                    keyboard
                    label={props.label}
                    format="YYYY-MM-DD"
                    mask={(value: any) =>
                        value
                            ? [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]
                            : []
                    }
                    value={selectedDate}
                    onChange={changeDate}
                    renderDay={renderWrappedWeekDay}
                    leftArrowIcon={<ArrowLeftIcon/>}
                    rightArrowIcon={<ArrowRightIcon/>}
                    keyboardIcon={<CalendarIcon/>}
                    disableOpenOnEnter
                    animateYearScrolling={false}/>
            </Grid>
            <Grid item>
                <IconButton>
                    <ArrowRightIcon onClick={handleShiftDate(1)}/>
                </IconButton>
            </Grid>
        </Grid>
    );

    function changeDate(moment: any) {
        props.handleChange(convertDateToFormatString(moment.toDate()))
    }
};

export default withStyles(styles)(DatePicker)