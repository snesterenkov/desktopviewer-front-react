import {Reducer} from "redux";

import * as snapshotActions from "./actions";
import {ActionType} from "typesafe-actions";
import {SnapshotActionTypes, SnapshotState} from "./types";

export type SnapshotAction = ActionType<typeof snapshotActions>;

const initialState: SnapshotState = {
    snapshots: [],
    dates: [],
    usersStats: {},
    isLoading: false
};

const reducer: Reducer<SnapshotState, SnapshotAction> = (state: SnapshotState = initialState, action: SnapshotAction) => {
    switch (action.type) {
        case SnapshotActionTypes.LOAD_DATES_WITH_SNAPSHOT_BY_USER_START:
        case SnapshotActionTypes.LOAD_SNAPSHOTS_BY_USER_AND_DATES_START:
        case SnapshotActionTypes.LOAD_USER_POSITION_IN_COMPANY_START:
        case SnapshotActionTypes.LOAD_USER_STATS_BY_PROJECT_AND_DATE_START:
        case SnapshotActionTypes.CHANGE_SNAPSHOT_PRODUCTIVITY_START:
            return {...state, isLoading: true};
        case SnapshotActionTypes.LOAD_DATES_WITH_SNAPSHOT_BY_USER_SUCCESS:
            return {...state, isLoading: false, dates: action.payload.dates};
        case SnapshotActionTypes.LOAD_SNAPSHOTS_BY_USER_AND_DATES_SUCCESS:
            return {...state, isLoading: false, snapshots: action.payload.snapshots};
        case SnapshotActionTypes.LOAD_USER_POSITION_IN_COMPANY_SUCCESS:
            return {...state, isLoading: false, userPosition: action.payload.userPosition};
        case SnapshotActionTypes.LOAD_USER_STATS_BY_PROJECT_AND_DATE_SUCCESS:
            return {...state, isLoading: false, usersStats: action.payload.usersStats};
        case SnapshotActionTypes.CHANGE_SNAPSHOT_PRODUCTIVITY_SUCCESS:
            state.snapshots
                .filter(snapshot => action.payload.snapshots.filter(updatedSnapshot => snapshot.id === updatedSnapshot.id).length !== 0)
                .forEach(snapshot => snapshot.productivity = action.payload.productivity);
            return {...state, isLoading: false};
        case SnapshotActionTypes.LOAD_DATES_WITH_SNAPSHOT_BY_USER_FAILURE:
        case SnapshotActionTypes.LOAD_SNAPSHOTS_BY_USER_AND_DATES_FAILURE:
        case SnapshotActionTypes.LOAD_USER_POSITION_IN_COMPANY_FAILURE:
        case SnapshotActionTypes.LOAD_USER_STATS_BY_PROJECT_AND_DATE_FAILURE:
        case SnapshotActionTypes.CHANGE_SNAPSHOT_PRODUCTIVITY_FAILURE:
            return {...state, isLoading: false,};
        default:
            return state
    }
};

export {reducer as snapshotReducer}