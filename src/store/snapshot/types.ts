import {Productivity} from "../../types";

export type Snapshot = {
    id: number,
    userId: number,
    file?: null,//TODO check backend
    note: string,
    message: string,
    originalFileName: string,
    resizedFileName: string,
    progectId: number,//это в беке так было)))
    countMouseClick: number,
    countKeyboardClick: number,
    timeInterval: number,
    userActivityPercent: number,
    date: string, //format YYYY-MM-DD
    time: string, //format HH-mm
    productivity: Productivity
}

export type UserPositionInCompany = {
    companyName: string,
    employeePlace: number,
    employeesCount: number,
}

export type SnapshotState = {
    snapshots: Array<Snapshot>,
    userPosition?: UserPositionInCompany,
    usersStats: any,
    dates: Array<number>,
    isLoading: boolean,
    message?: string
}

export enum SnapshotActionTypes {
    //datesWithSnapshotsByUser
    LOAD_DATES_WITH_SNAPSHOT_BY_USER_START = "@@snapshot/LOAD_DATES_WITH_SNAPSHOT_BY_USER_START",
    LOAD_DATES_WITH_SNAPSHOT_BY_USER_SUCCESS = "@@snapshot/LOAD_DATES_WITH_SNAPSHOT_BY_USER_SUCCESS",
    LOAD_DATES_WITH_SNAPSHOT_BY_USER_FAILURE = "@@snapshot/LOAD_DATES_WITH_SNAPSHOT_BY_USER_FAILURE",

    //snapshotsByUserAndDates
    LOAD_SNAPSHOTS_BY_USER_AND_DATES_START = "@@snapshot/LOAD_SNAPSHOTS_BY_USER_AND_DATES_START",
    LOAD_SNAPSHOTS_BY_USER_AND_DATES_SUCCESS = "@@snapshot/LOAD_SNAPSHOTS_BY_USER_AND_DATES_SUCCESS",
    LOAD_SNAPSHOTS_BY_USER_AND_DATES_FAILURE = "@@snapshot/LOAD_SNAPSHOTS_BY_USER_AND_DATES_FAILURE",

    //position in company for user
    LOAD_USER_POSITION_IN_COMPANY_START = "@@snapshot/LOAD_USER_POSITION_IN_COMPANY_START",
    LOAD_USER_POSITION_IN_COMPANY_SUCCESS = "@@snapshot/LOAD_USER_POSITION_IN_COMPANY_SUCCESS",
    LOAD_USER_POSITION_IN_COMPANY_FAILURE = "@@snapshot/LOAD_USER_POSITION_IN_COMPANY_FAILURE",

    //getUserStatsByProjectAndDate
    LOAD_USER_STATS_BY_PROJECT_AND_DATE_START = "@@snapshot/LOAD_USER_STATS_BY_PROJECT_AND_DATE_START",
    LOAD_USER_STATS_BY_PROJECT_AND_DATE_SUCCESS = "@@snapshot/LOAD_USER_STATS_BY_PROJECT_AND_DATE_SUCCESS",
    LOAD_USER_STATS_BY_PROJECT_AND_DATE_FAILURE = "@@snapshot/LOAD_USER_STATS_BY_PROJECT_AND_DATE_FAILURE",

    //changeProductivity
    CHANGE_SNAPSHOT_PRODUCTIVITY_START = "@@snapshot/CHANGE_SNAPSHOT_PRODUCTIVITY_START",
    CHANGE_SNAPSHOT_PRODUCTIVITY_SUCCESS = "@@snapshot/CHANGE_SNAPSHOT_PRODUCTIVITY_SUCCESS",
    CHANGE_SNAPSHOT_PRODUCTIVITY_FAILURE = "@@snapshot/CHANGE_SNAPSHOT_PRODUCTIVITY_FAILURE",

    //delete
    DELETE_SNAPSHOTS_START = "@@snapshot/DELETE_SNAPSHOTS_START",
    DELETE_SNAPSHOTS_SUCCESS = "@@snapshot/DELETE_SNAPSHOTS_SUCCESS",
    DELETE_SNAPSHOTS_FAILURE = "@@snapshot/DELETE_SNAPSHOTS_FAILURE",
}