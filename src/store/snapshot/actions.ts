import {Snapshot, SnapshotActionTypes, UserPositionInCompany} from "./types";
import {action} from "typesafe-actions";
import axios, {AxiosRequestConfig} from "axios";
import {Dispatch} from "redux";
import {API_SNAPSHOT_URL} from "../../constants";
import {createConfig} from "../../utils";
import {Productivity} from "../../types";
import {Company} from "../settings/company/types";
import {ProjectWithDetails} from "../settings/project/types";

export const loadDatesWithSnapshotByUserRequest = () => action(SnapshotActionTypes.LOAD_DATES_WITH_SNAPSHOT_BY_USER_START);
export const loadDatesWithSnapshotByUserSuccess = (dates: Array<number>) => action(SnapshotActionTypes.LOAD_DATES_WITH_SNAPSHOT_BY_USER_SUCCESS, {dates});
export const loadDatesWithSnapshotByUserFailure = (message: string) => action(SnapshotActionTypes.LOAD_DATES_WITH_SNAPSHOT_BY_USER_FAILURE, {message});

export const loadSnapshotByUserAndDatesRequest = () => action(SnapshotActionTypes.LOAD_SNAPSHOTS_BY_USER_AND_DATES_START);
export const loadSnapshotByUserAndDatesSuccess = (snapshots: Array<Snapshot>) => action(SnapshotActionTypes.LOAD_SNAPSHOTS_BY_USER_AND_DATES_SUCCESS, {snapshots});
export const loadSnapshotByUserAndDatesFailure = (message: string) => action(SnapshotActionTypes.LOAD_SNAPSHOTS_BY_USER_AND_DATES_FAILURE, {message});

export const loadUserPositionInCompanyRequest = () => action(SnapshotActionTypes.LOAD_USER_POSITION_IN_COMPANY_START);
export const loadUserPositionInCompanySuccess = (userPosition: UserPositionInCompany) => action(SnapshotActionTypes.LOAD_USER_POSITION_IN_COMPANY_SUCCESS, {userPosition});
export const loadUserPositionInCompanyFailure = (message: string) => action(SnapshotActionTypes.LOAD_USER_POSITION_IN_COMPANY_FAILURE, {message});

export const loadUserStatsByProjectAndDateRequest = () => action(SnapshotActionTypes.LOAD_USER_STATS_BY_PROJECT_AND_DATE_START);
export const loadUserStatsByProjectAndDateSuccess = (usersStats: any) => action(SnapshotActionTypes.LOAD_USER_STATS_BY_PROJECT_AND_DATE_SUCCESS, {usersStats});
export const loadUserStatsByProjectAndDateFailure = (message: string) => action(SnapshotActionTypes.LOAD_USER_STATS_BY_PROJECT_AND_DATE_FAILURE, {message});

export const changeSnapshotsProductivityRequest = () => action(SnapshotActionTypes.CHANGE_SNAPSHOT_PRODUCTIVITY_START);
export const changeSnapshotsProductivitySuccess = (snapshots: Array<Snapshot>, productivity: Productivity) => action(SnapshotActionTypes.CHANGE_SNAPSHOT_PRODUCTIVITY_SUCCESS, {
    snapshots,
    productivity
});
export const changeSnapshotsProductivityFailure = (message: string) => action(SnapshotActionTypes.CHANGE_SNAPSHOT_PRODUCTIVITY_FAILURE, {message});

export const deleteSnapshotsRequest = () => action(SnapshotActionTypes.DELETE_SNAPSHOTS_START);
export const deleteSnapshotsSuccess = () => action(SnapshotActionTypes.DELETE_SNAPSHOTS_SUCCESS);
export const deleteSnapshotsFailure = (message: string) => action(SnapshotActionTypes.DELETE_SNAPSHOTS_FAILURE, {message});

export const getDatesWithSnapshotByUser = (userId: number) => {
    return async (dispatch: Dispatch) => {
        const url = API_SNAPSHOT_URL + "/user/snapshots/datesWithSnapshots/" + userId;
        dispatch(loadDatesWithSnapshotByUserRequest());
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadDatesWithSnapshotByUserSuccess(res.data.dates));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadDatesWithSnapshotByUserFailure(err.message));
            });
    }
};

export const getSnapshotsByUserAndDates = (userId: number, startDate: string, endDate: string) => {
    return async (dispatch: Dispatch) => {
        const url = API_SNAPSHOT_URL + "/user/dates/" + userId;
        dispatch(loadSnapshotByUserAndDatesRequest());
        return await axios.get(url, createConfig(url, {startDate, endDate}))
            .then(res => {
                dispatch(loadSnapshotByUserAndDatesSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadSnapshotByUserAndDatesFailure(err.message));
            });
    }
};

export const getUserPositionInCompany = (userId: number, startDate: string, endDate: string, company: Company) => {
    return async (dispatch: Dispatch) => {
        const url = API_SNAPSHOT_URL + "/user/place/dates/" + userId;
        let config: AxiosRequestConfig = createConfig(url, {startDate, endDate});
        config.headers = {
            'Content-Type': 'application/json;charset=UTF-8'
        };
        dispatch(loadUserPositionInCompanyRequest());
        return await axios.post(url, company, config)
            .then(res => {
                dispatch(loadUserPositionInCompanySuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadUserPositionInCompanyFailure(err.message));
            });
    }
};

export const getUserStatsByProjectAndDate = (projects: Array<ProjectWithDetails>, date: string) => {
    return async (dispatch: Dispatch) => {
        const url = API_SNAPSHOT_URL + "/user/stats/projects/date";
        dispatch(loadUserStatsByProjectAndDateRequest());
        return await axios.post(url, projects, createConfig(url, {date}))
            .then(res => {
                dispatch(loadUserStatsByProjectAndDateSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadUserStatsByProjectAndDateFailure(err.message));
            });
    }
};

export const changeSnapshotsProductivity = (snapshots: Array<Snapshot>, productivity: Productivity) => {
    return async (dispatch: Dispatch) => {
        const url = API_SNAPSHOT_URL + "/productivity/change";
        dispatch(changeSnapshotsProductivityRequest());
        return await axios.post(url, snapshots, createConfig(url, {productivity}))
            .then(() => {
                dispatch(changeSnapshotsProductivitySuccess(snapshots, productivity));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(changeSnapshotsProductivityFailure(err.message));
            });
    }
};

export const deleteSnapshots = (snapshots: Array<Snapshot>) => {
    return async (dispatch: Dispatch) => {
        const url = API_SNAPSHOT_URL + "/delete";
        dispatch(deleteSnapshotsRequest());
        return await axios.post(url, snapshots, createConfig(url))
            .then(() => {
                dispatch(deleteSnapshotsSuccess());
            })
            .catch(err => {
                console.error(err.message);
                dispatch(deleteSnapshotsFailure(err.message));
            });
    }
};