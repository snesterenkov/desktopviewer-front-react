import {Dispatch} from "redux";
import {API_USER_SETTINGS_URL} from "../../constants";
import axios from "axios";
import {createConfig, getUserInfoFromStorage} from "../../utils";
import {action} from "typesafe-actions";
import {User} from "../../types";
import {UserPageActionTypes} from "./types";

export const loadObservedUserByIdRequest = () => action(UserPageActionTypes.LOAD_OBSERVED_USER_BY_ID_START);
export const loadObservedUserByIdSuccess = (observedUser: User) => action(UserPageActionTypes.LOAD_OBSERVED_USER_BY_ID_SUCCESS, {observedUser});
export const loadObservedUserByIdFailure = (message: string) => action(UserPageActionTypes.LOAD_OBSERVED_USER_BY_ID_FAILURE, {message});
export const loadObservedUserFromStorageSuccess = (observedUser: User) => action(UserPageActionTypes.LOAD_OBSERVED_USER_FROM_STORAGE_SUCCESS, {observedUser});

const loadObservedUser = (userId: number) => {
    return (dispatch: Dispatch) => {
        const url: string = `${API_USER_SETTINGS_URL}/${userId}`;
        dispatch(loadObservedUserByIdRequest());
        return axios.get(url, createConfig(url))
            .then((res) => {
                dispatch(loadObservedUserByIdSuccess(res.data));
                return Promise.resolve(res.data);
            })
            .catch(err => {
                console.error(err.message);
                const message = (err.response && err.response.data && err.response.data.message) ? err.response.data.message : err.message;
                dispatch(loadObservedUserByIdFailure(message));
                return Promise.reject(message);
            });
    }
};

export const getObservedUser = (userId?: number) => {
    if (userId) return loadObservedUser(userId);
    return (dispatch: Dispatch) => {
        const user: User = getUserInfoFromStorage();
        dispatch(loadObservedUserFromStorageSuccess(user));
        return Promise.resolve(user);
    }
};