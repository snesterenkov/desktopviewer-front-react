import {User} from "../../types";

export  type UserPageState = {
    observedUser?: User,
    isLoading: boolean,
    message?: string
}

export enum UserPageActionTypes {
    LOAD_OBSERVED_USER_BY_ID_START = "@@userPage/LOAD_OBSERVED_USER_BY_ID_START",
    LOAD_OBSERVED_USER_BY_ID_SUCCESS = "@@userPage/LOAD_OBSERVED_USER_BY_ID_SUCCESS",
    LOAD_OBSERVED_USER_BY_ID_FAILURE = "@@userPage/LOAD_OBSERVED_USER_BY_ID_FAILURE",
    LOAD_OBSERVED_USER_FROM_STORAGE_SUCCESS = "@@userPage/LOAD_OBSERVED_USER_FROM_STORAGE_SUCCESS",
}