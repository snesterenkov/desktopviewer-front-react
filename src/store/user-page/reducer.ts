import {ActionType} from "typesafe-actions";
import {Reducer} from "redux";

import * as userPageActions from "./actions";
import {UserPageActionTypes, UserPageState} from "./types";

export type UserPageAction = ActionType<typeof userPageActions>;

const initialState: UserPageState = {
    isLoading: false,
};

const reducer: Reducer<UserPageState> = (state: UserPageState = initialState, action: UserPageAction): UserPageState => {
    switch (action.type) {
        case UserPageActionTypes.LOAD_OBSERVED_USER_BY_ID_START:
            return {...state, isLoading: true};
        case UserPageActionTypes.LOAD_OBSERVED_USER_BY_ID_SUCCESS:
            return {...state, isLoading: false, observedUser: action.payload.observedUser};
        case UserPageActionTypes.LOAD_OBSERVED_USER_FROM_STORAGE_SUCCESS:
            return {...state, observedUser: action.payload.observedUser};
        case UserPageActionTypes.LOAD_OBSERVED_USER_BY_ID_FAILURE:
            return {...state, isLoading: false, message: action.payload.message};
        default:
            return state
    }
};

export {reducer as userPageReducer}