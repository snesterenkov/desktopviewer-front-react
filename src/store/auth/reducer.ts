import {ActionType} from "typesafe-actions";
import {Reducer} from "redux";

import * as authActions from "./actions";
import {Auth, AuthActionTypes, AuthState} from "./types";
import {User} from "../../types";
import {getUserFromStorage, getUserInfoFromStorage} from "../../utils";

export type AuthAction = ActionType<typeof authActions>;

let user: Auth = getUserFromStorage();
let userInfo: User = getUserInfoFromStorage();

const initialState: AuthState = {
    user: user,
    userInfo: userInfo,
    isAuthenticated: !!userInfo,
    isLoading: false
};

const reducer: Reducer<AuthState> = (state: AuthState = initialState, action: AuthAction) => {
    switch (action.type) {
        case AuthActionTypes.LOGIN:
            return {...state, isLoading: true, isAuthenticated: false};
        case AuthActionTypes.LOGIN_SUCCESS:
            return {...state, isLoading: false, isAuthenticated: true, user: action.payload.user, userInfo: action.payload.userInfo};
        case AuthActionTypes.LOGIN_FAILURE:
            return {...state, isLoading: false, isAuthenticated: false, error: action.payload.error};
        case AuthActionTypes.LOGOUT:
            return {...state, isLoading: false, isAuthenticated: false};
        default:
            return state
    }
};

export {reducer as authReducer}