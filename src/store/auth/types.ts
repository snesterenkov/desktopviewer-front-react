import {User} from "../../types";

export type Auth = {
    login: string,
    password: string
}

export type AuthState = {
    user: Auth,
    userInfo?: User,
    isAuthenticated: boolean,
    isLoading: boolean,
    error?: string
}

export enum AuthActionTypes {
    LOGIN = "@@auth/LOGIN",
    LOGIN_SUCCESS = "@@auth/LOGIN_SUCCESS",
    LOGIN_FAILURE = "@@auth/LOGIN_FAILURE",
    LOGOUT = "@@auth/LOGOUT",

    CONFIRM_REGISTRATION_REQUEST = "@@auth/CONFIRM_REGISTRATION_REQUEST",
    CONFIRM_REGISTRATION_SUCCESS = "@@auth/CONFIRM_REGISTRATION_SUCCESS",
    CONFIRM_REGISTRATION_FAILURE = "@@auth/CONFIRM_REGISTRATION_FAILURE",

}