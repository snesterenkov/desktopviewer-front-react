import axios from "axios";
import {Dispatch} from "redux";
import {action} from "typesafe-actions";
import {Auth, AuthActionTypes} from "./types";
import {createAuthConfig} from "../../utils";
import {API_AUTH_URL, API_CONFIRM_REGISTRATION_URL, LOGIN_URL, MY_PAGE_URL} from "../../constants";

import history from '../browserHistory';
import {User} from "../../types";

export const requestLogin = (user: Auth) => action(AuthActionTypes.LOGIN, {user});
export const receiveLogin = (user: Auth, userInfo: User) => action(AuthActionTypes.LOGIN_SUCCESS, {user, userInfo});
export const loginError = (message: string) => action(AuthActionTypes.LOGIN_FAILURE, {error: message});
export const logout = () => action(AuthActionTypes.LOGOUT);

export const confirmRegistrationRequest = () => action(AuthActionTypes.CONFIRM_REGISTRATION_REQUEST);
export const confirmRegistrationSuccess = () => action(AuthActionTypes.CONFIRM_REGISTRATION_SUCCESS);
export const confirmRegistrationFailure = (error: string) => action(AuthActionTypes.CONFIRM_REGISTRATION_FAILURE, {error});


export const loginUser = (user: Auth) => {
    return async (dispatch: Dispatch) => {
        dispatch(requestLogin(user));
        return await axios.get(API_AUTH_URL, createAuthConfig(API_AUTH_URL, user))
            .then(res => {
                localStorage.setItem("user", JSON.stringify(user));
                sessionStorage.setItem("userInfo", JSON.stringify(res.data));
                dispatch(receiveLogin(user, res.data));
            })
            .then(() =>
                history.push(MY_PAGE_URL)
            )
            .catch(err => {
                console.error(err.message);
                const message = (err.response && err.response.data && err.response.data.message) ? err.response.data.message : err.message;
                dispatch(loginError(message));
                return Promise.reject(message);
            });
    }
};

export const logoutUser = () => (dispatch: Dispatch) => {
    localStorage.clear();
    sessionStorage.clear();
    history.push(LOGIN_URL);
    dispatch(logout());
};

export const confirmRegistration = (token: string) => {
    return (dispatch: Dispatch) => {
        const url: string = `${API_CONFIRM_REGISTRATION_URL}/${token}`;
        dispatch(confirmRegistrationRequest());
        return axios.get(url)
            .then(res => {
                dispatch(confirmRegistrationSuccess());
                return Promise.resolve();
            })
            .catch(err => {
                console.error(err.message);
                const message = (err.response && err.response.data && err.response.data.message) ? err.response.data.message : err.message;
                dispatch(confirmRegistrationFailure(message));
                return Promise.reject(message);
            });
    }
};