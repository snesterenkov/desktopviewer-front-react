import {combineReducers} from "redux";
import {AuthState} from "./auth/types";
import {authReducer} from "./auth/reducer";
import {UserSettingsState} from "./settings/user/types";
import {usersSettingsReducer} from "./settings/user/reducer";
import {CompanySettingState} from "./settings/company/types";
import {companySettingsReducer} from "./settings/company/reducer";
import {roleSettingsReducer} from "./settings/role/reducer";
import {RoleSettingState} from "./settings/role/types";
import {DepartmentSettingState} from "./settings/department/types";
import {departmentSettingsReducer} from "./settings/department/reducer";
import {ProjectSettingState} from "./settings/project/types";
import {projectSettingsReducer} from "./settings/project/reducer";
import {ApplicationSettingState} from "./settings/application/types";
import {applicationSettingsReducer} from "./settings/application/reducer";
import {SnapshotState} from "./snapshot/types";
import {snapshotReducer} from "./snapshot/reducer";
import {WorkDiaryState} from "./reports/workdiary/types";
import {workDiaryReducer} from "./reports/workdiary/reducer";
import {PermissionSettingState} from "./settings/permission/types";
import {permissionSettingsReducer} from "./settings/permission/reducer";
import {UserPageState} from "./user-page/types";
import {userPageReducer} from "./user-page/reducer";
import {ServerStorageState} from "./settings/server/storage/types";
import {serverStorageReducer} from "./settings/server/storage/reducer";
import {ServerProxyState} from "./settings/server/proxy/types";
import {serverProxyReducer} from "./settings/server/proxy/reducer";
import {ServerClientState} from "./settings/server/client/types";
import {serverClientReducer} from "./settings/server/client/reducer";
import {ServerFilesState} from "./settings/server/files/types";
import {serverFilesReducer} from "./settings/server/files/reducer";
import {InviteState} from "./invite/types";
import {inviteReducer} from "./invite/reducer";

export interface ApplicationState {
    auth: AuthState,
    userPage: UserPageState,
    invite: InviteState,
    companySettings: CompanySettingState,//todo merge into settings
    departmentSettings: DepartmentSettingState,
    projectSettings: ProjectSettingState,
    applicationSettings: ApplicationSettingState,
    roleSettings: RoleSettingState,
    serverStorage: ServerStorageState,
    serverProxy: ServerProxyState,
    serverFiles: ServerFilesState,
    serverClient: ServerClientState,
    permissionSettings: PermissionSettingState,
    userSettings: UserSettingsState,
    snapshot: SnapshotState,
    workDiary: WorkDiaryState
    // settings: SettingsState,
}

export const rootReducer = combineReducers<ApplicationState>({
    auth: authReducer,
    invite: inviteReducer,
    userPage: userPageReducer,
    companySettings: companySettingsReducer,
    departmentSettings: departmentSettingsReducer,
    projectSettings: projectSettingsReducer,
    applicationSettings: applicationSettingsReducer,
    roleSettings: roleSettingsReducer,
    serverStorage: serverStorageReducer,
    serverProxy: serverProxyReducer,
    serverFiles: serverFilesReducer,
    serverClient: serverClientReducer,
    permissionSettings: permissionSettingsReducer,
    userSettings: usersSettingsReducer,
    snapshot: snapshotReducer,
    workDiary: workDiaryReducer
    // settings: settingsReducer
});