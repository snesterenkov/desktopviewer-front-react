import axios from "axios";
import {Dispatch} from "redux";
import {action} from "typesafe-actions";
import {UserSettingsActionTypes} from "./types";
import {User} from "../../../types";
import {createConfig} from "../../../utils";
import {API_USER_SETTINGS_URL} from "../../../constants";
import {REGISTRATION_SUCCESS, RESTORE_PASSWORD_REQUEST_SUCCESS, RESTORE_PASSWORD_SUCCESS} from "../../../constants/apiMessages";

export const loadUsersRequest = () => action(UserSettingsActionTypes.LOAD_USERS_START);
export const loadUsersSuccess = (users: Array<User>) => action(UserSettingsActionTypes.LOAD_USERS_SUCCESS, {users});
export const loadUsersFailure = (message: string) => action(UserSettingsActionTypes.LOAD_USERS_FAILURE, {message});

export const loadUserByIdRequest = () => action(UserSettingsActionTypes.LOAD_USER_BY_ID_START);
export const loadUserByIdSuccess = (observedUser: User) => action(UserSettingsActionTypes.LOAD_USER_BY_ID_SUCCESS, {observedUser});
export const loadUserByIdFailure = (message: string) => action(UserSettingsActionTypes.LOAD_USER_BY_ID_FAILURE, {message});

export const loadFreeUsersForCompanyRequest = () => action(UserSettingsActionTypes.LOAD_FREE_USERS_FOR_COMPANY_START);
export const loadFreeUsersForCompanySuccess = (freeUsers: Array<User>) => action(UserSettingsActionTypes.LOAD_FREE_USERS_FOR_COMPANY_SUCCESS, {freeUsers});
export const loadFreeUsersForCompanyFailure = (message: string) => action(UserSettingsActionTypes.LOAD_FREE_USERS_FOR_COMPANY_FAILURE, {message});

export const loadFreeUsersForDepartmentRequest = () => action(UserSettingsActionTypes.LOAD_FREE_USERS_FOR_DEPARTMENT_START);
export const loadFreeUsersForDepartmentSuccess = (freeUsers: Array<User>) => action(UserSettingsActionTypes.LOAD_FREE_USERS_FOR_DEPARTMENT_SUCCESS, {freeUsers});
export const loadFreeUsersForDepartmentFailure = (message: string) => action(UserSettingsActionTypes.LOAD_FREE_USERS_FOR_DEPARTMENT_FAILURE, {message});

export const loadFreeUsersForProjectRequest = () => action(UserSettingsActionTypes.LOAD_FREE_USERS_FOR_PROJECT_START);
export const loadFreeUsersForProjectSuccess = (freeUsers: Array<User>) => action(UserSettingsActionTypes.LOAD_FREE_USERS_FOR_PROJECT_SUCCESS, {freeUsers});
export const loadFreeUsersForProjectFailure = (message: string) => action(UserSettingsActionTypes.LOAD_FREE_USERS_FOR_PROJECT_FAILURE, {message});

export const addUserRequest = () => action(UserSettingsActionTypes.ADD_USER_START);
export const addUserSuccess = (user: User) => action(UserSettingsActionTypes.ADD_USER_SUCCESS, {lastAddedUser: user});
export const addUserFailure = (message: string) => action(UserSettingsActionTypes.ADD_USER_FAILURE, {message});

export const updateUserRequest = () => action(UserSettingsActionTypes.UPDATE_USER_START);
export const updateUserSuccess = (user: User) => action(UserSettingsActionTypes.UPDATE_USER_SUCCESS, {lastUpdatedUser: user});
export const updateUserFailure = (message: string) => action(UserSettingsActionTypes.UPDATE_USER_FAILURE, {message});

export const deleteUserRequest = () => action(UserSettingsActionTypes.DELETE_USER_START);
export const deleteUserSuccess = (userId: number) => action(UserSettingsActionTypes.DELETE_USER_SUCCESS, {userId});
export const deleteUserFailure = (message: string) => action(UserSettingsActionTypes.DELETE_USER_FAILURE, {message});

export const recoveryPasswordRequest = () => action(UserSettingsActionTypes.RECOVERY_PASSWORD_REQUEST);
export const recoveryPasswordSuccess = () => action(UserSettingsActionTypes.RECOVERY_PASSWORD_SUCCESS);
export const recoveryPasswordFailure = (message: string) => action(UserSettingsActionTypes.RECOVERY_PASSWORD_FAILURE, {message});

export const changePasswordRequest = () => action(UserSettingsActionTypes.CHANGE_PASSWORD_REQUEST);
export const changePasswordSuccess = () => action(UserSettingsActionTypes.CHANGE_PASSWORD_SUCCESS);
export const changePasswordFailure = (message: string) => action(UserSettingsActionTypes.CHANGE_PASSWORD_FAILURE, {message});


export const getUsers = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadUsersRequest());
        return await axios.get(API_USER_SETTINGS_URL, createConfig(API_USER_SETTINGS_URL))
            .then(res => {
                dispatch(loadUsersSuccess(res.data));
            })
            .catch(err => {
                console.log(err.message);
                dispatch(loadUsersFailure(err.message));
            });
    }
};

export const getFreeUsersForCompany = (companyId: number) => {
    return async (dispatch: Dispatch) => {
        const url: string = `${API_USER_SETTINGS_URL}/freeUsersForCompany`;
        dispatch(loadFreeUsersForCompanyRequest());
        return await axios.get(url, createConfig(url, {companyid: companyId}))
            .then((res) => {
                dispatch(loadFreeUsersForCompanySuccess(res.data));
            })
            .catch(err => {
                console.log(err.message);
                dispatch(loadFreeUsersForCompanyFailure(err.message));
            });
    }
};

export const getFreeUsersForDepartment = (departmentId: number) => {
    return async (dispatch: Dispatch) => {
        const url: string = `${API_USER_SETTINGS_URL}/freeUsersForDepartment`;
        dispatch(loadFreeUsersForDepartmentRequest());
        return await axios.get(url, createConfig(url, {departmentid: departmentId}))
            .then((res) => {
                dispatch(loadFreeUsersForDepartmentSuccess(res.data));
            })
            .catch(err => {
                console.log(err.message);
                dispatch(loadFreeUsersForDepartmentFailure(err.message));
            });
    }
};

export const getFreeUsersForProject = (projectId: number) => {
    return async (dispatch: Dispatch) => {
        const url: string = `${API_USER_SETTINGS_URL}/free`;
        dispatch(loadFreeUsersForProjectRequest());
        return await axios.get(url, createConfig(url, {projectid: projectId}))
            .then((res) => {
                dispatch(loadFreeUsersForProjectSuccess(res.data));
            })
            .catch(err => {
                console.log(err.message);
                dispatch(loadFreeUsersForProjectFailure(err.message));
            });
    }
};

export const getUserById = (userId: number) => {
    return async (dispatch: Dispatch) => {
        const url: string = `${API_USER_SETTINGS_URL}/${userId}`;
        dispatch(loadUserByIdRequest());
        return await axios.get(url, createConfig(url))
            .then((res) => {
                dispatch(loadUserByIdSuccess(res.data));
            })
            .catch(err => {
                console.log(err.message);
                dispatch(loadUserByIdFailure(err.message));
            });
    }
};

export const addUser = (newUser: User) => {
    return async (dispatch: Dispatch) => {
        dispatch(addUserRequest());
        return await axios.post(API_USER_SETTINGS_URL, newUser, createConfig(API_USER_SETTINGS_URL))
            .then(res => {
                dispatch(addUserSuccess(res.data));
                return {success: true, message: REGISTRATION_SUCCESS};
            })
            .catch(err => {
                console.log(err.message);
                const message = (err.response && err.response.data && err.response.data.message) ? err.response.data.message : err.message;
                dispatch(addUserFailure(message));
                return {success: false, message};
            });
    }
};

export const updateUser = (updatedUser: User) => {
    return async (dispatch: Dispatch) => {
        const url = `${API_USER_SETTINGS_URL}/${updatedUser.id}`;
        dispatch(updateUserRequest());
        return await axios.put(url, updatedUser, createConfig(url))
            .then(res => {
                dispatch(updateUserSuccess(res.data));
            })
            .catch(err => {
                console.log(err.message);
                dispatch(updateUserFailure(err.message));
            });
    }
};

export const deleteUser = (userId: number) => {
    return async (dispatch: Dispatch) => {
        const url = `${API_USER_SETTINGS_URL}/${userId}`;
        dispatch((deleteUserRequest()));
        return await axios.delete(url, createConfig(url))
            .then(() => {
                dispatch(deleteUserSuccess(userId));
            })
            .catch(err => {
                console.log(err.message);
                dispatch(deleteUserFailure(err.message));
            });
    }
};

export const recoveryPassword = (email: string) => {
    return (dispatch: Dispatch) => {
        const url = `${API_USER_SETTINGS_URL}/requestOnChangingPassword`;
        dispatch(recoveryPasswordRequest());
        return axios.get(url, {params: {email}})
            .then(res => {
                dispatch(recoveryPasswordSuccess());
                return {success: true, message: RESTORE_PASSWORD_REQUEST_SUCCESS};
            })
            .catch(err => {
                console.error(err.message);
                const message = (err.response && err.response.data && err.response.data.message) ? err.response.data.message : err.message;
                dispatch(recoveryPasswordFailure(message));
                return {success: false, message};
            });
    }
};

export const changePassword = (token: string, password: string) => {
    return async (dispatch: Dispatch) => {
        const url = `${API_USER_SETTINGS_URL}/changePassword/${token}`;
        dispatch(changePasswordRequest());
        return await axios.get(url, {params: {password}})
            .then(res => {
                dispatch(changePasswordSuccess());
                return {success: true, message: RESTORE_PASSWORD_SUCCESS};
            })
            .catch(err => {
                console.error(err.message);
                const message = (err.response && err.response.data && err.response.data.message) ? err.response.data.message : err.message;
                dispatch(changePasswordFailure(message));
                return {success: false, message};
            });
    }
};