import {ActionType} from "typesafe-actions";
import {Reducer} from "redux";

import * as userSettingsActions from "./actions";
import {UserSettingsActionTypes, UserSettingsState} from "./types";
import {User} from "../../../types";

export type UserSettingsAction = ActionType<typeof userSettingsActions>;

const initialState: UserSettingsState = {
    users: [],
    freeUsers: [],
    lastAddedUser: {},
    lastUpdatedUser: {},
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<UserSettingsState, UserSettingsAction> = (state: UserSettingsState = initialState, action: UserSettingsAction) => {
    switch (action.type) {
        case UserSettingsActionTypes.LOAD_USERS_START:
        case UserSettingsActionTypes.LOAD_USER_BY_ID_START:
        case UserSettingsActionTypes.LOAD_FREE_USERS_FOR_COMPANY_START:
        case UserSettingsActionTypes.LOAD_FREE_USERS_FOR_DEPARTMENT_START:
        case UserSettingsActionTypes.LOAD_FREE_USERS_FOR_PROJECT_START:
        case UserSettingsActionTypes.ADD_USER_START:
        case UserSettingsActionTypes.UPDATE_USER_START:
        case UserSettingsActionTypes.DELETE_USER_START:
            return {...state, isLoading: true, isLoaded: false};
        case UserSettingsActionTypes.LOAD_USERS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, users: action.payload.users};
        case UserSettingsActionTypes.LOAD_USER_BY_ID_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, observedUser: action.payload.observedUser};
        case UserSettingsActionTypes.LOAD_FREE_USERS_FOR_COMPANY_SUCCESS:
        case UserSettingsActionTypes.LOAD_FREE_USERS_FOR_DEPARTMENT_SUCCESS:
        case UserSettingsActionTypes.LOAD_FREE_USERS_FOR_PROJECT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                freeUsers: action.payload.freeUsers
            };
        case UserSettingsActionTypes.ADD_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                users: [...state.users, action.payload.lastAddedUser],
                lastAddedUser: action.payload.lastAddedUser
            };
        case UserSettingsActionTypes.UPDATE_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                users: (state.users as Array<User>).map((user: User) =>
                    user.id == action.payload.lastUpdatedUser.id ? action.payload.lastUpdatedUser : user),
                lastUpdatedUser: action.payload.lastUpdatedUser
            };
        case UserSettingsActionTypes.DELETE_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                users: (state.users as Array<User>).filter((item: User) => item.id != action.payload.userId),
            };
        case UserSettingsActionTypes.LOAD_USERS_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message, users: []};
        case UserSettingsActionTypes.LOAD_FREE_USERS_FOR_COMPANY_FAILURE:
        case UserSettingsActionTypes.LOAD_FREE_USERS_FOR_DEPARTMENT_FAILURE:
        case UserSettingsActionTypes.LOAD_FREE_USERS_FOR_PROJECT_FAILURE:
            return {
                ...state,
                isLoading: false,
                isLoaded: false,
                message: action.payload.message,
                freeUsers: []
            };
        case UserSettingsActionTypes.ADD_USER_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message, lastAddedUser: {}};
        case UserSettingsActionTypes.UPDATE_USER_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message, lastUpdatedUser: {}};
        case UserSettingsActionTypes.LOAD_USER_BY_ID_FAILURE:
        case UserSettingsActionTypes.DELETE_USER_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }

};

export {reducer as usersSettingsReducer}