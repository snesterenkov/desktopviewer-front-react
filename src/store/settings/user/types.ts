import {User} from "../../../types";

export type UserSettingsState = {
    users: Array<User> | [],
    freeUsers : Array<User> | [];
    lastAddedUser: User | {},
    lastUpdatedUser: User | {}
    observedUser?: User,
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum UserSettingsActionTypes {
    LOAD_USERS_START = "@@settings/user/LOAD_USERS_START",
    LOAD_USERS_SUCCESS = "@@settings/user/LOAD_USERS_SUCCESS",
    LOAD_USERS_FAILURE = "@@settings/user/LOAD_USERS_FAILURE",

    LOAD_USER_BY_ID_START = "@@settings/user/LOAD_USER_BY_ID_START",
    LOAD_USER_BY_ID_SUCCESS = "@@settings/user/LOAD_USER_BY_ID_SUCCESS",
    LOAD_USER_BY_ID_FAILURE = "@@settings/user/LOAD_USER_BY_ID_FAILURE",

    LOAD_FREE_USERS_FOR_COMPANY_START = "@@settings/user/LOAD_FREE_USERS_FOR_COMPANY_START",
    LOAD_FREE_USERS_FOR_COMPANY_SUCCESS = "@@settings/user/LOAD_FREE_USERS_FOR_COMPANY_SUCCESS",
    LOAD_FREE_USERS_FOR_COMPANY_FAILURE = "@@settings/user/LOAD_FREE_USERS_FOR_COMPANY_FAILURE",

    LOAD_FREE_USERS_FOR_DEPARTMENT_START = "@@settings/user/LOAD_FREE_USERS_FOR_DEPARTMENT_START",
    LOAD_FREE_USERS_FOR_DEPARTMENT_SUCCESS = "@@settings/user/LOAD_FREE_USERS_FOR_DEPARTMENT_SUCCESS",
    LOAD_FREE_USERS_FOR_DEPARTMENT_FAILURE = "@@settings/user/LOAD_FREE_USERS_FOR_DEPARTMENT_FAILURE",

    LOAD_FREE_USERS_FOR_PROJECT_START = "@@settings/user/LOAD_FREE_USERS_FOR_PROJECT_START",
    LOAD_FREE_USERS_FOR_PROJECT_SUCCESS = "@@settings/user/LOAD_FREE_USERS_FOR_PROJECT_SUCCESS",
    LOAD_FREE_USERS_FOR_PROJECT_FAILURE = "@@settings/user/LOAD_FREE_USERS_FOR_PROJECT_FAILURE",

    ADD_USER_START = "@@settings/user/ADD_USER_START",
    ADD_USER_SUCCESS = "@@settings/user/ADD_USER_SUCCESS",
    ADD_USER_FAILURE = "@@settings/user/ADD_USER_FAILURE",

    UPDATE_USER_START = "@@settings/user/UPDATE_USER_START",
    UPDATE_USER_SUCCESS = "@@settings/user/UPDATE_USER_SUCCESS",
    UPDATE_USER_FAILURE = "@@settings/user/UPDATE_USER_FAILURE",

    DELETE_USER_START = "@@settings/user/DELETE_USER_START",
    DELETE_USER_SUCCESS = "@@settings/user/DELETE_USER_SUCCESS",
    DELETE_USER_FAILURE = "@@settings/user/DELETE_USER_FAILURE",

    RECOVERY_PASSWORD_REQUEST = "@@settings/user/RECOVERY_PASSWORD_REQUEST",
    RECOVERY_PASSWORD_SUCCESS = "@@settings/user/RECOVERY_PASSWORD_SUCCESS",
    RECOVERY_PASSWORD_FAILURE = "@@settings/user/RECOVERY_PASSWORD_FAILURE",

    CHANGE_PASSWORD_REQUEST = "@@settings/user/CHANGE_PASSWORD_REQUEST",
    CHANGE_PASSWORD_SUCCESS = "@@settings/user/CHANGE_PASSWORD_SUCCESS",
    CHANGE_PASSWORD_FAILURE = "@@settings/user/CHANGE_PASSWORD_FAILURE",
}