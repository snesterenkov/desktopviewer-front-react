import {Reducer} from "redux";
import * as permissionSettingsActions from "./actions";
import {ActionType} from "typesafe-actions";
import {PermissionSettingsActionTypes, PermissionSettingState} from "./types";

export type PermissionSettingAction = ActionType<typeof permissionSettingsActions>;

const initialState: PermissionSettingState = {
    permissions: [],
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<PermissionSettingState, PermissionSettingAction> = (state: PermissionSettingState = initialState, action: PermissionSettingAction) => {
    switch (action.type) {
        case PermissionSettingsActionTypes.LOAD_PERMISSIONS_START:
            return {...state, isLoading: true, isLoaded: false};
        case PermissionSettingsActionTypes.LOAD_PERMISSIONS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, permissions: action.payload.permissions};
        case PermissionSettingsActionTypes.LOAD_PERMISSIONS_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }

};

export {reducer as permissionSettingsReducer}