export type Permission = {
    id?: number,
    code: string,
    name: string
}

export type PermissionSettingState = {
    permissions: Array<Permission> | [],
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum PermissionSettingsActionTypes {
    LOAD_PERMISSIONS_START = "@@settings/permission/LOAD_PERMISSIONS_START",
    LOAD_PERMISSIONS_SUCCESS = "@@settings/permission/LOAD_PERMISSIONS_SUCCESS",
    LOAD_PERMISSIONS_FAILURE = "@@settings/permission/LOAD_PERMISSIONS_FAILURE",
}