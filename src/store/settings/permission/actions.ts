import {action} from "typesafe-actions";
import {Permission, PermissionSettingsActionTypes} from "./types";
import {API_PERMISSION_SETTINGS_URL} from "../../../constants";
import {createConfig} from "../../../utils";
import {Dispatch} from "redux";
import axios from "axios";

export const loadPermissionsRequest = () => action(PermissionSettingsActionTypes.LOAD_PERMISSIONS_START);
export const loadPermissionsSuccess = (permissions: Array<Permission>) =>
    action(PermissionSettingsActionTypes.LOAD_PERMISSIONS_SUCCESS, {permissions: permissions});
export const loadPermissionsFailure = (message: string) =>
    action(PermissionSettingsActionTypes.LOAD_PERMISSIONS_FAILURE, {message: message});

export const getAllPermissions = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadPermissionsRequest());
        return await axios.get(API_PERMISSION_SETTINGS_URL, createConfig(API_PERMISSION_SETTINGS_URL))
            .then(res => {
                dispatch(loadPermissionsSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(loadPermissionsFailure(errorMessage));
            });
    }
};

