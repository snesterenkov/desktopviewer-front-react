import {ProxyCredentials, ProxySettings} from "./types";

export function getEmptyProxySettings(): ProxySettings {
    return {
        nonProxyHosts: "",
        proxyCredentials: getEmptyProxyCredentials(),
        proxyHost: "",
        proxyPort: "",
        proxyType: ""
    }
}

export function getEmptyProxyCredentials(): ProxyCredentials {
    return {
        username: "",
        password: ""
    }
}