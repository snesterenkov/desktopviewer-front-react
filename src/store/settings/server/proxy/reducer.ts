import {ActionType} from "typesafe-actions";
import * as serverProxyActions from "../../server/proxy/actions";
import {Reducer} from "redux";
import {ServerProxyActionTypes, ServerProxyState} from "./types";
import {getEmptyProxySettings} from "./utils";

export type ServerProxyAction = ActionType<typeof serverProxyActions>;

const initialState: ServerProxyState = {
    proxySettings: getEmptyProxySettings(),
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<ServerProxyState, ServerProxyAction> = (state: ServerProxyState = initialState, action: ServerProxyAction) => {
    switch (action.type) {
        case ServerProxyActionTypes.LOAD_PROXY_SETTINGS_START:
        case ServerProxyActionTypes.SET_PROXY_SETTINGS_START:
            return {...state, isLoading: true, isLoaded: false};
        case ServerProxyActionTypes.LOAD_PROXY_SETTINGS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, proxySettings: action.payload.proxySettings};
        case ServerProxyActionTypes.SET_PROXY_SETTINGS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, proxySettings: action.payload.proxySettings};
        case ServerProxyActionTypes.LOAD_PROXY_SETTINGS_FAILURE:
        case ServerProxyActionTypes.SET_PROXY_SETTINGS_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }
};

export {reducer as serverProxyReducer}