import {action} from "typesafe-actions";
import {Dispatch} from "redux";
import {API_SERVER_PROXY_URL} from "../../../../constants";
import axios from "axios";
import {createConfig} from "../../../../utils";
import {ProxySettings, ServerProxyActionTypes} from "./types";

export const loadProxyRequest = () => action(ServerProxyActionTypes.LOAD_PROXY_SETTINGS_START);
export const loadProxySuccess = (proxySettings: ProxySettings) =>
    action(ServerProxyActionTypes.LOAD_PROXY_SETTINGS_SUCCESS, {proxySettings});
export const loadProxyFailure = (message: string) =>
    action(ServerProxyActionTypes.LOAD_PROXY_SETTINGS_FAILURE, {message});

export const setProxySettingsRequest = () => action(ServerProxyActionTypes.SET_PROXY_SETTINGS_START);
export const setProxySettingsSuccess = (proxySettings: ProxySettings) =>
    action(ServerProxyActionTypes.SET_PROXY_SETTINGS_SUCCESS, {proxySettings});
export const setProxySettingsFailure = (message: string) =>
    action(ServerProxyActionTypes.SET_PROXY_SETTINGS_FAILURE, {message});

export const getProxySettings = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadProxyRequest());
        return await axios.get(API_SERVER_PROXY_URL, createConfig(API_SERVER_PROXY_URL))
            .then(res => {
                dispatch(loadProxySuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(loadProxyFailure(errorMessage));
            });
    }
};

export const setProxySettings = (proxySettings: ProxySettings) => {
    return async (dispatch: Dispatch) => {
        dispatch(setProxySettingsRequest());
        return await axios.post(API_SERVER_PROXY_URL, proxySettings, createConfig(API_SERVER_PROXY_URL))
            .then(() => {
                dispatch(setProxySettingsSuccess(proxySettings));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(setProxySettingsFailure(errorMessage));
            });
    }
};