export type ProxySettings = {
    nonProxyHosts: string
    proxyCredentials: ProxyCredentials
    proxyHost: string
    proxyPort: string
    proxyType: string
}

export type ProxyCredentials ={
    username: string
    password: string
}

export type ServerProxyState = {
    proxySettings: ProxySettings,
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum ServerProxyActionTypes {
    LOAD_PROXY_SETTINGS_START = "@@settings/server/proxy/LOAD_PROXY_SETTINGS_START",
    LOAD_PROXY_SETTINGS_SUCCESS = "@@settings/server/proxy/LOAD_PROXY_SETTINGS_SUCCESS",
    LOAD_PROXY_SETTINGS_FAILURE = "@@settings/server/proxy/LOAD_PROXY_SETTINGS_FAILURE",

    SET_PROXY_SETTINGS_START = "@@settings/server/proxy/SET_PROXY_SETTINGS_START",
    SET_PROXY_SETTINGS_SUCCESS = "@@settings/server/proxy/SET_PROXY_SETTINGS_SUCCESS",
    SET_PROXY_SETTINGS_FAILURE = "@@settings/server/proxy/SET_PROXY_SETTINGS_FAILURE"
}