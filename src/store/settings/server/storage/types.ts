export type Storage = {
    type: string,
    current: boolean
}

export type GoogleDriveParams = {
    serviceAccountId: string,
    secretFileLocation: string,
    storageFolderName: string,
    tempFolderName: string,
}

export type LocalStorageParams = {
    imagesHostURL: string,
    storageFolderName: string
}

export type ServerStorageState = {
    storages: Storage[],
    googleDriveParams: GoogleDriveParams,
    localStorageParams: LocalStorageParams,
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum ServerStorageActionTypes {
    LOAD_STORAGES_START = "@@settings/server/storage/LOAD_STORAGES_START",
    LOAD_STORAGES_SUCCESS = "@@settings/server/storage/LOAD_STORAGES_SUCCESS",
    LOAD_STORAGES_FAILURE = "@@settings/server/storage/LOAD_STORAGES_FAILURE",

    LOAD_GOOGLE_DRIVE_START = "@@settings/server/storage/LOAD_GOOGLE_DRIVE_START",
    LOAD_GOOGLE_DRIVE_SUCCESS = "@@settings/server/storage/LOAD_GOOGLE_DRIVE_SUCCESS",
    LOAD_GOOGLE_DRIVE_FAILURE = "@@settings/server/storage/LOAD_GOOGLE_DRIVE_FAILURE",

    LOAD_LOCAL_STORAGE_START = "@@settings/server/storage/LOAD_LOCAL_STORAGE_START",
    LOAD_LOCAL_STORAGE_SUCCESS = "@@settings/server/storage/LOAD_LOCAL_STORAGE_SUCCESS",
    LOAD_LOCAL_STORAGE_FAILURE = "@@settings/server/storage/LOAD_LOCAL_STORAGE_FAILURE",

    SET_CURRENT_STORAGE_START = "@@settings/server/storage/SET_CURRENT_STORAGE_START",
    SET_CURRENT_STORAGE_SUCCESS = "@@settings/server/storage/SET_CURRENT_STORAGE_SUCCESS",
    SET_CURRENT_STORAGE_FAILURE = "@@settings/server/storage/SET_CURRENT_STORAGE_FAILURE",

    SET_GOOGLE_DRIVE_PARAMS_START = "@@settings/server/storage/SET_GOOGLE_DRIVE_PARAMS_START",
    SET_GOOGLE_DRIVE_PARAMS_SUCCESS = "@@settings/server/storage/SET_GOOGLE_DRIVE_PARAMS_SUCCESS",
    SET_GOOGLE_DRIVE_PARAMS_FAILURE = "@@settings/server/storage/SET_GOOGLE_DRIVE_PARAMS_FAILURE",

    SET_LOCAL_STORAGE_PARAMS_START = "@@settings/server/storage/SET_LOCAL_STORAGE_PARAMS_START",
    SET_LOCAL_STORAGE_PARAMS_SUCCESS = "@@settings/server/storage/SET_LOCAL_STORAGE_PARAMS_SUCCESS",
    SET_LOCAL_STORAGE_PARAMS_FAILURE = "@@settings/server/storage/SET_LOCAL_STORAGE_PARAMS_FAILURE",
}