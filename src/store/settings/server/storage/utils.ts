import {GoogleDriveParams, LocalStorageParams} from "./types";

export function getEmptyGoogleDriveParams(): GoogleDriveParams {
    return {
        serviceAccountId: '',
        secretFileLocation: '',
        storageFolderName: '',
        tempFolderName: '',
    }
}

export function getEmptyLocalStorageParams(): LocalStorageParams {
    return {
        imagesHostURL: '',
        storageFolderName: ''
    }
}