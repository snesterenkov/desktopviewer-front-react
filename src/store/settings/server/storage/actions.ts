import {action} from "typesafe-actions";
import {Dispatch} from "redux";
import {API_GOOGLE_DRIVE_URL, API_LOCAL_STORAGE_URL, API_SERVER_STORAGE_URL} from "../../../../constants";
import axios from "axios";
import {createConfig} from "../../../../utils";
import {GoogleDriveParams, LocalStorageParams, ServerStorageActionTypes, Storage} from "./types";

export const loadStoragesRequest = () => action(ServerStorageActionTypes.LOAD_STORAGES_START);
export const loadStoragesSuccess = (storages: Storage[]) =>
    action(ServerStorageActionTypes.LOAD_STORAGES_SUCCESS, {storages});
export const loadStoragesFailure = (message: string) =>
    action(ServerStorageActionTypes.LOAD_STORAGES_FAILURE, {message});

export const loadGoogleDriveParamsRequest = () => action(ServerStorageActionTypes.LOAD_GOOGLE_DRIVE_START);
export const loadGoogleDriveParamsSuccess = (googleDriveParams: GoogleDriveParams) =>
    action(ServerStorageActionTypes.LOAD_GOOGLE_DRIVE_SUCCESS, {googleDriveParams});
export const loadGoogleDriveParamsFailure = (message: string) =>
    action(ServerStorageActionTypes.LOAD_GOOGLE_DRIVE_FAILURE, {message});

export const loadLocalStorageParamsRequest = () => action(ServerStorageActionTypes.LOAD_LOCAL_STORAGE_START);
export const loadLocalStorageParamsSuccess = (localStorageParams: LocalStorageParams) =>
    action(ServerStorageActionTypes.LOAD_LOCAL_STORAGE_SUCCESS, {localStorageParams});
export const loadLocalStorageParamsFailure = (message: string) =>
    action(ServerStorageActionTypes.LOAD_LOCAL_STORAGE_FAILURE, {message});

export const setCurrentStorageRequest = () => action(ServerStorageActionTypes.SET_CURRENT_STORAGE_START);
export const setCurrentStorageSuccess = (storage: Storage) =>
    action(ServerStorageActionTypes.SET_CURRENT_STORAGE_SUCCESS, {storage});
export const setCurrentStorageFailure = (message: string) =>
    action(ServerStorageActionTypes.SET_CURRENT_STORAGE_FAILURE, {message});

export const setGoogleDriveParamsRequest = () => action(ServerStorageActionTypes.SET_GOOGLE_DRIVE_PARAMS_START);
export const setGoogleDriveParamsSuccess = (googleDriveParams: GoogleDriveParams) =>
    action(ServerStorageActionTypes.SET_GOOGLE_DRIVE_PARAMS_SUCCESS, {googleDriveParams});
export const setGoogleDriveParamsFailure = (message: string) =>
    action(ServerStorageActionTypes.SET_GOOGLE_DRIVE_PARAMS_FAILURE, {message});

export const setLocalStorageParamsRequest = () => action(ServerStorageActionTypes.SET_LOCAL_STORAGE_PARAMS_START);
export const setLocalStorageParamsSuccess = (localStorageParams: LocalStorageParams) =>
    action(ServerStorageActionTypes.SET_LOCAL_STORAGE_PARAMS_SUCCESS, {localStorageParams});
export const setLocalStorageParamsFailure = (message: string) =>
    action(ServerStorageActionTypes.SET_LOCAL_STORAGE_PARAMS_FAILURE, {message});

export const getStorages = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadStoragesRequest());
        return await axios.get(API_SERVER_STORAGE_URL, createConfig(API_SERVER_STORAGE_URL))
            .then(res => {
                dispatch(loadStoragesSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(loadStoragesFailure(errorMessage));
            });
    }
};

export const getGoogleDriveParams = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadGoogleDriveParamsRequest());
        return await axios.get(API_GOOGLE_DRIVE_URL, createConfig(API_GOOGLE_DRIVE_URL))
            .then(res => {
                dispatch(loadGoogleDriveParamsSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(loadGoogleDriveParamsFailure(errorMessage));
            });
    }
};

export const getLocalStorage = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadLocalStorageParamsRequest());
        return await axios.get(API_LOCAL_STORAGE_URL, createConfig(API_LOCAL_STORAGE_URL))
            .then(res => {
                dispatch(loadLocalStorageParamsSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(loadLocalStorageParamsFailure(errorMessage));
            });
    }
};

export const setCurrentStorage = (storage: Storage) => {
    return async (dispatch: Dispatch) => {
        dispatch(setCurrentStorageRequest());
        return await axios.post(API_SERVER_STORAGE_URL, storage, createConfig(API_SERVER_STORAGE_URL))
            .then(res => {
                dispatch(setCurrentStorageSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(setCurrentStorageFailure(errorMessage));
            });
    }
};

export const setGoogleDriveParams = (googleDriveParams: GoogleDriveParams) => {
    return async (dispatch: Dispatch) => {
        dispatch(setGoogleDriveParamsRequest());
        return await axios.post(API_GOOGLE_DRIVE_URL, googleDriveParams, createConfig(API_GOOGLE_DRIVE_URL))
            .then(() => {
                dispatch(setGoogleDriveParamsSuccess(googleDriveParams));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(setGoogleDriveParamsFailure(errorMessage));
            });
    }
};

export const setLocalStorageParams = (localStorageParams: LocalStorageParams) => {
    return async (dispatch: Dispatch) => {
        dispatch(setLocalStorageParamsRequest());
        return await axios.post(API_LOCAL_STORAGE_URL, localStorageParams, createConfig(API_LOCAL_STORAGE_URL))
            .then(() => {
                dispatch(setLocalStorageParamsSuccess(localStorageParams));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(setLocalStorageParamsFailure(errorMessage));
            });
    }
};