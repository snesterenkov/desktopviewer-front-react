import {ActionType} from "typesafe-actions";
import * as serverStorageActions from "../../server/storage/actions";
import {Reducer} from "redux";
import {ServerStorageActionTypes, ServerStorageState} from "./types";
import {getEmptyGoogleDriveParams, getEmptyLocalStorageParams} from "./utils";

export type ServerStorageAction = ActionType<typeof serverStorageActions>;

const initialState: ServerStorageState = {
    storages: [],
    googleDriveParams: getEmptyGoogleDriveParams(),
    localStorageParams: getEmptyLocalStorageParams(),
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<ServerStorageState, ServerStorageAction> = (state: ServerStorageState = initialState, action: ServerStorageAction) => {
    switch (action.type) {
        case ServerStorageActionTypes.LOAD_STORAGES_START:
        case ServerStorageActionTypes.LOAD_GOOGLE_DRIVE_START:
        case ServerStorageActionTypes.LOAD_LOCAL_STORAGE_START:
        case ServerStorageActionTypes.SET_CURRENT_STORAGE_START:
        case ServerStorageActionTypes.SET_GOOGLE_DRIVE_PARAMS_START:
        case ServerStorageActionTypes.SET_LOCAL_STORAGE_PARAMS_START:
            return {...state, isLoading: true, isLoaded: false};
        case ServerStorageActionTypes.LOAD_STORAGES_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, storages: action.payload.storages};
        case ServerStorageActionTypes.LOAD_GOOGLE_DRIVE_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, googleDriveParams: action.payload.googleDriveParams};
        case ServerStorageActionTypes.LOAD_LOCAL_STORAGE_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, localStorageParams: action.payload.localStorageParams};
        case ServerStorageActionTypes.SET_CURRENT_STORAGE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                storages: [...state.storages.filter(storage => {
                    storage.current = false;
                    return storage.type != action.payload.storage.type
                }), action.payload.storage]
            };
        case ServerStorageActionTypes.SET_GOOGLE_DRIVE_PARAMS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, googleDriveParams: action.payload.googleDriveParams};
        case ServerStorageActionTypes.SET_LOCAL_STORAGE_PARAMS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, localStorageParams: action.payload.localStorageParams};
        case ServerStorageActionTypes.LOAD_STORAGES_FAILURE:
        case ServerStorageActionTypes.LOAD_GOOGLE_DRIVE_FAILURE:
        case ServerStorageActionTypes.LOAD_LOCAL_STORAGE_FAILURE:
        case ServerStorageActionTypes.SET_CURRENT_STORAGE_FAILURE:
        case ServerStorageActionTypes.SET_GOOGLE_DRIVE_PARAMS_FAILURE:
        case ServerStorageActionTypes.SET_LOCAL_STORAGE_PARAMS_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }
};

export {reducer as serverStorageReducer}