import {ActionType} from "typesafe-actions";
import * as serverClientActions from "../../server/client/actions";
import {Reducer} from "redux";
import {getEmptyClientSettings} from "./utils";
import {ServerClientActionTypes, ServerClientState} from "./types";

export type ServerClientAction = ActionType<typeof serverClientActions>;

const initialState: ServerClientState = {
    clientSettings: getEmptyClientSettings(),
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<ServerClientState, ServerClientAction> = (state: ServerClientState = initialState, action: ServerClientAction) => {
    switch (action.type) {
        case ServerClientActionTypes.LOAD_CLIENT_SETTINGS_START:
        case ServerClientActionTypes.SET_CLIENT_SETTINGS_START:
            return {...state, isLoading: true, isLoaded: false};
        case ServerClientActionTypes.LOAD_CLIENT_SETTINGS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, clientSettings: action.payload.clientSettings};
        case ServerClientActionTypes.SET_CLIENT_SETTINGS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, clientSettings: action.payload.clientSettings};
        case ServerClientActionTypes.LOAD_CLIENT_SETTINGS_FAILURE:
        case ServerClientActionTypes.SET_CLIENT_SETTINGS_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }
};

export {reducer as serverClientReducer}