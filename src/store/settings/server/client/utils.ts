import {ClientSettings} from "./types";

export function getEmptyClientSettings(): ClientSettings {
    return {
        inviteConfirmURL: "",
        passwordRestoreURL: "",
        registrationConfirmURL: ""
    }
}