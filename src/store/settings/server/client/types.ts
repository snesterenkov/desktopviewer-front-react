export type ClientSettings = {
    inviteConfirmURL: string
    passwordRestoreURL: string
    registrationConfirmURL: string
}

export type ServerClientState = {
    clientSettings: ClientSettings,
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum ServerClientActionTypes {
    LOAD_CLIENT_SETTINGS_START = "@@settings/server/client/LOAD_CLIENT_SETTINGS_START",
    LOAD_CLIENT_SETTINGS_SUCCESS = "@@settings/server/client/LOAD_CLIENT_SETTINGS_SUCCESS",
    LOAD_CLIENT_SETTINGS_FAILURE = "@@settings/server/client/LOAD_CLIENT_SETTINGS_FAILURE",

    SET_CLIENT_SETTINGS_START = "@@settings/server/client/SET_CLIENT_SETTINGS_START",
    SET_CLIENT_SETTINGS_SUCCESS = "@@settings/server/client/SET_CLIENT_SETTINGS_SUCCESS",
    SET_CLIENT_SETTINGS_FAILURE = "@@settings/server/client/SET_CLIENT_SETTINGS_FAILURE"
}