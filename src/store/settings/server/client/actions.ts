import {action} from "typesafe-actions";
import {Dispatch} from "redux";
import {API_SERVER_CLIENT_URL} from "../../../../constants";
import axios from "axios";
import {createConfig} from "../../../../utils";
import {ClientSettings, ServerClientActionTypes} from "./types";

export const loadClientRequest = () => action(ServerClientActionTypes.LOAD_CLIENT_SETTINGS_START);
export const loadClientSuccess = (clientSettings: ClientSettings) =>
    action(ServerClientActionTypes.LOAD_CLIENT_SETTINGS_SUCCESS, {clientSettings});
export const loadClientFailure = (message: string) =>
    action(ServerClientActionTypes.LOAD_CLIENT_SETTINGS_FAILURE, {message});

export const setClientSettingsRequest = () => action(ServerClientActionTypes.SET_CLIENT_SETTINGS_START);
export const setClientSettingsSuccess = (clientSettings: ClientSettings) =>
    action(ServerClientActionTypes.SET_CLIENT_SETTINGS_SUCCESS, {clientSettings});
export const setClientSettingsFailure = (message: string) =>
    action(ServerClientActionTypes.SET_CLIENT_SETTINGS_FAILURE, {message});

export const getClientSettings = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadClientRequest());
        return await axios.get(API_SERVER_CLIENT_URL, createConfig(API_SERVER_CLIENT_URL))
            .then(res => {
                dispatch(loadClientSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(loadClientFailure(errorMessage));
            });
    }
};

export const setClientSettings = (clientSetting: ClientSettings) => {
    return async (dispatch: Dispatch) => {
        dispatch(setClientSettingsRequest());
        return await axios.put(API_SERVER_CLIENT_URL, clientSetting, createConfig(API_SERVER_CLIENT_URL))
            .then(() => {
                dispatch(setClientSettingsSuccess(clientSetting));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(setClientSettingsFailure(errorMessage));
            });
    }
};