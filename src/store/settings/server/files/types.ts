export type FilesSettings = {
    failMessage: string
    failed: boolean
    filesCount: number
    limitDate: string
    storage: string
}

export type ServerFilesState = {
    googleDriveFiles: FilesSettings,
    localStorageFiles: FilesSettings,
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum ServerFilesActionTypes {
    LOAD_GOOGLE_DRIVE_FILES_START = "@@settings/server/files/LOAD_GOOGLE_DRIVE_FILES_START",
    LOAD_GOOGLE_DRIVE_FILES_SUCCESS = "@@settings/server/files/LOAD_GOOGLE_DRIVE_FILES_SUCCESS",
    LOAD_GOOGLE_DRIVE_FILES_FAILURE = "@@settings/server/files/LOAD_GOOGLE_DRIVE_FILES_FAILURE",

    LOAD_LOCAL_STORAGE_FILES_START = "@@settings/server/files/LOAD_LOCAL_STORAGE_FILES_START",
    LOAD_LOCAL_STORAGE_FILES_SUCCESS = "@@settings/server/files/LOAD_LOCAL_STORAGE_FILES_SUCCESS",
    LOAD_LOCAL_STORAGE_FILES_FAILURE = "@@settings/server/files/LOAD_LOCAL_STORAGE_FILES_FAILURE",

    CLEAR_ALL_FILES_START = "@@settings/server/files/CLEAR_ALL_FILES_START",
    CLEAR_ALL_FILES_SUCCESS = "@@settings/server/files/CLEAR_ALL_FILES_SUCCESS",
    CLEAR_ALL_FILES_FAILURE = "@@settings/server/files/CLEAR_ALL_FILES_FAILURE",

    CLEAR_GOOGLE_DRIVE_FILES_START = "@@settings/server/files/CLEAR_GOOGLE_DRIVE_FILES_START",
    CLEAR_GOOGLE_DRIVE_FILES_SUCCESS = "@@settings/server/files/CLEAR_GOOGLE_DRIVE_FILES_SUCCESS",
    CLEAR_GOOGLE_DRIVE_FILES_FAILURE = "@@settings/server/files/CLEAR_GOOGLE_DRIVE_FILES_FAILURE",

    CLEAR_LOCAL_STORAGE_FILES_START = "@@settings/server/files/CLEAR_LOCAL_STORAGE_FILES_START",
    CLEAR_LOCAL_STORAGE_FILES_SUCCESS = "@@settings/server/files/CLEAR_LOCAL_STORAGE_FILES_SUCCESS",
    CLEAR_LOCAL_STORAGE_FILES_FAILURE = "@@settings/server/files/CLEAR_LOCAL_STORAGE_FILES_FAILURE",
}