import {ActionType} from "typesafe-actions";
import * as serverFilesActions from "../../server/files/actions";
import {Reducer} from "redux";
import {getEmptyFilesSettings} from "./utils";
import {ServerFilesActionTypes, ServerFilesState} from "./types";

export type ServerFilesAction = ActionType<typeof serverFilesActions>;

const initialState: ServerFilesState = {
    googleDriveFiles: getEmptyFilesSettings(),
    localStorageFiles: getEmptyFilesSettings(),
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<ServerFilesState, ServerFilesAction> = (state: ServerFilesState = initialState, action: ServerFilesAction) => {
    switch (action.type) {
        case ServerFilesActionTypes.LOAD_GOOGLE_DRIVE_FILES_START:
        case ServerFilesActionTypes.LOAD_LOCAL_STORAGE_FILES_START:
        case ServerFilesActionTypes.CLEAR_ALL_FILES_START:
        case ServerFilesActionTypes.CLEAR_GOOGLE_DRIVE_FILES_START:
        case ServerFilesActionTypes.CLEAR_LOCAL_STORAGE_FILES_START:
            return {...state, isLoading: true, isLoaded: false};

        case ServerFilesActionTypes.LOAD_GOOGLE_DRIVE_FILES_SUCCESS:
        case ServerFilesActionTypes.CLEAR_GOOGLE_DRIVE_FILES_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, googleDriveFiles: action.payload.googleDriveFiles};
        case ServerFilesActionTypes.LOAD_LOCAL_STORAGE_FILES_SUCCESS:
        case ServerFilesActionTypes.CLEAR_LOCAL_STORAGE_FILES_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, localStorageFiles: action.payload.localStorageFiles};
        case ServerFilesActionTypes.CLEAR_ALL_FILES_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                googleDriveFiles: action.payload.allFiles.find(file => file.storage == "GoogleDrive") ||  getEmptyFilesSettings(),
                localStorageFiles: action.payload.allFiles.find(file => file.storage == "LocalStorage") ||  getEmptyFilesSettings()
            };

        case ServerFilesActionTypes.LOAD_GOOGLE_DRIVE_FILES_FAILURE:
        case ServerFilesActionTypes.LOAD_LOCAL_STORAGE_FILES_FAILURE:
        case ServerFilesActionTypes.CLEAR_ALL_FILES_FAILURE:
        case ServerFilesActionTypes.CLEAR_GOOGLE_DRIVE_FILES_FAILURE:
        case ServerFilesActionTypes.CLEAR_LOCAL_STORAGE_FILES_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }
};

export {reducer as serverFilesReducer}