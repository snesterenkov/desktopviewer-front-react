import {action} from "typesafe-actions";
import {Dispatch} from "redux";
import {
    API_SERVER_FILES_URL,
    API_SERVER_GOOGLE_DRIVE_FILES_URL,
    API_SERVER_LOCAL_STORAGE_FILES_URL
} from "../../../../constants";
import axios from "axios";
import {createConfig} from "../../../../utils";
import {FilesSettings, ServerFilesActionTypes} from "./types";

export const loadGoogleDriveFilesRequest = () => action(ServerFilesActionTypes.LOAD_GOOGLE_DRIVE_FILES_START);
export const loadGoogleDriveFilesSuccess = (googleDriveFiles: FilesSettings) =>
    action(ServerFilesActionTypes.LOAD_GOOGLE_DRIVE_FILES_SUCCESS, {googleDriveFiles});
export const loadGoogleDriveFilesFailure = (message: string) =>
    action(ServerFilesActionTypes.LOAD_GOOGLE_DRIVE_FILES_FAILURE, {message});

export const loadLocalStorageFilesRequest = () => action(ServerFilesActionTypes.LOAD_LOCAL_STORAGE_FILES_START);
export const loadLocalStorageFilesSuccess = (localStorageFiles: FilesSettings) =>
    action(ServerFilesActionTypes.LOAD_LOCAL_STORAGE_FILES_SUCCESS, {localStorageFiles});
export const loadLocalStorageFilesFailure = (message: string) =>
    action(ServerFilesActionTypes.LOAD_LOCAL_STORAGE_FILES_FAILURE, {message});

export const clearAllFilesRequest = () => action(ServerFilesActionTypes.CLEAR_ALL_FILES_START);
export const clearAllFilesSuccess = (allFiles: FilesSettings[]) =>
    action(ServerFilesActionTypes.CLEAR_ALL_FILES_SUCCESS, {allFiles});
export const clearAllFilesFailure = (message: string) =>
    action(ServerFilesActionTypes.CLEAR_ALL_FILES_FAILURE, {message});

export const clearGoogleDriveFilesRequest = () => action(ServerFilesActionTypes.CLEAR_GOOGLE_DRIVE_FILES_START);
export const clearGoogleDriveFilesSuccess = (googleDriveFiles: FilesSettings) =>
    action(ServerFilesActionTypes.CLEAR_GOOGLE_DRIVE_FILES_SUCCESS, {googleDriveFiles});
export const clearGoogleDriveFilesFailure = (message: string) =>
    action(ServerFilesActionTypes.CLEAR_GOOGLE_DRIVE_FILES_FAILURE, {message});

export const clearLocalStorageFilesRequest = () => action(ServerFilesActionTypes.CLEAR_LOCAL_STORAGE_FILES_START);
export const clearLocalStorageFilesSuccess = (localStorageFiles: FilesSettings) =>
    action(ServerFilesActionTypes.CLEAR_LOCAL_STORAGE_FILES_SUCCESS, {localStorageFiles});
export const clearLocalStorageFilesFailure = (message: string) =>
    action(ServerFilesActionTypes.CLEAR_LOCAL_STORAGE_FILES_FAILURE, {message});

export const getGoogleDriveFiles = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadGoogleDriveFilesRequest());
        return await axios.get(API_SERVER_GOOGLE_DRIVE_FILES_URL, createConfig(API_SERVER_GOOGLE_DRIVE_FILES_URL))
            .then(res => {
                dispatch(loadGoogleDriveFilesSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(loadGoogleDriveFilesFailure(errorMessage));
            });
    }
};

export const getLocalStorageFiles = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadLocalStorageFilesRequest());
        return await axios.get(API_SERVER_LOCAL_STORAGE_FILES_URL, createConfig(API_SERVER_LOCAL_STORAGE_FILES_URL))
            .then(res => {
                dispatch(loadLocalStorageFilesSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(loadLocalStorageFilesFailure(errorMessage));
            });
    }
};

export const clearAllFiles = () => {
    return async (dispatch: Dispatch) => {
        dispatch(clearAllFilesRequest());
        return await axios.delete(API_SERVER_FILES_URL, createConfig(API_SERVER_FILES_URL))
            .then(res => {
                dispatch(clearAllFilesSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(clearAllFilesFailure(errorMessage));
            });
    }
};

export const clearGoogleDriveFiles = () => {
    return async (dispatch: Dispatch) => {
        dispatch(clearGoogleDriveFilesRequest());
        return await axios.delete(API_SERVER_GOOGLE_DRIVE_FILES_URL, createConfig(API_SERVER_GOOGLE_DRIVE_FILES_URL))
            .then(res => {
                dispatch(clearGoogleDriveFilesSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(clearGoogleDriveFilesFailure(errorMessage));
            });
    }
};

export const clearLocalStorageFiles = () => {
    return async (dispatch: Dispatch) => {
        dispatch(clearLocalStorageFilesRequest());
        return await axios.delete(API_SERVER_LOCAL_STORAGE_FILES_URL, createConfig(API_SERVER_LOCAL_STORAGE_FILES_URL))
            .then(res => {
                dispatch(clearLocalStorageFilesSuccess(res.data));
            })
            .catch(err => {
                let errorMessage = err.response ? err.response.data.message : err.message;
                console.error(errorMessage);
                dispatch(clearLocalStorageFilesFailure(errorMessage));
            });
    }
};
