import {FilesSettings} from "./types";

export function getEmptyFilesSettings(): FilesSettings {
    return {
        failMessage: "",
        failed: false,
        filesCount: 0,
        limitDate: "",
        storage: ""
    }
}