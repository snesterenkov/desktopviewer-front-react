import {Reducer} from "redux";

import * as roleSettingsActions from "./actions";
import {ActionType} from "typesafe-actions";
import {Role, RoleSettingsActionTypes, RoleSettingState} from "./types";

export type RoleSettingAction = ActionType<typeof roleSettingsActions>;

const initialState: RoleSettingState = {
    rolesForStructure: [],
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<RoleSettingState, RoleSettingAction> = (state: RoleSettingState = initialState, action: RoleSettingAction) => {
    switch (action.type) {
        case RoleSettingsActionTypes.LOAD_ROLES_FOR_STRUCTURE_START:
            return {...state, isLoading: true, isLoaded: false};
        case RoleSettingsActionTypes.LOAD_ROLES_FOR_STRUCTURE_SUCCESS:
            return {
                ...state, isLoading: false, isLoaded: true, rolesForStructure: action.payload.rolesForStructure
                    .sort((a: Role, b: Role) => {
                        if (a.name < b.name) return -1;
                        if (a.name > b.name) return 1;
                        return 0;
                    })
            };
        case RoleSettingsActionTypes.LOAD_ROLES_FOR_STRUCTURE_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }

};

export {reducer as roleSettingsReducer}