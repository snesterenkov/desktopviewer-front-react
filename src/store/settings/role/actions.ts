import {action} from "typesafe-actions";
import {Role, RoleSettingsActionTypes} from "./types";
import {API_ROLE_SETTINGS_URL} from "../../../constants";
import {createConfig} from "../../../utils";
import {Dispatch} from "redux";
import axios from "axios";

export const loadRolesForStructureRequest = () => action(RoleSettingsActionTypes.LOAD_ROLES_FOR_STRUCTURE_START);
export const loadRolesForStructureSuccess = (roles: Array<Role>) =>
    action(RoleSettingsActionTypes.LOAD_ROLES_FOR_STRUCTURE_SUCCESS, {rolesForStructure: roles});
export const loadRolesForStructureFailure = (message: string) =>
    action(RoleSettingsActionTypes.LOAD_ROLES_FOR_STRUCTURE_FAILURE, {message: message});

export const getRolesForStructureType = (structureId: number) => {
    return async (dispatch: Dispatch) => {
        const url: string = `${API_ROLE_SETTINGS_URL}/rolesForStructureType/${structureId}`;
        dispatch(loadRolesForStructureRequest());
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadRolesForStructureSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                const message = (err.response && err.response.data && err.response.data.message) ? err.response.data.message : err.message;
                dispatch(loadRolesForStructureFailure(message));
            });
    }
};

