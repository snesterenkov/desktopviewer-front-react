import {Company} from "../company/types";
import {Department} from "../department/types";
import {User} from "../../../types";
import {Project} from "../project/types";
import {Permission} from "../permission/types";

type StructureType = {
    id: number,
    type: string
}

export type Role = {
    id: number,
    name: string,
    code: any, // todo mb only null?
    structureType: StructureType,
    permissions: Array<Permission>,
}

export type UserRole = {
    id?: number,
    company?: Company,
    department?: Department,
    project?: Project,
    role: Role,
    user: User,
}

export type RoleSettingState = {
    rolesForStructure: Array<Role> | [],
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum RoleSettingsActionTypes {
    LOAD_ROLES_FOR_STRUCTURE_START = "@@settings/role/LOAD_ROLES_FOR_STRUCTURE_START",
    LOAD_ROLES_FOR_STRUCTURE_SUCCESS = "@@settings/role/LOAD_ROLES_FOR_STRUCTURE_SUCCESS",
    LOAD_ROLES_FOR_STRUCTURE_FAILURE = "@@settings/role/LOAD_ROLES_FOR_STRUCTURE_FAILURE",
}