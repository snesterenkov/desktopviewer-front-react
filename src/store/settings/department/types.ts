import {Company} from "../company/types";
import {Status, User} from "../../../types";
import {UserRole} from "../role/types";

export type Department = {
    id?: number,
    name: string,
    companyId: number,
    status: Status,
    parentStatus: Status,
    company: Company
}

type DepartmentDetail = {
    userRoles: Array<UserRole>,
    admin: boolean,
    owner: User
}

export type DepartmentWithDetails = Department & DepartmentDetail;

export type DepartmentSettingState = {
    departments: Array<DepartmentWithDetails> | [],
    openDepartments?: Array<Department> | [],
    department: DepartmentWithDetails,
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum DepartmentSettingsActionTypes {
    LOAD_DEPARTMENTS_START = "@@settings/department/LOAD_DEPARTMENTS_START",
    LOAD_DEPARTMENTS_SUCCESS = "@@settings/department/LOAD_DEPARTMENTS_SUCCESS",
    LOAD_DEPARTMENTS_FAILURE = "@@settings/department/LOAD_DEPARTMENTS_FAILURE",

    LOAD_DEPARTMENT_START = "@@settings/department/LOAD_DEPARTMENT_START",
    LOAD_DEPARTMENT_SUCCESS = "@@settings/department/LOAD_DEPARTMENT_SUCCESS",
    LOAD_DEPARTMENT_FAILURE = "@@settings/department/LOAD_DEPARTMENT_FAILURE",

    LOAD_OPEN_DEPARTMENTS_START = "@@settings/department/LOAD_OPEN_DEPARTMENTS_START",
    LOAD_OPEN_DEPARTMENTS_SUCCESS = "@@settings/department/LOAD_OPEN_DEPARTMENTS_SUCCESS",
    LOAD_OPEN_DEPARTMENTS_FAILURE = "@@settings/department/LOAD_OPEN_DEPARTMENTS_FAILURE",

    ADD_DEPARTMENT_START = "@@settings/department/ADD_DEPARTMENT_START",
    ADD_DEPARTMENT_SUCCESS = "@@settings/department/ADD_DEPARTMENT_SUCCESS",
    ADD_DEPARTMENT_FAILURE = "@@settings/department/ADD_DEPARTMENT_FAILURE",

    UPDATE_DEPARTMENT_START = "@@settings/department/UPDATE_DEPARTMENT_START",
    UPDATE_DEPARTMENT_SUCCESS = "@@settings/department/UPDATE_DEPARTMENT_SUCCESS",
    UPDATE_DEPARTMENT_FAILURE = "@@settings/department/UPDATE_DEPARTMENT_FAILURE",

    UPDATE_DEPARTMENT_DETAILS_START = "@@settings/department/UPDATE_DEPARTMENT_DETAILS_START",
    UPDATE_DEPARTMENT_DETAILS_SUCCESS = "@@settings/department/UPDATE_DEPARTMENT_DETAILS_SUCCESS",
    UPDATE_DEPARTMENT_DETAILS_FAILURE = "@@settings/department/UPDATE_DEPARTMENT_DETAILS_FAILURE",

    DELETE_DEPARTMENT_START = "@@settings/department/DELETE_DEPARTMENT_START",
    DELETE_DEPARTMENT_SUCCESS = "@@settings/department/DELETE_DEPARTMENT_SUCCESS",
    DELETE_DEPARTMENT_FAILURE = "@@settings/department/DELETE_DEPARTMENT_FAILURE",

    CHANGE_DEPARTMENT_STATUS_START = "@@settings/department/CHANGE_DEPARTMENT_STATUS_START",
    CHANGE_DEPARTMENT_STATUS_SUCCESS = "@@settings/department/CHANGE_DEPARTMENT_STATUS_SUCCESS",
    CHANGE_DEPARTMENT_STATUS_FAILURE = "@@settings/department/CHANGE_DEPARTMENT_STATUS_FAILURE",

    DELETE_USER_DEPARTMENT_ROLE_START = "@@settings/department/DELETE_USER_DEPARTMENT_ROLE_START",
    DELETE_USER_DEPARTMENT_ROLE_SUCCESS = "@@settings/department/DELETE_USER_DEPARTMENT_ROLE_SUCCESS",
    DELETE_USER_DEPARTMENT_ROLE_FAILURE = "@@settings/department/DELETE_USER_DEPARTMENT_ROLE_FAILURE",
}