import {action} from "typesafe-actions";
import {Department, DepartmentSettingsActionTypes, DepartmentWithDetails} from "./types";
import {API_DEPARTMENT_SETTINGS_URL} from "../../../constants";
import axios, {AxiosRequestConfig} from "axios";
import {Dispatch} from "redux";
import {createConfig} from "../../../utils";

export const loadDepartmentsRequest = () => action(DepartmentSettingsActionTypes.LOAD_DEPARTMENTS_START);
export const loadDepartmentsSuccess = (departments: Array<DepartmentWithDetails>) => action(DepartmentSettingsActionTypes.LOAD_DEPARTMENTS_SUCCESS, {departments});
export const loadDepartmentsFailure = (message: string) => action(DepartmentSettingsActionTypes.LOAD_DEPARTMENTS_FAILURE, {message});

export const loadDepartmentRequest = () => action(DepartmentSettingsActionTypes.LOAD_DEPARTMENT_START);
export const loadDepartmentSuccess = (department: DepartmentWithDetails) => action(DepartmentSettingsActionTypes.LOAD_DEPARTMENT_SUCCESS, {department});
export const loadDepartmentFailure = (message: string) => action(DepartmentSettingsActionTypes.LOAD_DEPARTMENT_FAILURE, {message});

export const loadOpenDepartmentsRequest = () => action(DepartmentSettingsActionTypes.LOAD_OPEN_DEPARTMENTS_START);
export const loadOpenDepartmentsSuccess = (openDepartments: Array<Department>) => action(DepartmentSettingsActionTypes.LOAD_OPEN_DEPARTMENTS_SUCCESS, {openDepartments});
export const loadOpenDepartmentsFailure = (message: string) => action(DepartmentSettingsActionTypes.LOAD_OPEN_DEPARTMENTS_FAILURE, {message});

export const addDepartmentRequest = () => action(DepartmentSettingsActionTypes.ADD_DEPARTMENT_START);
export const addDepartmentSuccess = (department: DepartmentWithDetails) => action(DepartmentSettingsActionTypes.ADD_DEPARTMENT_SUCCESS, {lastAddedDepartment: department});
export const addDepartmentFailure = (message: string) => action(DepartmentSettingsActionTypes.ADD_DEPARTMENT_FAILURE, {message});

export const updateDepartmentRequest = () => action(DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_START);
export const updateDepartmentSuccess = (updatedDepartment: DepartmentWithDetails) => action(DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_SUCCESS, {updatedDepartment});
export const updateDepartmentFailure = (message: string) => action(DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_FAILURE, {message});

export const updateDepartmentDetailsRequest = () => action(DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_DETAILS_START);
export const updateDepartmentDetailsSuccess = (updatedDepartment: DepartmentWithDetails) => action(DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_DETAILS_SUCCESS, {updatedDepartment});
export const updateDepartmentDetailsFailure = (message: string) => action(DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_DETAILS_FAILURE, {message});

export const deleteDepartmentRequest = () => action(DepartmentSettingsActionTypes.DELETE_DEPARTMENT_START);
export const deleteDepartmentSuccess = (departmentId: number) => action(DepartmentSettingsActionTypes.DELETE_DEPARTMENT_SUCCESS, {departmentId});
export const deleteDepartmentFailure = (message: string) => action(DepartmentSettingsActionTypes.DELETE_DEPARTMENT_FAILURE, {message});

export const changeDepartmentStatusRequest = () => action(DepartmentSettingsActionTypes.CHANGE_DEPARTMENT_STATUS_START);
export const changeDepartmentStatusSuccess = (updatedDepartment: DepartmentWithDetails) => action(DepartmentSettingsActionTypes.CHANGE_DEPARTMENT_STATUS_SUCCESS, {updatedDepartment});
export const changeDepartmentStatusFailure = (message: string) => action(DepartmentSettingsActionTypes.CHANGE_DEPARTMENT_STATUS_FAILURE, {message});

export const deleteUserDepartmentRoleRequest = () => action(DepartmentSettingsActionTypes.DELETE_USER_DEPARTMENT_ROLE_START);
export const deleteUserDepartmentRoleSuccess = (userRoleId: number) => action(DepartmentSettingsActionTypes.DELETE_USER_DEPARTMENT_ROLE_SUCCESS, {userRoleId});
export const deleteUserDepartmentRoleFailure = (message: string) => action(DepartmentSettingsActionTypes.DELETE_USER_DEPARTMENT_ROLE_FAILURE, {message});


export const getDepartments = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadDepartmentsRequest());
        return await axios.get(API_DEPARTMENT_SETTINGS_URL, createConfig(API_DEPARTMENT_SETTINGS_URL))
            .then(res => {
                dispatch(loadDepartmentsSuccess(res.data));
               // alert("список подразделений загружен");
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadDepartmentsFailure(err.message));
              //  alert("ошибка при загрузке списка подразделений");
            });
    }
};

export const getDepartment = (departmentId: number) => {
    return async (dispatch: Dispatch) => {
        dispatch(loadDepartmentRequest());
        let url = API_DEPARTMENT_SETTINGS_URL + "/" + departmentId;
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadDepartmentSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadDepartmentFailure(err.message));
             //   alert("ошибка при загрузке отделения");
            });
    }
};

export const getOpenDepartments = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadOpenDepartmentsRequest());
        let url = API_DEPARTMENT_SETTINGS_URL + "/open";
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadOpenDepartmentsSuccess(res.data));
              //  alert("список открытых отделов загружен");
            })
            .catch(err => {
                console.error(err.message);
              //  alert("ошибка при загрузке списка открытых отделов");
                dispatch(loadOpenDepartmentsFailure(err.message));
            });
    }
};


export const addDepartment = (department: Department) => {
    return async (dispatch: Dispatch) => {
        dispatch(addDepartmentRequest());
        return await axios.post(API_DEPARTMENT_SETTINGS_URL, department, createConfig(API_DEPARTMENT_SETTINGS_URL))
            .then(res => {
                dispatch(addDepartmentSuccess(res.data));
           //     alert("отдел добален");
            })
            .catch(err => {
                dispatch(addDepartmentFailure(err.message));
                console.error(err.message);
           //     alert("ошибка при добавлении отдела");
            });
    }
};

export const updateDepartment = (department: Department) => {
    return async (dispatch: Dispatch) => {
        let url = API_DEPARTMENT_SETTINGS_URL + "/" + department.id;
        dispatch(updateDepartmentRequest());
        return await axios.put(url, department, createConfig(url))
            .then(res => {
                dispatch(updateDepartmentSuccess(res.data));
            //    alert("отдел обновлен");
            })
            .catch(err => {
                dispatch(updateDepartmentFailure(err.message));
                console.error(err.message);
           //     alert("ошибка при обновлении отдела");
            });
    }
};


export const updateDetailsDepartment = (department: DepartmentWithDetails) => {
    return async (dispatch: Dispatch) => {
        let url = API_DEPARTMENT_SETTINGS_URL + "/detailupdate/" + department.id;
        dispatch(updateDepartmentDetailsRequest());
        return await axios.put(url, department, createConfig(url))
            .then(res => {
                dispatch(updateDepartmentDetailsSuccess(res.data));
               // alert("детали отдела обновлены");
            })
            .catch(err => {
                dispatch(updateDepartmentDetailsFailure(err.message));
                console.error(err.message);
              //  alert("ошибка при обновлении деталей отдела");
            });
    }
};

export const deleteDepartment = (departmentId: number) => {
    return async (dispatch: Dispatch) => {
        let url = API_DEPARTMENT_SETTINGS_URL + "/" + departmentId;
        dispatch(deleteDepartmentRequest());
        return await axios.delete(url, createConfig(url))
            .then(() => {
                dispatch(deleteDepartmentSuccess(departmentId));
            //    alert("отдел удален");
            })
            .catch(err => {
                console.error(err.message);
            //    alert("ошибка при удалении отдела");
                dispatch(deleteDepartmentFailure(err.message));
            });
    }
};

export const changeDepartmentStatus = (departmentId: number, status: string) => {
    return async (dispatch: Dispatch) => {
        let url = API_DEPARTMENT_SETTINGS_URL + "/changestatus/" + departmentId;
        let config: AxiosRequestConfig = createConfig(url);
        config.headers = {
            'Content-Type': 'application/json;charset=UTF-8'
        };
        dispatch(changeDepartmentStatusRequest());
        return await axios.put(url, JSON.stringify(status), config)
            .then((res) => {
                dispatch(changeDepartmentStatusSuccess(res.data));
             //   alert("статус подразделения изменен");
            })
            .catch(err => {
                dispatch(changeDepartmentStatusFailure(err.message));
                console.error(err.message);
               // alert("ошибка при изменении статуса подразделения");
            });
    }
};

export const deleteUserDepartmentRole = (userRoleId: number) => {
    return async (dispatch: Dispatch) => {
        let url = API_DEPARTMENT_SETTINGS_URL + "/removeUserDepartmentRole/" + userRoleId;
        dispatch((deleteUserDepartmentRoleRequest()));
        return await axios.delete(url, createConfig(url))
            .then(() => {
                dispatch(deleteUserDepartmentRoleSuccess(userRoleId));
                //alert("роль пользователя в отделе удалена");
            })
            .catch(err => {
                dispatch(deleteUserDepartmentRoleFailure(err.message));
                console.error(err.message);
               // alert("ошибка при удалении пользователя");
            });
    }
};
