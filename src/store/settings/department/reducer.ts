import {Reducer} from "redux";

import * as departmentSettingsActions from "./actions";
import {ActionType} from "typesafe-actions";
import {DepartmentSettingsActionTypes, DepartmentSettingState, DepartmentWithDetails} from "./types";
import {UserRole} from "../role/types";
import {getEmptyDepartment} from "./utils";
import {getEmptyUser} from "../../../utils";

export type DepartmentSettingAction = ActionType<typeof departmentSettingsActions>;

const initialState: DepartmentSettingState = {
    departments: [],
    openDepartments: [],
    department: {...getEmptyDepartment(), admin: false, userRoles: [], owner: getEmptyUser()},
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<DepartmentSettingState, DepartmentSettingAction> = (state: DepartmentSettingState = initialState, action: DepartmentSettingAction) => {
    switch (action.type) {
        case DepartmentSettingsActionTypes.LOAD_DEPARTMENTS_START:
        case DepartmentSettingsActionTypes.LOAD_DEPARTMENT_START:
        case DepartmentSettingsActionTypes.LOAD_OPEN_DEPARTMENTS_START:
        case DepartmentSettingsActionTypes.ADD_DEPARTMENT_START:
        case DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_START:
        case DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_DETAILS_START:
        case DepartmentSettingsActionTypes.DELETE_DEPARTMENT_START:
        case DepartmentSettingsActionTypes.CHANGE_DEPARTMENT_STATUS_START:
        case DepartmentSettingsActionTypes.DELETE_USER_DEPARTMENT_ROLE_START:
            return {...state, isLoading: true, isLoaded: false};
        case DepartmentSettingsActionTypes.LOAD_DEPARTMENTS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, departments: action.payload.departments};
        case DepartmentSettingsActionTypes.LOAD_DEPARTMENT_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, department: action.payload.department};
        case DepartmentSettingsActionTypes.LOAD_OPEN_DEPARTMENTS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, openDepartments: action.payload.openDepartments};
        case DepartmentSettingsActionTypes.ADD_DEPARTMENT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                departments: [...state.departments, action.payload.lastAddedDepartment!],
                lastAddedDepartment: action.payload.lastAddedDepartment!
            };
        case DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_DETAILS_SUCCESS:
            return{
                ...state,
                isLoading: false,
                isLoaded: true,
                department: action.payload.updatedDepartment
            };
        case DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_SUCCESS:
        case DepartmentSettingsActionTypes.CHANGE_DEPARTMENT_STATUS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                departments: (state.departments as Array<DepartmentWithDetails>).map((department: DepartmentWithDetails) =>
                    department.id == action.payload.updatedDepartment.id ? action.payload.updatedDepartment : department),
                lastUpdatedDepartment: action.payload.updatedDepartment
            };
        case DepartmentSettingsActionTypes.DELETE_DEPARTMENT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                departments: (state.departments as Array<DepartmentWithDetails>).filter((item: DepartmentWithDetails) => item.id != action.payload.departmentId)
            };
        case DepartmentSettingsActionTypes.DELETE_USER_DEPARTMENT_ROLE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                department: {
                    ...state.department,
                    userRoles: (state.department.userRoles as Array<UserRole>).filter(userRole =>
                        userRole.id != action.payload.userRoleId),
                }
            };

        case DepartmentSettingsActionTypes.LOAD_DEPARTMENTS_FAILURE:
        case DepartmentSettingsActionTypes.LOAD_DEPARTMENT_FAILURE:
        case DepartmentSettingsActionTypes.LOAD_OPEN_DEPARTMENTS_FAILURE:
        case DepartmentSettingsActionTypes.ADD_DEPARTMENT_FAILURE:
        case DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_FAILURE:
        case DepartmentSettingsActionTypes.UPDATE_DEPARTMENT_DETAILS_FAILURE:
        case DepartmentSettingsActionTypes.DELETE_DEPARTMENT_FAILURE:
        case DepartmentSettingsActionTypes.CHANGE_DEPARTMENT_STATUS_FAILURE:
        case DepartmentSettingsActionTypes.DELETE_USER_DEPARTMENT_ROLE_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }

};

export {reducer as departmentSettingsReducer}