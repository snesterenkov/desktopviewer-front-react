import {Department, DepartmentWithDetails} from "./types";
import {getEmptyCompany} from "../company/utils";
import {Status} from "../../../types";

export function departmentFromDetailDepartment(detailDepartment: DepartmentWithDetails): Department {
    return {
        id: detailDepartment.id,
        name: detailDepartment.name,
        status: detailDepartment.status,
        companyId: detailDepartment.companyId,
        parentStatus: detailDepartment.parentStatus,
        company: detailDepartment.company
    }
}

export function getEmptyDepartment(): Department {
    return {
        name: "",
        companyId: 0,
        status: Status.OPEN,
        parentStatus: Status.OPEN,
        company: getEmptyCompany()
    }
}