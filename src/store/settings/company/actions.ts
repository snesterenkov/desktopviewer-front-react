import {Company, CompanySettingsActionTypes, CompanyWithDetails} from "./types";
import {action} from "typesafe-actions";
import {API_COMPANY_SETTINGS_URL} from "../../../constants";
import {Dispatch} from "redux";
import axios, {AxiosRequestConfig} from "axios";
import {createConfig} from "../../../utils";

export const loadCompaniesRequest = () => action(CompanySettingsActionTypes.LOAD_COMPANIES_START);
export const loadCompaniesSuccess = (companies: Array<CompanyWithDetails>) => action(CompanySettingsActionTypes.LOAD_COMPANIES_SUCCESS, {companies});
export const loadCompaniesFailure = (message: string) => action(CompanySettingsActionTypes.LOAD_COMPANIES_FAILURE, {message});

export const loadOpenCompaniesRequest = () => action(CompanySettingsActionTypes.LOAD_OPEN_COMPANIES_START);
export const loadOpenCompaniesSuccess = (openCompanies: Array<Company>) => action(CompanySettingsActionTypes.LOAD_OPEN_COMPANIES_SUCCESS, {openCompanies});
export const loadOpenCompaniesFailure = (message: string) => action(CompanySettingsActionTypes.LOAD_OPEN_COMPANIES_FAILURE, {message});

export const loadCompanyByEmployeeRequest = () => action(CompanySettingsActionTypes.LOAD_COMPANY_BY_EMPLOYEE_START);
export const loadCompanyByEmployeeSuccess = (company: Array<Company>) => action(CompanySettingsActionTypes.LOAD_COMPANY_BY_EMPLOYEE_SUCCESS, {company});
export const loadCompanyByEmployeeFailure = (message: string) => action(CompanySettingsActionTypes.LOAD_COMPANY_BY_EMPLOYEE_FAILURE, {message});

export const loadCompanyRequest = () => action(CompanySettingsActionTypes.LOAD_COMPANY_START);
export const loadCompanySuccess = (company: CompanyWithDetails) =>
    action(CompanySettingsActionTypes.LOAD_COMPANY_SUCCESS, {company});
export const loadCompanyFailure = (message: string) =>
    action(CompanySettingsActionTypes.LOAD_COMPANY_FAILURE, {message});

export const addCompanyRequest = () => action(CompanySettingsActionTypes.ADD_COMPANY_START);
export const addCompanySuccess = (company: CompanyWithDetails) => action(CompanySettingsActionTypes.ADD_COMPANY_SUCCESS, {lastAddedCompany: company});//todo delete last item?
export const addCompanyFailure = (message: string) => action(CompanySettingsActionTypes.ADD_COMPANY_FAILURE, {message});

export const updateCompanyRequest = () => action(CompanySettingsActionTypes.UPDATE_COMPANY_START);
export const updateCompanySuccess = (updatedCompany: CompanyWithDetails) => action(CompanySettingsActionTypes.UPDATE_COMPANY_SUCCESS, {updatedCompany});
export const updateCompanyFailure = (message: string) => action(CompanySettingsActionTypes.UPDATE_COMPANY_FAILURE, {message});

export const deleteCompanyRequest = () => action(CompanySettingsActionTypes.DELETE_COMPANY_START);
export const deleteCompanySuccess = (companyId: number) => action(CompanySettingsActionTypes.DELETE_COMPANY_SUCCESS, {companyId});//todo получать удаленную компанию назад?(востановление)
export const deleteCompanyFailure = (message: string) => action(CompanySettingsActionTypes.DELETE_COMPANY_FAILURE, {message});

export const updateCompanyDetailsRequest = () => action(CompanySettingsActionTypes.UPDATE_COMPANY_DETAILS_START);
export const updateCompanyDetailsSuccess = (company: CompanyWithDetails) => action(CompanySettingsActionTypes.UPDATE_COMPANY_DETAILS_SUCCESS, {lastUpdatedCompanyWithDetails: company});//todo delete last item?
export const updateCompanyDetailsFailure = (message: string) => action(CompanySettingsActionTypes.UPDATE_COMPANY_DETAILS_FAILURE, {message});

export const changeCompanyStatusRequest = () => action(CompanySettingsActionTypes.CHANGE_COMPANY_STATUS_START);
export const changeCompanyStatusSuccess = (updatedCompany: CompanyWithDetails) => action(CompanySettingsActionTypes.CHANGE_COMPANY_STATUS_SUCCESS, {updatedCompany});
export const changeCompanyStatusFailure = (message: string) => action(CompanySettingsActionTypes.CHANGE_COMPANY_STATUS_FAILURE, {message});

export const deleteUserCompanyRoleRequest = () => action(CompanySettingsActionTypes.DELETE_USER_COMPANY_ROLE_START);
export const deleteUserCompanyRoleSuccess = (userRoleId: number) => action(CompanySettingsActionTypes.DELETE_USER_COMPANY_ROLE_SUCCESS, {userRoleId});
export const deleteUserCompanyRoleFailure = (message: string) => action(CompanySettingsActionTypes.DELETE_USER_COMPANY_ROLE_FAILURE, {message});

export const getCompanies = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadCompaniesRequest());
        return await axios.get(API_COMPANY_SETTINGS_URL, createConfig(API_COMPANY_SETTINGS_URL))
            .then(res => {
                dispatch(loadCompaniesSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                //alert("ошибка при загрузке списка компаний");
                dispatch(loadCompaniesFailure(err.message));
            });
    }
};

export const getCompany = (companyId: number) => {
    return async (dispatch: Dispatch) => {
        dispatch(loadCompanyRequest());
        let url = API_COMPANY_SETTINGS_URL + "/" + companyId;
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadCompanySuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
             //   alert("ошибка при загрузке компании");
                dispatch(loadCompanyFailure(err.message));
            });
    }
};

export const getOpenCompanies = () =>{
    return async (dispatch: Dispatch) => {
        dispatch(loadOpenCompaniesRequest());
        let url = API_COMPANY_SETTINGS_URL + "/open";
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadOpenCompaniesSuccess(res.data));
                //alert("список открытых компаний загружен");
            })
            .catch(err => {
                console.error(err.message);
               // alert("ошибка при загрузке списка открытых компаний");
                dispatch(loadOpenCompaniesFailure(err.message));
            });
    }
};

export const getCompaniesByEmployee = (employeeId: number) => {
    return async (dispatch: Dispatch) => {
        dispatch(loadCompanyByEmployeeRequest());
        let url = API_COMPANY_SETTINGS_URL + "/employee/" + employeeId;
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadCompanyByEmployeeSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadCompanyByEmployeeFailure(err.message));
            });
    }
};

export const addCompany = (company: Company) => {
    return async (dispatch: Dispatch) => {
        dispatch(addCompanyRequest());
        return await axios.post(API_COMPANY_SETTINGS_URL, company, createConfig(API_COMPANY_SETTINGS_URL))
            .then(res => {
                dispatch(addCompanySuccess(res.data));
             //   alert("компания добалена");
            })
            .catch(err => {
                console.error(err.message);
              //  alert("ошибка при добавлении компании");
                dispatch(addCompanyFailure(err.message));
            });
    }
};

export const updateCompany = (company: Company) => {
    return async (dispatch: Dispatch) => {
        let url = API_COMPANY_SETTINGS_URL + "/" + company.id;
        dispatch(updateCompanyRequest());
        return await axios.put(url, company, createConfig(url))
            .then(res => {
                dispatch(updateCompanySuccess(res.data));
               // alert("компания обновлена");
            })
            .catch(err => {
                dispatch(updateCompanyFailure(err.message));
                console.error(err.message);
              //  alert("ошибка при обновлении компании");
            });
    }
};

export const deleteCompany = (companyId: number) => {
    return async (dispatch: Dispatch) => {
        let url = API_COMPANY_SETTINGS_URL + "/" + companyId;
        dispatch(deleteCompanyRequest());
        return await axios.delete(url, createConfig(url))
            .then(() => {
                dispatch(deleteCompanySuccess(companyId));
               // alert("компания удалена");
            })
            .catch(err => {
                console.error(err.message);
            //    alert("ошибка при удалении компании");
                dispatch(deleteCompanyFailure(err.message));
            });
    }
};

export const updateDetailsCompany = (company: CompanyWithDetails) => {
    return async (dispatch: Dispatch) => {
        let url = API_COMPANY_SETTINGS_URL + "/detailupdate/" + company.id;
        dispatch(updateCompanyDetailsRequest());
        return await axios.put(url, company, createConfig(url))
            .then(res => {
                dispatch(updateCompanyDetailsSuccess(res.data));
               // alert("детали компании обновлены");
            })
            .catch(err => {
                dispatch(updateCompanyDetailsFailure(err.message));
                console.error(err.message);
            //    alert("ошибка при обновлении деталей компании");
            });
    }
};

export const changeCompanyStatus = (companyId: number, status: string) => {
    return async (dispatch: Dispatch) => {
        let url = API_COMPANY_SETTINGS_URL + "/changestatus/" + companyId;

        let config: AxiosRequestConfig = createConfig(url);
        config.headers = {
            'Content-Type': 'application/json;charset=UTF-8'
        };
        dispatch(changeCompanyStatusRequest());
        return await axios.put(url, JSON.stringify(status), config)
            .then((res) => {
                dispatch(changeCompanyStatusSuccess(res.data));
               // alert("статус компании изменен");
            })
            .catch(err => {
                dispatch(changeCompanyStatusFailure(err.message));
                console.error(err.message);
             //   alert("ошибка при изменении статуса компании");
            });
    }
};

export const deleteUserCompanyRole = (userRoleId: number) => {
    return async (dispatch: Dispatch) => {
        let url = API_COMPANY_SETTINGS_URL + "/removeUserCompanyRoles/" + userRoleId;
        dispatch((deleteUserCompanyRoleRequest()));
        return await axios.delete(url, createConfig(url))
            .then(() => {
                dispatch(deleteUserCompanyRoleSuccess(userRoleId));
              //  alert("роль пользователя в компании удалена");
            })
            .catch(err => {
                dispatch(deleteUserCompanyRoleFailure(err.message));
                console.error(err.message);
            //    alert("ошибка при удалении пользователя");
            });
    }
};

