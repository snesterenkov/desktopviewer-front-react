import {Reducer} from "redux";
import {CompanySettingsActionTypes, CompanySettingState, CompanyWithDetails} from "./types";

import * as companySettingsActions from "./actions";
import {ActionType} from "typesafe-actions";
import {UserRole} from "../role/types";
import {getEmptyCompany} from "./utils";
import {getEmptyUser} from "../../../utils";

export type CompanySettingAction = ActionType<typeof companySettingsActions>;

const initialState: CompanySettingState = {
    employeeCompanies: [],
    companies: [],
    company: {...getEmptyCompany(), userRoles: [], admin: false, owner: getEmptyUser()},
    openCompanies: [],
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<CompanySettingState, CompanySettingAction> = (state: CompanySettingState = initialState, action: CompanySettingAction) => {
    switch (action.type) {//todo мб сохранять что-то еще в state
        case CompanySettingsActionTypes.LOAD_COMPANIES_START:
        case CompanySettingsActionTypes.LOAD_COMPANY_START:
        case CompanySettingsActionTypes.LOAD_OPEN_COMPANIES_START:
        case CompanySettingsActionTypes.LOAD_COMPANY_BY_EMPLOYEE_START:
        case CompanySettingsActionTypes.ADD_COMPANY_START:
        case CompanySettingsActionTypes.UPDATE_COMPANY_START:
        case CompanySettingsActionTypes.UPDATE_COMPANY_DETAILS_START:
        case CompanySettingsActionTypes.DELETE_COMPANY_START:
        case CompanySettingsActionTypes.CHANGE_COMPANY_STATUS_START:
        case CompanySettingsActionTypes.DELETE_USER_COMPANY_ROLE_START:
            return {...state, isLoading: true, isLoaded: false};
        case CompanySettingsActionTypes.LOAD_COMPANIES_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, companies: action.payload.companies};
        case CompanySettingsActionTypes.LOAD_COMPANY_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, company: action.payload.company};
        case CompanySettingsActionTypes.LOAD_OPEN_COMPANIES_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, openCompanies: action.payload.openCompanies};
        case CompanySettingsActionTypes.LOAD_COMPANY_BY_EMPLOYEE_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, employeeCompanies: action.payload.company};
        case CompanySettingsActionTypes.ADD_COMPANY_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                companies: [...state.companies, action.payload.lastAddedCompany!],
                lastAddedCompany: action.payload.lastAddedCompany!
            };
        case CompanySettingsActionTypes.UPDATE_COMPANY_SUCCESS:
        case CompanySettingsActionTypes.CHANGE_COMPANY_STATUS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                companies: (state.companies as Array<CompanyWithDetails>).map(user =>
                    user.id == action.payload.updatedCompany.id ? action.payload.updatedCompany : user),
                lastUpdatedCompany: action.payload.updatedCompany
            };
        case CompanySettingsActionTypes.UPDATE_COMPANY_DETAILS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, company: action.payload.lastUpdatedCompanyWithDetails};
        case CompanySettingsActionTypes.DELETE_COMPANY_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                companies: (state.companies as Array<CompanyWithDetails>).filter((item: CompanyWithDetails) => item.id != action.payload.companyId)
            };
        case CompanySettingsActionTypes.DELETE_USER_COMPANY_ROLE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                company: {
                    ...state.company,
                    userRoles: (state.company.userRoles as Array<UserRole>).filter(userRole =>
                        userRole.id != action.payload.userRoleId),
                }
            };

        case CompanySettingsActionTypes.LOAD_COMPANIES_FAILURE:
        case CompanySettingsActionTypes.LOAD_OPEN_COMPANIES_FAILURE:
        case CompanySettingsActionTypes.LOAD_COMPANY_BY_EMPLOYEE_FAILURE:
        case CompanySettingsActionTypes.LOAD_COMPANY_FAILURE:
        case CompanySettingsActionTypes.ADD_COMPANY_FAILURE:
        case CompanySettingsActionTypes.UPDATE_COMPANY_FAILURE:
        case CompanySettingsActionTypes.UPDATE_COMPANY_DETAILS_FAILURE:
        case CompanySettingsActionTypes.DELETE_COMPANY_FAILURE:
        case CompanySettingsActionTypes.CHANGE_COMPANY_STATUS_FAILURE:
        case CompanySettingsActionTypes.DELETE_USER_COMPANY_ROLE_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }

};

export {reducer as companySettingsReducer}