import {Status, User} from "../../../types";
import {UserRole} from "../role/types";

export type Company = {
    id?: number,
    name: string,
    status: Status,
    workdayStart: string,
    workdayLength: number,
    workdayDelay: number,
    screenshotsSavingMonth: number,
}

type CompanyDetail = {
    userRoles: Array<UserRole>,
    admin: boolean,
    owner: User
}

export type CompanyWithDetails = Company & CompanyDetail;

export type CompanySettingState = {
    employeeCompanies: Array<Company>,
    companies: Array<CompanyWithDetails> | [],
    openCompanies?: Array<Company> | [],
    company: CompanyWithDetails,
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum CompanySettingsActionTypes {
    LOAD_COMPANIES_START = "@@settings/company/LOAD_COMPANIES_START",
    LOAD_COMPANIES_SUCCESS = "@@settings/company/LOAD_COMPANIES_SUCCESS",
    LOAD_COMPANIES_FAILURE = "@@settings/company/LOAD_COMPANIES_FAILURE",

    LOAD_COMPANY_START = "@@settings/company/LOAD_COMPANY_START",
    LOAD_COMPANY_SUCCESS = "@@settings/company/LOAD_COMPANY_SUCCESS",
    LOAD_COMPANY_FAILURE = "@@settings/company/LOAD_COMPANY_FAILURE",

    LOAD_OPEN_COMPANIES_START = "@@settings/company/LOAD_OPEN_COMPANIES_START",
    LOAD_OPEN_COMPANIES_SUCCESS = "@@settings/company/LOAD_OPEN_COMPANIES_SUCCESS",
    LOAD_OPEN_COMPANIES_FAILURE = "@@settings/company/LOAD_OPEN_COMPANIES_FAILURE",

    LOAD_COMPANY_BY_EMPLOYEE_START = "@@settings/company/LOAD_COMPANY_BY_EMPLOYEE_START",
    LOAD_COMPANY_BY_EMPLOYEE_SUCCESS = "@@settings/company/LOAD_COMPANY_BY_EMPLOYEE_SUCCESS",
    LOAD_COMPANY_BY_EMPLOYEE_FAILURE = "@@settings/company/LOAD_COMPANY_BY_EMPLOYEE_FAILURE",

    ADD_COMPANY_START = "@@settings/company/ADD_COMPANY_START",
    ADD_COMPANY_SUCCESS = "@@settings/company/ADD_COMPANY_SUCCESS",
    ADD_COMPANY_FAILURE = "@@settings/company/ADD_COMPANY_FAILURE",

    UPDATE_COMPANY_START = "@@settings/company/UPDATE_COMPANY_START",
    UPDATE_COMPANY_SUCCESS = "@@settings/company/UPDATE_COMPANY_SUCCESS",
    UPDATE_COMPANY_FAILURE = "@@settings/company/UPDATE_COMPANY_FAILURE",

    UPDATE_COMPANY_DETAILS_START = "@@settings/company/UPDATE_COMPANY_DETAILS_START",
    UPDATE_COMPANY_DETAILS_SUCCESS = "@@settings/company/UPDATE_COMPANY_DETAILS_SUCCESS",
    UPDATE_COMPANY_DETAILS_FAILURE = "@@settings/company/UPDATE_COMPANY_DETAILS_FAILURE",

    DELETE_COMPANY_START = "@@settings/company/DELETE_COMPANY_START",
    DELETE_COMPANY_SUCCESS = "@@settings/company/DELETE_COMPANY_SUCCESS",
    DELETE_COMPANY_FAILURE = "@@settings/company/DELETE_COMPANY_FAILURE",

    CHANGE_COMPANY_STATUS_START = "@@settings/company/CHANGE_COMPANY_STATUS_START",
    CHANGE_COMPANY_STATUS_SUCCESS = "@@settings/company/CHANGE_COMPANY_STATUS_SUCCESS",
    CHANGE_COMPANY_STATUS_FAILURE = "@@settings/company/CHANGE_COMPANY_STATUS_FAILURE",

    DELETE_USER_COMPANY_ROLE_START = "@@settings/company/DELETE_USER_COMPANY_ROLE_START",
    DELETE_USER_COMPANY_ROLE_SUCCESS = "@@settings/company/DELETE_USER_COMPANY_ROLE_SUCCESS",
    DELETE_USER_COMPANY_ROLE_FAILURE = "@@settings/company/DELETE_USER_COMPANY_ROLE_FAILURE",
}