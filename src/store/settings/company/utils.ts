import {Company, CompanyWithDetails} from "./types";
import {Status} from "../../../types";

export function companyFromDetailCompany(detailCompany: CompanyWithDetails): Company {
    return {
        id: detailCompany.id,
        name: detailCompany.name,
        screenshotsSavingMonth: detailCompany.screenshotsSavingMonth,
        status: detailCompany.status,
        workdayDelay: detailCompany.workdayDelay,
        workdayLength: detailCompany.workdayLength,
        workdayStart: detailCompany.workdayStart
    }
}

export function getEmptyCompany(): Company {
    return {
        name: "",
        status: Status.OPEN,
        workdayStart: "00:00",
        workdayLength: 0,
        workdayDelay: 0,
        screenshotsSavingMonth: 0
    }
}