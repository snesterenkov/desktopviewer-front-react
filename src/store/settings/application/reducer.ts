import {Reducer} from "redux";

import * as departmentSettingsActions from "./actions";
import {ActionType} from "typesafe-actions";
import {Application, ApplicationSettingsActionTypes, ApplicationSettingState} from "./types";

export type ApplicationSettingAction = ActionType<typeof departmentSettingsActions>;

const initialState: ApplicationSettingState = {
    applications: [],
    applicationsForDepartment: {},
    neutralApplicationsForDepartment: [],
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<ApplicationSettingState, ApplicationSettingAction> = (state: ApplicationSettingState = initialState, action: ApplicationSettingAction) => {
    switch (action.type) {
        case ApplicationSettingsActionTypes.LOAD_APPLICATIONS_BY_DEPARTMENT_START:
        case ApplicationSettingsActionTypes.LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_START:
        case ApplicationSettingsActionTypes.ADD_APPLICATION_START:
        case ApplicationSettingsActionTypes.UPDATE_APPLICATION_START:
            return {...state, isLoading: true, isLoaded: false};
        case ApplicationSettingsActionTypes.LOAD_APPLICATIONS_BY_DEPARTMENT_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, applicationsForDepartment: action.payload.applicationsForDepartment};
        case ApplicationSettingsActionTypes.LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, neutralApplicationsForDepartment: action.payload.neutralApplicationsForDepartment};
        case ApplicationSettingsActionTypes.ADD_APPLICATION_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                neutralApplicationsForDepartment: [...state.neutralApplicationsForDepartment, action.payload.application],
                //lastAddedApplication: action.payload.lastAddedApplication!
            };
        case ApplicationSettingsActionTypes.UPDATE_APPLICATION_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                applications: (state.applications as Array<Application>).map((application: Application) =>
                    application.id == action.payload.updatedApplication.id ? {...application, ...action.payload.updatedApplication} : application),
                // lastUpdatedProject: action.payload.updatedProject
            };
        case ApplicationSettingsActionTypes.CHANGE_APPLICATION_STATUS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true};
        case ApplicationSettingsActionTypes.LOAD_APPLICATIONS_BY_DEPARTMENT_FAILURE:
        case ApplicationSettingsActionTypes.LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_FAILURE:
        case ApplicationSettingsActionTypes.ADD_APPLICATION_FAILURE:
        case ApplicationSettingsActionTypes.UPDATE_APPLICATION_FAILURE:
        case ApplicationSettingsActionTypes.CHANGE_APPLICATION_STATUS_FAILURE:
            return {...state, isLoading: false, isLoaded: false};
        default:
            return state
    }

};

export {reducer as applicationSettingsReducer}