export type Application = {
    id?: number,
    name: string,
}

export type ApplicationProductiveTypes = {
    PRODUCTIVE: Array<Application>,
    UNPRODUCTIVE: Array<Application>
}

export type ApplicationSettingState = {
    applications: Array<Application> | [],
    applicationsForDepartment: ApplicationProductiveTypes | {},
    neutralApplicationsForDepartment: Array<Application> | [],
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum ApplicationSettingsActionTypes {
    LOAD_APPLICATIONS_START = "@@settings/application/LOAD_APPLICATIONS_START",
    LOAD_APPLICATIONS_SUCCESS = "@@settings/application/LOAD_APPLICATIONS_SUCCESS",
    LOAD_APPLICATIONS_FAILURE = "@@settings/application/LOAD_APPLICATIONS_FAILURE",

    LOAD_APPLICATIONS_BY_DEPARTMENT_START = "@@settings/application/LOAD_APPLICATIONS_BY_DEPARTMENT_START",
    LOAD_APPLICATIONS_BY_DEPARTMENT_SUCCESS = "@@settings/application/LOAD_APPLICATIONS_BY_DEPARTMENT_SUCCESS",
    LOAD_APPLICATIONS_BY_DEPARTMENT_FAILURE = "@@settings/application/LOAD_APPLICATIONS_BY_DEPARTMENT_FAILURE",

    LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_START = "@@settings/application/LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_START",
    LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_SUCCESS = "@@settings/application/LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_SUCCESS",
    LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_FAILURE = "@@settings/application/LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_FAILURE",

    ADD_APPLICATION_START = "@@settings/application/ADD_APPLICATION_START",
    ADD_APPLICATION_SUCCESS = "@@settings/application/ADD_APPLICATION_SUCCESS",
    ADD_APPLICATION_FAILURE = "@@settings/application/ADD_APPLICATION_FAILURE",

    UPDATE_APPLICATION_START = "@@settings/application/UPDATE_APPLICATION_START",
    UPDATE_APPLICATION_SUCCESS = "@@settings/application/UPDATE_APPLICATION_SUCCESS",
    UPDATE_APPLICATION_FAILURE = "@@settings/application/UPDATE_APPLICATION_FAILURE",

    DELETE_APPLICATION_START = "@@settings/application/DELETE_APPLICATION_START",
    DELETE_APPLICATION_SUCCESS = "@@settings/application/DELETE_APPLICATION_SUCCESS",
    DELETE_APPLICATION_FAILURE = "@@settings/application/DELETE_APPLICATION_FAILURE",

    CHANGE_APPLICATION_STATUS_START = "@@settings/application/CHANGE_APPLICATION_STATUS_START",
    CHANGE_APPLICATION_STATUS_SUCCESS = "@@settings/application/CHANGE_APPLICATION_STATUS_SUCCESS",
    CHANGE_APPLICATION_STATUS_FAILURE = "@@settings/application/CHANGE_APPLICATION_STATUS_FAILURE",
}