import {action} from "typesafe-actions";
import {Application, ApplicationProductiveTypes, ApplicationSettingsActionTypes} from "./types";
import axios, {AxiosRequestConfig} from "axios";
import {Dispatch} from "redux";
import {createConfig} from "../../../utils";
import {API_APPLICATION_SETTINGS_URL} from "../../../constants";
import {Productivity} from "../../../types";

export const loadApplicationsByDepartmentRequest = () => action(ApplicationSettingsActionTypes.LOAD_APPLICATIONS_BY_DEPARTMENT_START);
export const loadApplicationsByDepartmentSuccess = (applicationsForDepartment: ApplicationProductiveTypes) => action(ApplicationSettingsActionTypes.LOAD_APPLICATIONS_BY_DEPARTMENT_SUCCESS, {applicationsForDepartment});
export const loadApplicationsByDepartmentFailure = (message: string) => action(ApplicationSettingsActionTypes.LOAD_APPLICATIONS_BY_DEPARTMENT_FAILURE, {message});

export const loadNeutralApplicationsByDepartmentRequest = () => action(ApplicationSettingsActionTypes.LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_START);
export const loadNeutralApplicationsByDepartmentSuccess = (neutralApplicationsForDepartment: Array<Application>) => action(ApplicationSettingsActionTypes.LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_SUCCESS, {neutralApplicationsForDepartment});
export const loadNeutralApplicationsByDepartmentFailure = (message: string) => action(ApplicationSettingsActionTypes.LOAD_NEUTRAL_APPLICATIONS_BY_DEPARTMENT_FAILURE, {message});

export const addApplicationRequest = () => action(ApplicationSettingsActionTypes.ADD_APPLICATION_START);
export const addApplicationSuccess = (application: Application) => action(ApplicationSettingsActionTypes.ADD_APPLICATION_SUCCESS, {application});
export const addApplicationFailure = (message: string) => action(ApplicationSettingsActionTypes.ADD_APPLICATION_FAILURE, {message});

export const updateApplicationRequest = () => action(ApplicationSettingsActionTypes.UPDATE_APPLICATION_START);
export const updateApplicationSuccess = (updatedApplication: Application) => action(ApplicationSettingsActionTypes.UPDATE_APPLICATION_SUCCESS, {updatedApplication});
export const updateApplicationFailure = (message: string) => action(ApplicationSettingsActionTypes.UPDATE_APPLICATION_FAILURE, {message});

export const deleteApplicationRequest = () => action(ApplicationSettingsActionTypes.DELETE_APPLICATION_START);
export const deleteApplicationSuccess = (applications: Array<Application>) => action(ApplicationSettingsActionTypes.DELETE_APPLICATION_SUCCESS, {applications});
export const deleteApplicationFailure = (message: string) => action(ApplicationSettingsActionTypes.DELETE_APPLICATION_FAILURE, {message});

export const changeApplicationsStatusRequest = () => action(ApplicationSettingsActionTypes.CHANGE_APPLICATION_STATUS_START);
export const changeApplicationsStatusSuccess = () => action(ApplicationSettingsActionTypes.CHANGE_APPLICATION_STATUS_SUCCESS);
export const changeApplicationsStatusFailure = (message: string) => action(ApplicationSettingsActionTypes.CHANGE_APPLICATION_STATUS_FAILURE, {message});

export const getApplicationsForDepartment = (departmentId: number) => {
    return async (dispatch: Dispatch) => {
        const url = API_APPLICATION_SETTINGS_URL + "/department/" + departmentId;
        dispatch(loadApplicationsByDepartmentRequest());
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadApplicationsByDepartmentSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadApplicationsByDepartmentFailure(err.message));
            });
    }
};

export const getNeutralApplicationsForDepartment = (departmentId: number) => {
    return async (dispatch: Dispatch) => {
        const url = API_APPLICATION_SETTINGS_URL + "/department/free/" + departmentId;
        dispatch(loadNeutralApplicationsByDepartmentRequest());
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadNeutralApplicationsByDepartmentSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadNeutralApplicationsByDepartmentFailure(err.message));
            });
    }
};


export const addApplication = (project: Application) => {
    return async (dispatch: Dispatch) => {
        dispatch(addApplicationRequest());
        return await axios.post(API_APPLICATION_SETTINGS_URL, project, createConfig(API_APPLICATION_SETTINGS_URL))
            .then(res => {
                dispatch(addApplicationSuccess(res.data));
            })
            .catch(err => {
                dispatch(addApplicationFailure(err.message));
                console.error(err.message);
            });
    }
};

export const updateApplication = (project: Application) => {
    return async (dispatch: Dispatch) => {
        let url = API_APPLICATION_SETTINGS_URL + "/" + project.id;
        dispatch(updateApplicationRequest());
        return await axios.put(url, project, createConfig(url))
            .then(res => {
                dispatch(updateApplicationSuccess(res.data));
            })
            .catch(err => {
                dispatch(updateApplicationFailure(err.message));
                console.error(err.message);
            });
    }
};

export const deleteApplication = (applications: Array<Application>) => {
    return async (dispatch: Dispatch) => {
        let url = API_APPLICATION_SETTINGS_URL + "/delete";
        dispatch(deleteApplicationRequest());
        return await axios.post(url, applications, createConfig(url))
            .then(() => {
                dispatch(deleteApplicationSuccess(applications));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(deleteApplicationFailure(err.message));
            });
    }
};

export const changeApplicationsStatus = (departmentId: number, applications: Array<Application>, status: Productivity) => {
    return async (dispatch: Dispatch) => {
        let url = API_APPLICATION_SETTINGS_URL + "/department/attach/" + departmentId;
        let config: AxiosRequestConfig = createConfig(url, {productivity: status});
        dispatch(changeApplicationsStatusRequest());
        return await axios.post(url, applications, config)
            .then(() => {
                dispatch(changeApplicationsStatusSuccess());
            })
            .catch(err => {
                dispatch(changeApplicationsStatusFailure(err.message));
                console.error(err.message);
            });
    }
};

