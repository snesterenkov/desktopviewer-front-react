import {action} from "typesafe-actions";
import {Project, ProjectSettingsActionTypes, ProjectWithDetails} from "./types";
import {API_PROJECT_SETTINGS_URL} from "../../../constants";
import axios, {AxiosRequestConfig} from "axios";
import {Dispatch} from "redux";
import {createConfig} from "../../../utils";

export const loadProjectsRequest = () => action(ProjectSettingsActionTypes.LOAD_PROJECTS_START);
export const loadProjectsSuccess = (projects: Array<ProjectWithDetails>) => action(ProjectSettingsActionTypes.LOAD_PROJECTS_SUCCESS, {projects});
export const loadProjectsFailure = (message: string) => action(ProjectSettingsActionTypes.LOAD_PROJECTS_FAILURE, {message});

export const loadUserDependentProjectsRequest = () => action(ProjectSettingsActionTypes.LOAD_USER_DEPENDENT_PROJECTS_START);
export const loadUserDependentProjectsSuccess = (userDependentProjects: Array<ProjectWithDetails>) => action(ProjectSettingsActionTypes.LOAD_USER_DEPENDENT_PROJECTS_SUCCESS, {userDependentProjects});
export const loadUserDependentProjectsFailure = (message: string) => action(ProjectSettingsActionTypes.LOAD_USER_DEPENDENT_PROJECTS_FAILURE, {message});

export const loadProjectRequest = () => action(ProjectSettingsActionTypes.LOAD_PROJECT_START);
export const loadProjectSuccess = (project: ProjectWithDetails) => action(ProjectSettingsActionTypes.LOAD_PROJECT_SUCCESS, {project});
export const loadProjectFailure = (message: string) => action(ProjectSettingsActionTypes.LOAD_PROJECT_FAILURE, {message});

export const addProjectRequest = () => action(ProjectSettingsActionTypes.ADD_PROJECT_START);
export const addProjectSuccess = (project: ProjectWithDetails) => action(ProjectSettingsActionTypes.ADD_PROJECT_SUCCESS, {lastAddedProject: project});
export const addProjectFailure = (message: string) => action(ProjectSettingsActionTypes.ADD_PROJECT_FAILURE, {message});

export const updateProjectRequest = () => action(ProjectSettingsActionTypes.UPDATE_PROJECT_START);
export const updateProjectSuccess = (updatedProject: ProjectWithDetails) => action(ProjectSettingsActionTypes.UPDATE_PROJECT_SUCCESS, {updatedProject});
export const updateProjectFailure = (message: string) => action(ProjectSettingsActionTypes.UPDATE_PROJECT_FAILURE, {message});

export const updateProjectDetailsRequest = () => action(ProjectSettingsActionTypes.UPDATE_PROJECT_DETAILS_START);
export const updateProjectDetailsSuccess = (updatedProject: ProjectWithDetails) => action(ProjectSettingsActionTypes.UPDATE_PROJECT_DETAILS_SUCCESS, {updatedProject});
export const updateProjectDetailsFailure = (message: string) => action(ProjectSettingsActionTypes.UPDATE_PROJECT_DETAILS_FAILURE, {message});

export const deleteProjectRequest = () => action(ProjectSettingsActionTypes.DELETE_PROJECT_START);
export const deleteProjectSuccess = (projectId: number) => action(ProjectSettingsActionTypes.DELETE_PROJECT_SUCCESS, {projectId});
export const deleteProjectFailure = (message: string) => action(ProjectSettingsActionTypes.DELETE_PROJECT_FAILURE, {message});

export const changeProjectStatusRequest = () => action(ProjectSettingsActionTypes.CHANGE_PROJECT_STATUS_START);
export const changeProjectStatusSuccess = (updatedProject: ProjectWithDetails) => action(ProjectSettingsActionTypes.CHANGE_PROJECT_STATUS_SUCCESS, {updatedProject});
export const changeProjectStatusFailure = (message: string) => action(ProjectSettingsActionTypes.CHANGE_PROJECT_STATUS_FAILURE, {message});

export const deleteUserProjectRoleRequest = () => action(ProjectSettingsActionTypes.DELETE_USER_PROJECT_ROLE_START);
export const deleteUserProjectRoleSuccess = (userRoleId: number) => action(ProjectSettingsActionTypes.DELETE_USER_PROJECT_ROLE_SUCCESS, {userRoleId});
export const deleteUserProjectRoleFailure = (message: string) => action(ProjectSettingsActionTypes.DELETE_USER_PROJECT_ROLE_FAILURE, {message});

export const getProjects = () => {
    return async (dispatch: Dispatch) => {
        dispatch(loadProjectsRequest());
        return await axios.get(API_PROJECT_SETTINGS_URL, createConfig(API_PROJECT_SETTINGS_URL))
            .then(res => {
                dispatch(loadProjectsSuccess(res.data));
                //           alert("список подразделений загружен");
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadProjectsFailure(err.message));
                //      alert("ошибка при загрузке списка подразделений");
            });
    }
};

export const getProject = (projectId: number) => {
    return async (dispatch: Dispatch) => {
        dispatch(loadProjectRequest());
        let url = API_PROJECT_SETTINGS_URL + "/" + projectId;
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadProjectSuccess(res.data));
                //    alert("проект загружен");
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadProjectFailure(err.message));
                //       alert("ошибка при загрузке проекта");
            });
    }
};

export const getUserDependentProjects = () => {
    return async (dispatch: Dispatch) => {
        let url = API_PROJECT_SETTINGS_URL + "/dependent";
        dispatch(loadUserDependentProjectsRequest());
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadUserDependentProjectsSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadUserDependentProjectsFailure(err.message));
            });
    }
};

export const addProject = (project: Project) => {
    return async (dispatch: Dispatch) => {
        dispatch(addProjectRequest());
        return await axios.post(API_PROJECT_SETTINGS_URL, project, createConfig(API_PROJECT_SETTINGS_URL))
            .then(res => {
                dispatch(addProjectSuccess(res.data));
                //     alert("отдел добален");
            })
            .catch(err => {
                dispatch(addProjectFailure(err.message));
                console.error(err.message);
                //      alert("ошибка при добавлении отдела");
            });
    }
};

export const updateProject = (project: Project) => {
    return async (dispatch: Dispatch) => {
        let url = API_PROJECT_SETTINGS_URL + "/" + project.id;
        dispatch(updateProjectRequest());
        return await axios.put(url, project, createConfig(url))
            .then(res => {
                dispatch(updateProjectSuccess(res.data));
                //alert("проект обновлен");
            })
            .catch(err => {
                dispatch(updateProjectFailure(err.message));
                console.error(err.message);
                // alert("ошибка при обновлении проекта");
            });
    }
};

export const updateDetailsProject = (project: ProjectWithDetails) => {
    return async (dispatch: Dispatch) => {
        let url = API_PROJECT_SETTINGS_URL + "/detailupdate/" + project.id;
        dispatch(updateProjectDetailsRequest());
        return await axios.put(url, project, createConfig(url))
            .then(res => {
                dispatch(updateProjectDetailsSuccess(res.data));
                //   alert("проект обновлен");
            })
            .catch(err => {
                dispatch(updateProjectDetailsFailure(err.message));
                console.error(err.message);
                //  alert("ошибка при обновлении проекта");
            });
    }
};

export const deleteProject = (projectId: number) => {
    return async (dispatch: Dispatch) => {
        let url = API_PROJECT_SETTINGS_URL + "/" + projectId;
        dispatch(deleteProjectRequest());
        return await axios.delete(url, createConfig(url))
            .then(() => {
                dispatch(deleteProjectSuccess(projectId));
                // alert("отдел удален");
            })
            .catch(err => {
                console.error(err.message);
                // alert("ошибка при удалении отдела");
                dispatch(deleteProjectFailure(err.message));
            });
    }
};

export const changeProjectStatus = (projectId: number, status: string) => {
    return async (dispatch: Dispatch) => {
        let url = API_PROJECT_SETTINGS_URL + "/changestatus/" + projectId;
        let config: AxiosRequestConfig = createConfig(url);
        config.headers = {
            'Content-Type': 'application/json;charset=UTF-8'
        };
        dispatch(changeProjectStatusRequest());
        return await axios.put(url, JSON.stringify(status), config)
            .then((res) => {
                dispatch(changeProjectStatusSuccess(res.data));
                //  alert("статус подразделения изменен");
            })
            .catch(err => {
                dispatch(changeProjectStatusFailure(err.message));
                console.error(err.message);
                // alert("ошибка при изменении статуса подразделения");
            });
    }
};


export const deleteUserProjectRole = (userRoleId: number) => {
    return async (dispatch: Dispatch) => {
        let url = API_PROJECT_SETTINGS_URL + "/removeUserProjectRole/" + userRoleId;
        dispatch((deleteUserProjectRoleRequest()));
        return await axios.delete(url, createConfig(url))
            .then(() => {
                dispatch(deleteUserProjectRoleSuccess(userRoleId));
                //   alert("роль пользователя в проекте удалена");
            })
            .catch(err => {
                dispatch(deleteUserProjectRoleFailure(err.message));
                console.error(err.message);
                //      alert("ошибка при удалении роли пользователя");
            });
    }
};
