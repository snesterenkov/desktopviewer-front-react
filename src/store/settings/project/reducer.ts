import {Reducer} from "redux";

import * as departmentSettingsActions from "./actions";
import {ActionType} from "typesafe-actions";
import {Project, ProjectSettingsActionTypes, ProjectSettingState, ProjectWithDetails} from "./types";
import {UserRole} from "../role/types";
import {getEmptyProject} from "./utils";
import {getEmptyUser} from "../../../utils";

export type ProjectSettingAction = ActionType<typeof departmentSettingsActions>;

const initialState: ProjectSettingState = {
    projects: [],
    userDependentProjects: [],
    project: {...getEmptyProject(), admin: false, userRoles: [], owner: getEmptyUser()},
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<ProjectSettingState, ProjectSettingAction> = (state: ProjectSettingState = initialState, action: ProjectSettingAction) => {
    switch (action.type) {
        case ProjectSettingsActionTypes.LOAD_PROJECTS_START:
        case ProjectSettingsActionTypes.LOAD_USER_DEPENDENT_PROJECTS_START:
        case ProjectSettingsActionTypes.LOAD_PROJECT_START:
        case ProjectSettingsActionTypes.ADD_PROJECT_START:
        case ProjectSettingsActionTypes.UPDATE_PROJECT_START:
        case ProjectSettingsActionTypes.UPDATE_PROJECT_DETAILS_START:
        case ProjectSettingsActionTypes.DELETE_PROJECT_START:
        case ProjectSettingsActionTypes.CHANGE_PROJECT_STATUS_START:
        case ProjectSettingsActionTypes.DELETE_USER_PROJECT_ROLE_START:
            return {...state, isLoading: true, isLoaded: false};
        case ProjectSettingsActionTypes.LOAD_PROJECTS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, projects: action.payload.projects};
        case ProjectSettingsActionTypes.LOAD_USER_DEPENDENT_PROJECTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                userDependentProjects: action.payload.userDependentProjects
            };
        case ProjectSettingsActionTypes.LOAD_PROJECT_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, project: action.payload.project};
        case ProjectSettingsActionTypes.ADD_PROJECT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                projects: [...state.projects, action.payload.lastAddedProject!],
                lastAddedProject: action.payload.lastAddedProject!
            };
        case ProjectSettingsActionTypes.UPDATE_PROJECT_SUCCESS:
        case ProjectSettingsActionTypes.UPDATE_PROJECT_DETAILS_SUCCESS:
        case ProjectSettingsActionTypes.CHANGE_PROJECT_STATUS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                projects: (state.projects as Array<ProjectWithDetails>).map((project: ProjectWithDetails) =>
                    project.id == action.payload.updatedProject.id ? {...project as ProjectWithDetails, ...action.payload.updatedProject as Project} : project),
                project: action.payload.updatedProject
            };
        case ProjectSettingsActionTypes.DELETE_PROJECT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                projects: (state.projects as Array<ProjectWithDetails>).filter((item: ProjectWithDetails) => item.id != action.payload.projectId)
            };
        case ProjectSettingsActionTypes.DELETE_USER_PROJECT_ROLE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                project: {
                    ...state.project,
                    userRoles: (state.project.userRoles as Array<UserRole>).filter(userRole =>
                        userRole.id != action.payload.userRoleId),
                }
            };

        case ProjectSettingsActionTypes.LOAD_PROJECTS_FAILURE:
        case ProjectSettingsActionTypes.LOAD_USER_DEPENDENT_PROJECTS_FAILURE:
        case ProjectSettingsActionTypes.LOAD_PROJECT_FAILURE:
        case ProjectSettingsActionTypes.ADD_PROJECT_FAILURE:
        case ProjectSettingsActionTypes.UPDATE_PROJECT_FAILURE:
        case ProjectSettingsActionTypes.UPDATE_PROJECT_DETAILS_FAILURE:
        case ProjectSettingsActionTypes.DELETE_PROJECT_FAILURE:
        case ProjectSettingsActionTypes.CHANGE_PROJECT_STATUS_FAILURE:
        case ProjectSettingsActionTypes.DELETE_USER_PROJECT_ROLE_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }

};

export {reducer as projectSettingsReducer}