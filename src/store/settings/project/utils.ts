import {Project, ProjectWithDetails} from "./types";
import {Status} from "../../../types";
import {getEmptyDepartment} from "../department/utils";

export function projectFromDetailProject(detailProject: ProjectWithDetails): Project {
    return {
        id: detailProject.id,
        name: detailProject.name,
        status: detailProject.status,
        departmentId: detailProject.departmentId,
        parentStatus: detailProject.parentStatus,
        department: detailProject.department
    }
}

export function getEmptyProject(): Project {
    return {
        name: '',
        status: Status.OPEN,
        departmentId: 0,
        parentStatus: Status.OPEN,
        department: getEmptyDepartment()
    }
}