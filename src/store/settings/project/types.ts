import {Status, User} from "../../../types";
import {Department} from "../department/types";
import {UserRole} from "../role/types";

export type Project = {
    id?: number,
    name: string,
    departmentId: number,
    status: Status,
    parentStatus: Status,
    department: Department
}

export type ProjectDetail = {
    userRoles: Array<UserRole>,
    admin: boolean,
    owner: User
}

export type ProjectWithDetails = Project & ProjectDetail;

export type ProjectSettingState = {
    projects: Array<ProjectWithDetails> | [],
    project: ProjectWithDetails,
    userDependentProjects: Array<ProjectWithDetails> | [],
    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export enum ProjectSettingsActionTypes {
    LOAD_PROJECTS_START = "@@settings/project/LOAD_PROJECTS_START",
    LOAD_PROJECTS_SUCCESS = "@@settings/project/LOAD_PROJECTS_SUCCESS",
    LOAD_PROJECTS_FAILURE = "@@settings/project/LOAD_PROJECTS_FAILURE",

    LOAD_USER_DEPENDENT_PROJECTS_START = "@@settings/project/LOAD_USER_DEPENDENT_PROJECTS_START",
    LOAD_USER_DEPENDENT_PROJECTS_SUCCESS = "@@settings/project/LOAD_USER_DEPENDENT_PROJECTS_SUCCESS",
    LOAD_USER_DEPENDENT_PROJECTS_FAILURE = "@@settings/project/LOAD_USER_DEPENDENT_PROJECTS_FAILURE",

    LOAD_PROJECT_START = "@@settings/project/LOAD_PROJECT_START",
    LOAD_PROJECT_SUCCESS = "@@settings/project/LOAD_PROJECT_SUCCESS",
    LOAD_PROJECT_FAILURE = "@@settings/project/LOAD_PROJECT_FAILURE",

    ADD_PROJECT_START = "@@settings/project/ADD_PROJECT_START",
    ADD_PROJECT_SUCCESS = "@@settings/project/ADD_PROJECT_SUCCESS",
    ADD_PROJECT_FAILURE = "@@settings/project/ADD_PROJECT_FAILURE",

    UPDATE_PROJECT_START = "@@settings/project/UPDATE_PROJECT_START",
    UPDATE_PROJECT_SUCCESS = "@@settings/project/UPDATE_PROJECT_SUCCESS",
    UPDATE_PROJECT_FAILURE = "@@settings/project/UPDATE_PROJECT_FAILURE",

    UPDATE_PROJECT_DETAILS_START = "@@settings/project/UPDATE_PROJECT_DETAILS_START",
    UPDATE_PROJECT_DETAILS_SUCCESS = "@@settings/project/UPDATE_PROJECT_DETAILS_SUCCESS",
    UPDATE_PROJECT_DETAILS_FAILURE = "@@settings/project/UPDATE_PROJECT_DETAILS_FAILURE",

    DELETE_PROJECT_START = "@@settings/project/DELETE_PROJECT_START",
    DELETE_PROJECT_SUCCESS = "@@settings/project/DELETE_PROJECT_SUCCESS",
    DELETE_PROJECT_FAILURE = "@@settings/project/DELETE_PROJECT_FAILURE",

    CHANGE_PROJECT_STATUS_START = "@@settings/project/CHANGE_PROJECT_STATUS_START",
    CHANGE_PROJECT_STATUS_SUCCESS = "@@settings/project/CHANGE_PROJECT_STATUS_SUCCESS",
    CHANGE_PROJECT_STATUS_FAILURE = "@@settings/project/CHANGE_PROJECT_STATUS_FAILURE",

    DELETE_USER_PROJECT_ROLE_START = "@@settings/project/DELETE_USER_PROJECT_ROLE_START",
    DELETE_USER_PROJECT_ROLE_SUCCESS = "@@settings/project/DELETE_USER_PROJECT_ROLE_SUCCESS",
    DELETE_USER_PROJECT_ROLE_FAILURE = "@@settings/project/DELETE_USER_PROJECT_ROLE_FAILURE",
}