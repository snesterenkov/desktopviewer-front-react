import {action} from "typesafe-actions";
import {CompanyWithProjects, WorkDiaryActionTypes, WorkPeriodStatistic} from "./types";
import {Dispatch} from "redux";
import axios from "axios";
import {createConfig} from "../../../utils";
import {API_REPORTS_URL} from "../../../constants";
import {ActivityPeriod} from "../../../types";
import {Project} from "../../settings/project/types";

export const loadProjectsListRequest = () => action(WorkDiaryActionTypes.LOAD_COMPANIES_WITH_PROJECTS_START);
export const loadProjectsListSuccess = (companiesWithProjects: Array<CompanyWithProjects>) => action(WorkDiaryActionTypes.LOAD_COMPANIES_WITH_PROJECTS_SUCCESS, {companiesWithProjects});
export const loadProjectsListFailure = (message: string) => action(WorkDiaryActionTypes.LOAD_COMPANIES_WITH_PROJECTS_FAILURE, {message});

export const loadWorkPeriodStatisticsRequest = () => action(WorkDiaryActionTypes.LOAD_WORK_PERIOD_STATISTICS_START);
export const loadWorkPeriodStatisticsSuccess = (workPeriodStatistics: Array<WorkPeriodStatistic>) => action(WorkDiaryActionTypes.LOAD_WORK_PERIOD_STATISTICS_SUCCESS, {workPeriodStatistics});
export const loadWorkPeriodStatisticsFailure = (message: string) => action(WorkDiaryActionTypes.LOAD_WORK_PERIOD_STATISTICS_FAILURE, {message});

export const getProjectsList = () => {
    return async (dispatch: Dispatch) => {
        const url = API_REPORTS_URL + "/admin/projects/list";
        dispatch(loadProjectsListRequest());
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(loadProjectsListSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadProjectsListFailure(err.message));
            });
    }
};

export const getWorkPeriodStatistics = (period: ActivityPeriod, startDate: string, endDate: string, projectDTOs: Array<Project>) => {
    return async (dispatch: Dispatch) => {
        let url = API_REPORTS_URL + "/workDiary";
        dispatch(loadWorkPeriodStatisticsRequest());
        return await axios.post(url, {projectDTOs}, createConfig(url, {period, startDate, endDate}))
            .then(res => {
                dispatch(loadWorkPeriodStatisticsSuccess(res.data));
            })
            .catch(err => {
                console.error(err.message);
                dispatch(loadWorkPeriodStatisticsFailure(err.message));
            });
    }
};