import {Company} from "../../settings/company/types";
import {Department} from "../../settings/department/types";
import {Project} from "../../settings/project/types";
import {User} from "../../../types";

export type WorkDiaryState = {
    companiesWithProjects: Array<CompanyWithProjects>,
    workPeriodStatistics: Array<WorkPeriodStatistic>,

    isLoaded: boolean,
    isLoading: boolean,
    message?: string
}

export type Period = {
    startDate: number,
    endDate: number,
    efficiency: number,
    hours: number,
}

export type CompanyWithProjects = Company & { departments: Array<Department & { projects: Array<Project> }> };

export type WorkPeriodStatistic = {
    user: User;
    periods: Array<Period>
}

export enum WorkDiaryActionTypes {
    LOAD_COMPANIES_WITH_PROJECTS_START = "@@settings/user/LOAD_COMPANIES_WITH_PROJECTS_START",
    LOAD_COMPANIES_WITH_PROJECTS_SUCCESS = "@@settings/user/LOAD_COMPANIES_WITH_PROJECTS_SUCCESS",
    LOAD_COMPANIES_WITH_PROJECTS_FAILURE = "@@settings/user/LOAD_COMPANIES_WITH_PROJECTS_FAILURE",

    LOAD_WORK_PERIOD_STATISTICS_START = "@@settings/user/LOAD_WORK_PERIOD_STATISTICS_START",
    LOAD_WORK_PERIOD_STATISTICS_SUCCESS = "@@settings/user/LOAD_WORK_PERIOD_STATISTICS_SUCCESS",
    LOAD_WORK_PERIOD_STATISTICS_FAILURE = "@@settings/user/LOAD_WORK_PERIOD_STATISTICS_FAILURE",
}