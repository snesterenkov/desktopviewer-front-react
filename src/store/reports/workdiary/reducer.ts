import {Reducer} from "redux";

import * as workDiaryActions from "./actions";
import {ActionType} from "typesafe-actions";
import {WorkDiaryActionTypes, WorkDiaryState} from "./types";

export type WorkDiaryAction = ActionType<typeof workDiaryActions>;

const initialState: WorkDiaryState = {
    workPeriodStatistics: [],
    companiesWithProjects: [],
    isLoaded: false,
    isLoading: false
};

const reducer: Reducer<WorkDiaryState, WorkDiaryAction> = (state: WorkDiaryState = initialState, action: WorkDiaryAction): WorkDiaryState => {
    switch (action.type) {
        case WorkDiaryActionTypes.LOAD_COMPANIES_WITH_PROJECTS_START:
        case WorkDiaryActionTypes.LOAD_WORK_PERIOD_STATISTICS_START:
            return {...state, isLoading: true, isLoaded: false};
        case WorkDiaryActionTypes.LOAD_COMPANIES_WITH_PROJECTS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, companiesWithProjects: action.payload.companiesWithProjects!};
        case WorkDiaryActionTypes.LOAD_WORK_PERIOD_STATISTICS_SUCCESS:
            return {...state, isLoading: false, isLoaded: true, workPeriodStatistics: action.payload.workPeriodStatistics};
        case WorkDiaryActionTypes.LOAD_COMPANIES_WITH_PROJECTS_FAILURE:
        case WorkDiaryActionTypes.LOAD_WORK_PERIOD_STATISTICS_FAILURE:
            return {...state, isLoading: false, isLoaded: false, message: action.payload.message};
        default:
            return state
    }

};

export {reducer as workDiaryReducer}