import {ActionType} from "typesafe-actions";
import {Reducer} from "redux";

import * as inviteActions from "./actions";
import {InviteActionTypes, InviteState} from "./types";

export type InviteAction = ActionType<typeof inviteActions>;

const initialState: InviteState = {
    isLoading: false
};

const reducer: Reducer<InviteState> = (state: InviteState = initialState, action: InviteAction): InviteState => {
    switch (action.type) {
        case InviteActionTypes.CREATE_INVITE_REQUEST:
        case InviteActionTypes.GET_INVITE_BY_TOKEN_REQUEST:
        case InviteActionTypes.CONFIRM_INVITE_REQUEST:
            return {...state, isLoading: true};
        case InviteActionTypes.CREATE_INVITE_SUCCESS:
        case InviteActionTypes.GET_INVITE_BY_TOKEN_SUCCESS:
        case InviteActionTypes.CONFIRM_INVITE_SUCCESS:
            return {...state, isLoading: false, invite: action.payload.invite};
        case InviteActionTypes.CREATE_INVITE_FAILURE:
        case InviteActionTypes.GET_INVITE_BY_TOKEN_FAILURE:
        case InviteActionTypes.CONFIRM_INVITE_FAILURE:
            return {...state, isLoading: false, error: action.payload.error};
        default:
            return state
    }
};

export {reducer as inviteReducer}