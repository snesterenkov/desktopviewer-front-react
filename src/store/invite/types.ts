import {User} from "../../types";

export type InviteState = {
    isLoading: boolean,
    error?: string,
    invite?: Invite
}

export type Invite = {
    type: string;
    structureId: number;
    roleId: number;
    email: string;
    user?: User;
}

export enum InviteActionTypes {
    CREATE_INVITE_REQUEST = "@@auth/CREATE_INVITE_REQUEST",
    CREATE_INVITE_SUCCESS = "@@auth/CREATE_INVITE_SUCCESS",
    CREATE_INVITE_FAILURE = "@@auth/CREATE_INVITE_FAILURE",

    GET_INVITE_BY_TOKEN_REQUEST = "@@auth/GET_INVITE_BY_TOKEN_REQUEST",
    GET_INVITE_BY_TOKEN_SUCCESS = "@@auth/GET_INVITE_BY_TOKEN_SUCCESS",
    GET_INVITE_BY_TOKEN_FAILURE = "@@auth/GET_INVITE_BY_TOKEN_FAILURE",

    CONFIRM_INVITE_REQUEST = "@@auth/CONFIRM_INVITE_REQUEST",
    CONFIRM_INVITE_SUCCESS = "@@auth/CONFIRM_INVITE_SUCCESS",
    CONFIRM_INVITE_FAILURE = "@@auth/CONFIRM_INVITE_FAILURE",
}