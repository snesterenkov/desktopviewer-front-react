import {Invite, InviteActionTypes} from "./types";
import {action} from "typesafe-actions";
import {Dispatch} from "redux";
import axios from "axios";
import {API_INVITE_URL} from "../../constants";
import {createConfig} from "../../utils";

export const createInviteRequest = () => action(InviteActionTypes.CREATE_INVITE_REQUEST);
export const createInviteSuccess = (invite: Invite) => action(InviteActionTypes.CREATE_INVITE_SUCCESS, {invite});
export const createInviteFailure = (error: string) => action(InviteActionTypes.CREATE_INVITE_FAILURE, {error});

export const getInviteByTokenRequest = () => action(InviteActionTypes.GET_INVITE_BY_TOKEN_REQUEST);
export const getInviteByTokenSuccess = (invite: Invite) => action(InviteActionTypes.GET_INVITE_BY_TOKEN_SUCCESS, {invite});
export const getInviteByTokenFailure = (error: string) => action(InviteActionTypes.GET_INVITE_BY_TOKEN_FAILURE, {error});

export const confirmInviteRequest = () => action(InviteActionTypes.CONFIRM_INVITE_REQUEST);
export const confirmInviteSuccess = (invite: Invite) => action(InviteActionTypes.CONFIRM_INVITE_SUCCESS, {invite});
export const confirmInviteFailure = (error: string) => action(InviteActionTypes.CONFIRM_INVITE_FAILURE, {error});

export const createInvite = (invite: Invite) => {
    return async (dispatch: Dispatch) => {
        dispatch(createInviteRequest());
        return await axios.post(API_INVITE_URL, invite, createConfig(API_INVITE_URL))
            .then(res => {
                dispatch(createInviteSuccess(res.data));
            })
            .catch(err => {
                dispatch(createInviteFailure(err.message));
                console.error(err.message);
            });
    }
};

export const getInviteByToken = (token: string) => {
    return async (dispatch: Dispatch) => {
        const url = `${API_INVITE_URL}/${token}`;
        dispatch(getInviteByTokenRequest());
        return await axios.get(url, createConfig(url))
            .then(res => {
                dispatch(getInviteByTokenSuccess(res.data));
                return Promise.resolve(res.data);
            })
            .catch(err => {
                console.error(err.message);
                const message = (err.response && err.response.data && err.response.data.message) ? err.response.data.message : err.message;
                dispatch(getInviteByTokenFailure(message));
                return Promise.reject(message)
            });
    }
};

export const confirmInvite = (token: string) => {
    return async (dispatch: Dispatch) => {
        const url = `${API_INVITE_URL}/${token}/confirm`;
        dispatch(confirmInviteRequest());
        return axios.post(url, {}, createConfig(url))
            .then(res => {
                dispatch(confirmInviteSuccess(res.data));
                return Promise.resolve(res.data);
            })
            .catch(err => {
                console.error(err.message);
                const message = (err.response && err.response.data && err.response.data.message) ? err.response.data.message : err.message;
                dispatch(confirmInviteFailure(message));
                return Promise.reject(message)
            });
    }
};