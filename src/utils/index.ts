import {SignatureParams, User, UserWithRoles} from "../types";
import {Auth} from "../store/auth/types";
import {AxiosRequestConfig} from "axios";

export const createAuthConfig = (url: string, user: Auth) => {
    let params: SignatureParams = {client: user.login, login: user.login};
    params.signature = createSignature(url, params, user.password!);
    return {params};
};

export const createConfig = (url: string, customParams?: SignatureParams): AxiosRequestConfig => {
    const savedUser: Auth = getUserFromStorage();
    if (!savedUser) return {};
    let params: SignatureParams = {client: savedUser.login, ...customParams};
    params.signature = createSignature(url, params, savedUser.password);
    return {params};
};

const createSignature = (url: string, params: SignatureParams, password: string): string => {
    let paramNames: Array<string> = [];
    Object.keys(params).forEach(key => paramNames.push(key));
    let signature = password + ':' + url;
    paramNames.sort().forEach(entry => signature = signature + ":" + entry + ":" + params[entry]);
    return require("sha1")(signature);
};


export const getUserFromStorage = (): Auth => {
    return JSON.parse(localStorage.getItem("user") as string);
};

export const getUserInfoFromStorage = (): UserWithRoles => {
    return JSON.parse(sessionStorage.getItem("userInfo") as string);
};

export const getEmptyUser = (): User => {
    return {
        id: 0,
        firstName: "",
        lastName: "",
        login: "",
        email: "",
        superUser: false
    }
};






