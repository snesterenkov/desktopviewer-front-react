import moment from "moment";

const DATE_FORMAT = "YYYY-MM-DD";

export const convertMinutesToTime = (minutes: number): string => {
    return Math.floor(minutes / 60) + "h" + minutes % 60 + "m";
};

export const convertDateToFormatString = (date: Date): string => {
    return moment(date).format(DATE_FORMAT);
};

export const convertFormatStringToDate = (date: string) => {//todo typing
    return moment(date, DATE_FORMAT).toDate();
};

export const shiftDate = (date: string, offset: number) => {
    return moment(date).add(offset, "day").format(DATE_FORMAT);
};

export const getStartOfWeek = (date: string) => {
    return moment(date).startOf("week").format(DATE_FORMAT);
};

export const getEndOfWeek = (date: string) => {
    return moment(date).endOf("week").format(DATE_FORMAT);
};

export const getStartOfMonth = (date: string) => {
    return moment(date).startOf("month").format(DATE_FORMAT);
};

export const getEndOfMonth = (date: string) => {
    return moment(date).endOf("month").format(DATE_FORMAT);
};